CREATE TABLE archi_games
(
    game_id       uuid      NOT NULL PRIMARY KEY,
    uploader      bytea     NOT NULL REFERENCES users (user_id),
    date_uploaded timestamp NOT NULL,
    name          text      NOT NULL,
    is_official   boolean   NOT NULL,
    filename      text      CHECK (is_official = TRUE OR (filename IS NOT NULL AND filename NOT LIKE '%/%')),
    icon          text      CHECK (icon NOT LIKE '%/%')
);

CREATE VIEW games_ext AS
    SELECT archi_games.*,
           users.username AS uploader_name,
           users.gender AS uploader_gender
    FROM archi_games
    INNER JOIN users
    ON archi_games.uploader = users.user_id;

GO

CREATE TABLE archi_yamls
(
    yaml_id       uuid      NOT NULL PRIMARY KEY,
    archi_id      uuid      NOT NULL REFERENCES archis (archi_id) ON DELETE CASCADE,
    player        bytea     NOT NULL REFERENCES users (user_id),
    date_uploaded timestamp NOT NULL,
    game_id       uuid      NOT NULL REFERENCES archi_games (game_id) ON DELETE CASCADE,
    filename      text      NOT NULL CHECK (filename NOT LIKE '%/%')
);

CREATE VIEW yamls_ext AS
    SELECT archi_yamls.*,
           users.username AS player_name,
           users.gender AS player_gender
    FROM archi_yamls
    INNER JOIN users
    ON archi_yamls.player = users.user_id;