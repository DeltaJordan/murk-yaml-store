ALTER TABLE archis
    ADD COLUMN is_async bool,
    ADD COLUMN archi_ip text,
    ADD COLUMN archi_port int;

GO

DROP VIEW archis_ext;

GO

CREATE VIEW archis_ext AS
SELECT archis.*,
       users.username AS author_name,
       users.gender   AS author_gender
FROM archis
         INNER JOIN users
                    ON archis.author = users.user_id;