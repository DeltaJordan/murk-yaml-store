CREATE TYPE enum_role AS ENUM(
    'Banned',
    'User',
    'Moderator',
    'Staff'
);

CREATE TYPE enum_gender AS ENUM(
	'Masculine',
	'Feminine',
	'Neuter'
);

GO

CREATE TABLE users
(
    -- Discord managed fields
    user_id         bytea     NOT NULL PRIMARY KEY,
    username        text      NOT NULL,
    display_name    text,
    avatar_hash     text,
    banner_hash     text,
    -- Our fields
    role            enum_role NOT NULL,
    joined_date     timestamp NOT NULL,
    bio             text,
    gender          enum_gender,
    banner_override bytea
);

GO

CREATE UNIQUE INDEX ix_users_by_username ON users (username) INCLUDE(user_id);
CREATE INDEX ix_users_mods ON users (role) WHERE role = 'Moderator' OR role = 'Staff';
CREATE INDEX ix_user_context ON users (user_id) INCLUDE(username, display_name, avatar_hash, banner_hash, role);

GO

CREATE TABLE news_posts
(
    post_id      uuid      NOT NULL PRIMARY KEY,
    author       bytea REFERENCES users (user_id),
    date_created timestamp NOT NULL,
    date_edited  timestamp,
    title        text      NOT NULL,
    message      text      NOT NULL,
    markdown     text      NOT NULL
);

CREATE INDEX ix_news_by_date ON news_posts (date_created DESC);

CREATE VIEW news_posts_ext AS
    SELECT news_posts.*,
           users.username AS author_name,
           users.gender   AS author_gender
    FROM news_posts
    INNER JOIN users
    ON news_posts.author = users.user_id;

GO

CREATE TABLE migrations
(
    filename       text      NOT NULL PRIMARY KEY,
    date_completed timestamp NOT NULL
);

GO

CREATE MATERIALIZED VIEW user_context AS
SELECT user_id,
       username,
       display_name,
       avatar_hash,
       banner_hash,
       role
FROM users
GROUP BY user_id;

GO

CREATE TABLE archis
(
    archi_id       uuid      NOT NULL PRIMARY KEY,
    author         bytea     NOT NULL REFERENCES users (user_id),
    title          text      NOT NULL,
    description    text      NOT NULL,
    icon           text CHECK ( icon NOT LIKE '%/%' ),
    date_created   timestamp NOT NULL,
    start_date     timestamp NOT NULL,
    date_completed timestamp
);

CREATE INDEX ix_archis_by_date ON archis (date_created DESC);

CREATE VIEW archis_ext AS
    SELECT archis.*,
           users.username AS author_name,
           users.gender   AS author_gender
    FROM archis
    INNER JOIN users
    ON archis.author = users.user_id;