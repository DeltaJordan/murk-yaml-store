DELETE FROM archi_games ag
WHERE ag.date_uploaded < (SELECT MAX(ag2.date_uploaded)
                          FROM archi_games ag2
                          WHERE ag.name = ag2.name
                          AND ag.archi_id = ag2.archi_id);

GO

CREATE UNIQUE INDEX archi_games_archi_unique
    ON archi_games(name, archi_id);

GO

ALTER TABLE archi_games
ADD CONSTRAINT unique_archi_games
UNIQUE USING INDEX archi_games_archi_unique;