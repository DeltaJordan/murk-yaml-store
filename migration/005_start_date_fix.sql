ALTER TABLE archis
    RENAME start_date TO submission_deadline;

GO

ALTER TABLE archis
    ADD COLUMN start_date timestamp NOT NULL DEFAULT '2000-01-01 00:00:00';

GO

DROP VIEW archis_ext;

GO

CREATE VIEW archis_ext AS
SELECT archis.*,
       users.username AS author_name,
       users.gender   AS author_gender
FROM archis
         INNER JOIN users
                    ON archis.author = users.user_id;