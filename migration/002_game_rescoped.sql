DELETE FROM archi_games;
DELETE FROM archi_yamls;
DROP VIEW games_ext;
DROP VIEW archis_ext;

GO

ALTER TABLE archi_games
    ADD COLUMN archi_id uuid NOT NULL REFERENCES archis(archi_id);

CREATE VIEW games_ext AS
SELECT archi_games.*,
       users.username AS uploader_name,
       users.gender AS uploader_gender
FROM archi_games
         INNER JOIN users
                    ON archi_games.uploader = users.user_id;

GO

ALTER TABLE archis
    ADD COLUMN seed_filename text CHECK (seed_filename NOT LIKE '%/%');

CREATE VIEW archis_ext AS
SELECT archis.*,
       users.username AS author_name,
       users.gender   AS author_gender
FROM archis
         INNER JOIN users
                    ON archis.author = users.user_id;