namespace MurkYamlStore.Activation.Impl;

internal static class PluginFactory<T>
{

	private static readonly List<Type> concreteTypes = [];

	internal static void Register<P>() where P : T
	{
		concreteTypes.Add(typeof(P));
	}

	internal static IPlugins<T> Activate(IServiceProvider services)
	{
		return new Plugins<T>(
			concreteTypes.Select(type => (T)services.GetService(type))
		);
	}

}
