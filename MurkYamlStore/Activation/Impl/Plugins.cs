using System.Collections;
using System.Collections.Generic;

namespace MurkYamlStore.Activation.Impl;

internal sealed class Plugins<T>(IEnumerable<T> plugins) : IPlugins<T>
{
	private readonly List<T> plugins = plugins.CoerceToList();

	int IPlugins<T>.Count => this.plugins.Count;

	IEnumerator<T> IEnumerable<T>.GetEnumerator()
	{
		return this.plugins.GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return this.plugins.GetEnumerator();
	}
}
