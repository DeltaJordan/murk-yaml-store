namespace MurkYamlStore.Activation;

public interface IPlugins<T> : IEnumerable<T>
{
	int Count { get; }
}
