namespace MurkYamlStore.Activation;

public interface IFactory<T> where T : class
{
	T Create();
}
