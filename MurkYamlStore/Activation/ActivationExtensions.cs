using Microsoft.Extensions.DependencyInjection;
using MurkYamlStore.Activation.Impl;

namespace MurkYamlStore.Activation;

public static class ActivationExtensions
{

	public static void AddFactory<T, F>(this IServiceCollection registry)
		where T : class
		where F : class, IFactory<T>
	{
		registry.AddSingleton<IFactory<T>, F>();
		registry.AddSingleton(serviceProvider => serviceProvider.GetRequiredService<IFactory<T>>().Create());
	}

	public static void AddPlugin<TInterface, TImpl>(this IServiceCollection registry) where TImpl : class, TInterface
	{
		registry.AddSingleton<TImpl, TImpl>();
		PluginFactory<TInterface>.Register<TImpl>();
	}

	public static void ConfigurePlugins<T>(this IServiceCollection registry)
	{
		registry.AddSingleton(PluginFactory<T>.Activate);
	}

}
