using MurkYamlStore.Util;
using Newtonsoft.Json;

namespace MurkYamlStore;

public sealed class ServerConfig
{
    private const string DISCORD_SECRET = "DISCORD_SECRET";

    private ServerConfig(
        string discordSecret,
        ConfigFile cfg
    )
    {
        this.DiscordSecret = discordSecret;
        this.DiscordClientId = cfg.DiscordClientId;
        this.UploadSizeLimit = cfg.UploadSizeLimit * 1024L * 1024L;
        this.StoragePerUser = cfg.StoragePerUser;
        this.DbConnectionString = cfg.ConnectionString;
        this.LogLevel = cfg.LogLevel;
        this.LogFilePath = cfg.LogFilePath;
        this.CacheSize = cfg.CacheSize;
        this.WebHost = cfg.WebHost;
        this.AllowedOrigins = cfg.AllowedOrigins;
        this.TemporaryDirectory = cfg.TemporaryDirectory;
    }

    public string DiscordSecret { get; }
    public string DiscordClientId { get; }
    
    /// <summary>
    /// Max upload size, in MiB
    /// </summary>
    public long UploadSizeLimit { get; }
    public int StoragePerUser { get; }
    public string DbConnectionString { get; }
    public LogLevel LogLevel { get; }
    public string LogFilePath { get; }
    public int CacheSize { get; }
    public string WebHost { get; }
    public string[] AllowedOrigins { get; }
    public string TemporaryDirectory { get; }

    [JsonObject]
    private sealed class ConfigFile
    {
        public string ConnectionString { get; set; }
        [JsonConverter(typeof(LogLevelConverter))]
        public LogLevel LogLevel { get; set; }
        public string LogFilePath { get; set; }
        public int CacheSize { get; set; }
        public string WebHost { get; set; }
        public string[] AllowedOrigins { get; set; }
        public string TemporaryDirectory { get; set; }
        public string DiscordClientId { get; set; }
        public int UploadSizeLimit { get; set; }
        public int StoragePerUser { get; set; }
    }
    private static readonly SafeLazy<ServerConfig> instance = new(
            InitServerConfig,
            LazyThreadSafetyMode.PublicationOnly
        );

    public static ServerConfig Instance => instance.Value;

    private static ServerConfig InitServerConfig()
    {
        string discordSecret = Environment.GetEnvironmentVariable(DISCORD_SECRET);

#if DEBUG
        Console.Out.WriteLine($"Running in DEBUG environment with process id [{Environment.ProcessId}]");
#endif
        
        Console.Out.WriteLine($"Running as {Environment.UserName} in {Directory.GetCurrentDirectory()}");

        if (string.IsNullOrWhiteSpace(discordSecret))
        {
            throw new Exception($"FATAL ERROR: Environment variable {DISCORD_SECRET} is not set.");
        }

        string configJson = File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}/config.json");
        ConfigFile config = JsonConvert.DeserializeObject<ConfigFile>(configJson);

        // TODO: Set important defaults.

        return new ServerConfig(discordSecret, config);
    }

    private class LogLevelConverter : JsonConverter
    {
        public override bool CanWrite => false;
        public override bool CanRead => true;

        public override bool CanConvert(Type objectType)
        {
            return false;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                // Default to Info
                return LogLevel.Info;
            }
            else if (reader.TokenType == JsonToken.String)
            {
                return LogLevel.FromString((string)reader.Value);
            }
            else if (reader.TokenType == JsonToken.Integer)
            {
                return LogLevel.FromOrdinal((int)reader.Value);
            }

            return LogLevel.Info;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}