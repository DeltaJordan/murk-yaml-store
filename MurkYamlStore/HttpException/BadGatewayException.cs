using System;
using System.Net;
using Microsoft.Extensions.Logging;

namespace MurkYamlStore.HttpException;


public class BadGatewayException : HttpErrorResponseException
{

	public BadGatewayException(
		string message = null,
		string logMessage = null,
		Exception innerException = null
	) : base(message ?? "Bad Gateway", logMessage, innerException) { }

	public override string Title => "Bad Gateway";
	public override HttpStatusCode StatusCode => HttpStatusCode.BadGateway;
	public override LogLevel LoggingLevel => LogLevel.Warn;

}
