using System.Net;
using Microsoft.Extensions.Logging;

namespace MurkYamlStore.HttpException;


public class DeadlockException : HttpErrorResponseException
{

	internal DeadlockException(string lockName) : base(
		message: "Web request encountered a deadlock and was terminated. Please try again.",
		logMessage: $"Deadlock occured on lock '{lockName}'"
	)
	{
		this.LockName = lockName;
	}

	public string LockName { get; }

	public override string Title => "Request Deadlocked";
	public override HttpStatusCode StatusCode => HttpStatusCode.Conflict;
	public override LogLevel LoggingLevel => LogLevel.Error;

}
