using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.Extensions.Logging;

namespace MurkYamlStore.HttpException;


public class UnprocessableEntityException : HttpErrorResponseException
{

	private readonly string m_paramName;

	public UnprocessableEntityException(
		string message = null,
		string logMessage = null,
		Exception innerException = null,
		string paramName = null
	) : base(message ?? "Unprocessable Entity", logMessage, innerException)
	{
		this.m_paramName = paramName;
	}

	public override string Title => "Unprocessable Entity";
	public override HttpStatusCode StatusCode => HttpStatusCode.UnprocessableEntity;
	public override LogLevel LoggingLevel => LogLevel.Info;

	protected internal override IDictionary<string, string> ExtraResponseHeaders =>
		string.IsNullOrEmpty(this.m_paramName) ? null : new Dictionary<string, string> {
			{ "X-Invalid-Parameter", this.m_paramName }
		};

}
