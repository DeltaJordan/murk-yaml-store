using System;
using System.Net;
using Microsoft.Extensions.Logging;

namespace MurkYamlStore.HttpException;


public class ConflictException : HttpErrorResponseException
{

	public ConflictException(
		string message = null,
		string logMessage = null,
		Exception innerException = null
	) : base(message ?? "Conflict", logMessage, innerException) { }

	public override string Title => "Conflict";
	public override HttpStatusCode StatusCode => HttpStatusCode.Conflict;
	public override LogLevel LoggingLevel => LogLevel.Info;

}
