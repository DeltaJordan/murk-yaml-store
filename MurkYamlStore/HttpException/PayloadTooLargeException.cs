using System;
using System.Net;
using Microsoft.Extensions.Logging;

namespace MurkYamlStore.HttpException;


public class PayloadTooLargeException : HttpErrorResponseException
{

    public PayloadTooLargeException(
        string message = null,
        string logMessage = null,
        Exception innerException = null
    ) : base(message ?? "Payload Too Large", logMessage, innerException) { }

    public override string Title => "Payload Too Large";
    public override HttpStatusCode StatusCode => HttpStatusCode.RequestEntityTooLarge;
    public override LogLevel LoggingLevel => LogLevel.Info;

}
