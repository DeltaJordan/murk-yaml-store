using System;
using System.Net;
using Microsoft.Extensions.Logging;

namespace MurkYamlStore.HttpException;


public class FailedDependencyException : HttpErrorResponseException
{

	public FailedDependencyException(
		string message = null,
		string logMessage = null,
		Exception innerException = null
	) : base(message ?? "Failed Dependency", logMessage, innerException) { }

	public override string Title => "Failed Dependency";
	public override HttpStatusCode StatusCode => HttpStatusCode.FailedDependency;
	public override LogLevel LoggingLevel => LogLevel.Debug;

}
