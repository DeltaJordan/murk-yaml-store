using System.Net;
using Microsoft.Extensions.Primitives;
using Microsoft.Net.Http.Headers;
using MurkYamlStore.Postgres;
using Newtonsoft.Json;
using NLog;
using Npgsql;

namespace MurkYamlStore.HttpException;


[JsonConverter(typeof(ErrorDetails.Serializer))]
public sealed class ErrorDetails
{

    private static readonly Logger logger = LogManager.GetCurrentClassLogger();

    private readonly string logMessage;
    private readonly LogLevel logLevel;

    public ErrorDetails(Exception exception, HttpRequest requestContext)
    {
        this.Exception = exception;
        this.ApiCall = null;
        this.UserAgent = null;
        this.SourcePage = null;

        if (requestContext?.Headers?.TryGetValue(HeaderNames.UserAgent, out StringValues userAgent) == true)
        {
            this.UserAgent = userAgent.ToString();
        }

        if (requestContext?.Headers?.TryGetValue("X-Source-Page", out StringValues sourcePage) == true)
        {
            this.SourcePage = sourcePage.ToString();
        }

        if (requestContext?.Method != null && requestContext.Path.HasValue)
        {
            this.ApiCall = $"{requestContext.Method} {requestContext.Path.Value}";
        }

        if (exception is HttpErrorResponseException httpException)
        {
            this.logMessage = httpException.LogMessage ?? exception.Message ?? string.Empty;
            this.logLevel = httpException.LoggingLevel;
            this.StatusCode = httpException.StatusCode;
            this.Title = httpException.Title;
            this.Detail = httpException.Message;
            this.AdditionalProperties = httpException.AdditionalProperties;
            this.ExtraResponseHeaders = httpException.ExtraResponseHeaders;
        }
        else if (exception is BadHttpRequestException aspException)
        {
            this.logMessage = exception.Message;
            this.logLevel = LogLevel.Warn;
            this.StatusCode = (HttpStatusCode)aspException.StatusCode;
            this.Title = this.StatusCode.ToString();
#if DEBUG
            this.Detail = exception.Message;
#else
            this.Detail = null;
#endif
            this.AdditionalProperties = null;
            this.ExtraResponseHeaders = null;
        }
        else if (exception is JsonSerializationException)
        {
            this.logMessage = exception.Message;
            this.logLevel = LogLevel.Warn;
            this.StatusCode = HttpStatusCode.BadRequest;
            this.Title = "Invalid JSON Payload";
#if DEBUG
            this.Detail = exception.Message;
#else
            this.Detail = null;
#endif
            this.AdditionalProperties = null;
            this.ExtraResponseHeaders = null;
        }
        else if (exception is PostgresException postgresException)
        {
            PostgresErrorClass errorType = postgresException.GetErrorClass();
            this.logMessage = exception.Message;
            this.logLevel = LogLevel.Error;
            switch (errorType)
            {
                case PostgresErrorClass.TransactionRollback:
                    this.StatusCode = HttpStatusCode.Conflict;
                    this.Detail = "Action failed due to a transaction rollback. Please retry this request, and if it fails again, submit a bug report.";
                    break;
                case PostgresErrorClass.IntegrityConstraintViolation:
                case PostgresErrorClass.TriggeredDataChangeViolation:
                    this.StatusCode = HttpStatusCode.Conflict;
                    this.Detail = "Action failed due to a database integretity constraint violation. Please retry this request, and if it fails again, submit a bug report.";
                    break;
                case PostgresErrorClass.InsufficientResources:
                case PostgresErrorClass.OperatorIntervention:
                    this.StatusCode = HttpStatusCode.ServiceUnavailable;
                    this.Detail = null;
                    break;
                default:
                    this.StatusCode = HttpStatusCode.InternalServerError;
                    this.Title = "Internal Server Error";
#if DEBUG
                    this.Detail = exception.Message;
#else
                    this.Detail = null;
#endif
                    break;
            }
            this.AdditionalProperties = null;
            this.ExtraResponseHeaders = null;
        }
        else
        {
            this.logMessage = exception.Message ?? "Unknown Error";
            this.logLevel = LogLevel.Error;
            this.StatusCode = HttpStatusCode.InternalServerError;
            this.Title = "Internal Server Error";
#if DEBUG
            this.Detail = exception.Message;
#else
            this.Detail = null;
#endif
            this.AdditionalProperties = null;
            this.ExtraResponseHeaders = null;
        }
    }

    public Exception Exception { get; }
    public HttpStatusCode StatusCode { get; }
    public string Title { get; }
    public string Detail { get; }
    public IDictionary<string, object> AdditionalProperties { get; }
    public IDictionary<string, string> ExtraResponseHeaders { get; }

    public string ApiCall { get; }
    public string UserAgent { get; }
    public string SourcePage { get; }

    public void Log()
    {
        if (this.logLevel != LogLevel.Off)
        {
            logger.Log(this.logLevel, this.Exception);
        }
    }

    private class Serializer : JsonConverter<ErrorDetails>
    {

        public override ErrorDetails ReadJson(
            JsonReader reader,
            Type objectType,
            ErrorDetails existingValue,
            bool hasExistingValue,
            JsonSerializer serializer
        )
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(
            JsonWriter writer,
            ErrorDetails value,
            JsonSerializer serializer
        )
        {
            writer.WriteStartObject();
            writer.WritePropertyName("title");
            writer.WriteValue(value.Title);
            writer.WritePropertyName("status");
            writer.WriteValue((int)value.StatusCode);
            if (value.Detail != null && value.Detail.Length > 0)
            {
                writer.WritePropertyName("detail");
                writer.WriteValue(value.Detail);
            }
            if (value.AdditionalProperties != null)
            {
                foreach (KeyValuePair<string, object> prop in value.AdditionalProperties)
                {
                    writer.WritePropertyName(prop.Key);
                    serializer.Serialize(writer, prop.Value);
                }
            }
#if DEBUG
            if (value.Exception != null)
            {
                writer.WritePropertyName("exception");
                SerializeException(writer, value.Exception);
            }
#endif
            writer.WriteEndObject();
        }

        private static void SerializeException(
            JsonWriter writer,
            Exception exception
        )
        {
            writer.WriteStartObject();
            writer.WritePropertyName("type");
            writer.WriteValue(exception.GetType().Name);
            writer.WritePropertyName("message");
            writer.WriteValue(exception.Message ?? string.Empty);

            if (exception.StackTrace != null && exception.StackTrace.Length > 0)
            {
                writer.WritePropertyName("stack_trace");
                SerializeStackTrace(writer, exception.StackTrace);
            }

            if (exception is AggregateException aggregateException)
            {
                writer.WritePropertyName("inner_exceptions");
                writer.WriteStartArray();
                foreach (Exception childException in aggregateException.InnerExceptions)
                {
                    if (childException == null) continue;
                    SerializeException(writer, childException);
                }
                writer.WriteEndArray();
            }
            else if (exception.InnerException != null)
            {
                writer.WritePropertyName("inner_exception");
                SerializeException(writer, exception.InnerException);
            }

            writer.WriteEndObject();
        }

        private static void SerializeStackTrace(
            JsonWriter writer,
            string stackTrace
        )
        {
            writer.WriteStartArray();

            string[] callStack = stackTrace.Split(
                new char[] { '\r', '\n' },
                StringSplitOptions.RemoveEmptyEntries
            );

            foreach (string line in callStack)
            {
                string trimmedLine = line.Trim();

                if (trimmedLine.StartsWith("at ", StringComparison.OrdinalIgnoreCase))
                {
                    trimmedLine = trimmedLine.Substring(3);
                }

                if (trimmedLine.Length > 0)
                {
                    writer.WriteValue(trimmedLine);
                }
            }

            writer.WriteEndArray();
        }

    }

}

