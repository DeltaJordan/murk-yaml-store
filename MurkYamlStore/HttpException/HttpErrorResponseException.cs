using System.Net;

namespace MurkYamlStore.HttpException;

public abstract class HttpErrorResponseException : Exception
{

	private readonly string m_logMessage;

	protected HttpErrorResponseException(
		string message = null,
		string logMessage = null,
		Exception innerException = null
	) : base(message, innerException)
	{
		this.m_logMessage = logMessage ?? message ?? this.GetType().Name;
	}

	public string LogMessage { get => this.m_logMessage; }

	public abstract string Title { get; }
	public abstract HttpStatusCode StatusCode { get; }
	public abstract LogLevel LoggingLevel { get; }

	protected internal virtual IDictionary<string, object> AdditionalProperties
	{
		get { return null; }
	}

	protected internal virtual IDictionary<string, string> ExtraResponseHeaders
	{
		get { return null; }
	}


}
