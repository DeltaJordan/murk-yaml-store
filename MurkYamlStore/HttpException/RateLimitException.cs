using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.Extensions.Logging;

namespace MurkYamlStore.HttpException;


public class RateLimitException : HttpErrorResponseException
{

    protected readonly TimeSpan m_retryAfter;

    public RateLimitException(
        TimeSpan retryAfter,
        string message = null,
        string logMessage = null,
        Exception innerException = null
    ) : base(message ?? "Too Many Requests", logMessage, innerException)
    {
        this.m_retryAfter = retryAfter;
    }

    public override string Title => "Too Many Requests";
    public override HttpStatusCode StatusCode => HttpStatusCode.TooManyRequests;
    public override LogLevel LoggingLevel => LogLevel.Off;

    protected internal override IDictionary<string, object> AdditionalProperties
    {
        get
        {
            return new Dictionary<string, object>{
                { "retryAfterMs", (int)Math.Max( 1.0, Math.Ceiling( this.m_retryAfter.TotalMilliseconds ) ) }
            };
        }
    }

    protected internal override IDictionary<string, string> ExtraResponseHeaders
    {
        get
        {
            int seconds = (int)Math.Max(1.0, Math.Ceiling(this.m_retryAfter.TotalSeconds));
            return new Dictionary<string, string>{
                { "Retry-After", seconds.ToString() }
            };
        }
    }

}
