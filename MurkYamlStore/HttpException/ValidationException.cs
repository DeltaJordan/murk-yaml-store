using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using Microsoft.Extensions.Logging;

namespace MurkYamlStore.HttpException;

public sealed class ValidationException : HttpErrorResponseException
{

    private readonly string m_errorCode;

    public ValidationException(
        [NotNull] string errorCode,
        [NotNull] string message,
        Exception innerException = null
    ) : base(message, null, innerException)
    {
        this.m_errorCode = errorCode;
    }

    public override string Title => $"Validation Error: {this.m_errorCode}";
    public override HttpStatusCode StatusCode => HttpStatusCode.BadRequest;
    public override LogLevel LoggingLevel => LogLevel.Debug;

    protected internal override IDictionary<string, object> AdditionalProperties => new Dictionary<string, object> {
        { "code", this.m_errorCode }
    };

}
