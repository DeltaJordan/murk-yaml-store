using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.Extensions.Logging;

namespace MurkYamlStore.HttpException;


internal sealed class ForbiddenZipContentException : HttpErrorResponseException
{

	private readonly IReadOnlyList<string> m_rejectedFiles;
	private readonly IReadOnlySet<string> m_allowedExtensions;

	public ForbiddenZipContentException(
		IReadOnlyList<string> rejectedFiles,
		IReadOnlySet<string> allowedExtensions
	) : base("The zip file was rejected because it contains a file with a disallowed extension.")
	{
		this.m_rejectedFiles = rejectedFiles;
		this.m_allowedExtensions = allowedExtensions;
	}

	public override string Title => "Forbidden Zip Content";
	public override HttpStatusCode StatusCode => HttpStatusCode.UnprocessableEntity;
	public override LogLevel LoggingLevel => LogLevel.Info;

	protected internal override IDictionary<string, object> AdditionalProperties
	{
		get => new Dictionary<string, object> {
			{ "rejected_files", this.m_rejectedFiles.ToArray() },
			{ "allowed_extensions", this.m_allowedExtensions.ToArray() }
		};
	}

}
