using System;
using System.Net;
using Microsoft.Extensions.Logging;

namespace MurkYamlStore.HttpException;


public class BadRequestException : HttpErrorResponseException
{

	public BadRequestException(
		string message = null,
		string logMessage = null,
		Exception innerException = null
	) : base(message ?? "Bad Request", logMessage, innerException) { }

	public override string Title => "Bad Request";
	public override HttpStatusCode StatusCode => HttpStatusCode.BadRequest;
	public override LogLevel LoggingLevel => LogLevel.Info;

}
