using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.Extensions.Logging;

namespace MurkYamlStore.HttpException;


public class UnsupportedMediaTypeException : HttpErrorResponseException
{

    private readonly IReadOnlyList<string> m_allowedTypes;

    public UnsupportedMediaTypeException(
        IEnumerable<string> allowedMimeTypes,
        string message = null
    ) : base(message ?? "Unsupported Media Type")
    {
        this.m_allowedTypes = allowedMimeTypes.CoerceToIReadOnlyList();
    }

    public UnsupportedMediaTypeException(string message) : this(null, message) { }

    public override string Title => "Unsupported Media Type";
    public override HttpStatusCode StatusCode => HttpStatusCode.UnsupportedMediaType;
    public override LogLevel LoggingLevel => LogLevel.Info;

    protected internal override IDictionary<string, object> AdditionalProperties
    {
        get
        {
            if (this.m_allowedTypes == null) return null;
            IDictionary<string, object> props = base.AdditionalProperties ?? new Dictionary<string, object>();
            props["allowed_types"] = this.m_allowedTypes.ToArray();
            return props;
        }
    }

}
