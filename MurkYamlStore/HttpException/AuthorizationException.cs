using System;
using System.Net;
using Microsoft.Extensions.Logging;

namespace MurkYamlStore.HttpException;


public class AuthorizationException : HttpErrorResponseException
{

	public AuthorizationException(
		string message = null,
		string logMessage = null,
		Exception innerException = null
	) : base(message ?? "Permission Denied", logMessage, innerException) { }

	public override string Title => "Permission Denied";
	public override HttpStatusCode StatusCode => HttpStatusCode.Forbidden;
	public override LogLevel LoggingLevel => LogLevel.Info;

}
