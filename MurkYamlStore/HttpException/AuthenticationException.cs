using System.Net;

namespace MurkYamlStore.HttpException;


public class AuthenticationException : HttpErrorResponseException
{
	protected readonly TimeSpan? retryAfter;

	public AuthenticationException(
		string message = null,
		string logMessage = null,
		Exception innerException = null,
		TimeSpan? retryAfter = null
	) : base(message ?? "Not Authenticated", logMessage, innerException)
	{
		this.retryAfter = retryAfter;
	}

	public override string Title => "Not Authenticated";
	public override HttpStatusCode StatusCode => HttpStatusCode.Unauthorized;
	public override LogLevel LoggingLevel => LogLevel.Debug;

	protected internal override IDictionary<string, object> AdditionalProperties
	{
		get
		{
			return this.retryAfter.HasValue ? new Dictionary<string, object>{
				{ "retryAfterMs", (int)Math.Max( 1.0, Math.Ceiling( this.retryAfter.Value.TotalMilliseconds ) ) }
			} : null;
		}
	}

	protected internal override IDictionary<string, string> ExtraResponseHeaders
	{
		get
		{
			Dictionary<string, string> headers = [];

			if (this.retryAfter.HasValue)
			{
				int seconds = (int)Math.Max(1.0, Math.Ceiling(this.retryAfter.Value.TotalSeconds));
				headers.Add("Retry-After", seconds.ToString());
			}

			return headers;
		}
	}

}
