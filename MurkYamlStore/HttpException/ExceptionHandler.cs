using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace MurkYamlStore.HttpException;


public static class ExceptionHandler
{

    public static void Register(IApplicationBuilder appBuilder)
    {
        appBuilder.Run(HandleExceptionAsync);
    }

    private static async Task HandleExceptionAsync(HttpContext context)
    {

        Exception exception = context.Features.Get<IExceptionHandlerPathFeature>()?.Error;
        if (exception == null) return;

        if (exception is FileNotFoundException)
        {
            exception = new NotFoundException(
                logMessage: exception.Message,
                innerException: exception
            );
        }

        ErrorDetails error = new(exception, context.Request);
        error.Log();

        context.Response.StatusCode = (int)error.StatusCode;
        context.Response.ContentType = "application/problem+json";
        if (error.ExtraResponseHeaders != null)
        {
            foreach (KeyValuePair<string, string> header in error.ExtraResponseHeaders)
            {
                context.Response.Headers.Append(header.Key, header.Value);
            }
        }

        string responseJson = JsonConvert.SerializeObject(
            error,
#if DEBUG
            Formatting.Indented
#else
				Formatting.None
#endif
        );

        context.Response.Headers.ContentLength = responseJson.Length;
        await context.Response.WriteAsync(responseJson).SafeAsync();
    }

}
