using System;
using System.Net;
using Microsoft.Extensions.Logging;

namespace MurkYamlStore.HttpException;

public class NotFoundException : HttpErrorResponseException
{

    public NotFoundException(
        string message = null,
        string logMessage = null,
        Exception innerException = null
    ) : base(message ?? "Resource Not Found", logMessage, innerException) { }

    public override string Title => "Resource Not Found";
    public override HttpStatusCode StatusCode => HttpStatusCode.NotFound;
    public override LogLevel LoggingLevel => LogLevel.Debug;

}
