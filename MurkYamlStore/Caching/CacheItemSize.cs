namespace MurkYamlStore.Caching
{
    public enum CacheItemSize
    {
        Small = 1,
        Big = 5,
        Huge = 20,
        Giant = 100
    }
}