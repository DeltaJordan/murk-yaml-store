using MurkYamlStore.Core;

namespace MurkYamlStore.Caching;

public interface ICache
{
    Task<T> GetAsync<T>(string cacheKey, Func<Task<T>> getter, CacheItemSize itemSize = CacheItemSize.Small);
    Task<T> GetAsync<T>(string cacheKey, Func<Task<T>> getter, Func<T, int> itemSizer);
    Task<TryResult<T>> TryGetAsync<T>(string cacheKey);
    Task SetAsync<T>(string cacheKey, T value, CacheItemSize itemSize = CacheItemSize.Small);
    Task RemoveAsync(string cacheKey);

}
