using MurkYamlStore.Caching.Impl;

namespace MurkYamlStore.Caching;

internal static class DependencyLoader
{
    public static void Load(IServiceCollection registry)
    {
        registry.AddSingleton<ICacheProvider, CacheProvider>();
    }

}
