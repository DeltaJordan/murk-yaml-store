namespace MurkYamlStore.Caching
{
    public interface ICacheProvider
    {

        ICache GetHttpRequestCache(
            string cacheKeyPrefix
        );

        ICache GetPersistentCache(
            string cacheKeyPrefix,
            TimeSpan cacheDuration
        );

    }
}