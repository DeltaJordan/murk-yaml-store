using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace MurkYamlStore.Caching.Impl;

/* For better perforamnce, this caching layer doesn't use a thread-safe dictionary
	 * under the assumption that a single web request will not be making concurrent
	 * calls to the cache. If parallelization within the same web request is later added,
	 * then this cache must be updated to use ConcurrentDictionary instead
	 */

internal sealed class RequestCache : ICacheImplementation
{

	private static readonly object s_contextKey = new();
	private readonly IHttpContextAccessor httpContextAccessor;

	public RequestCache(
		IHttpContextAccessor httpContextAccessor
	)
	{
		this.httpContextAccessor = httpContextAccessor;
	}

	private bool TryGetHttpCache(out IDictionary<string, object> cache)
	{
		HttpContext context = this.httpContextAccessor.HttpContext;
		if (context == null)
		{
			cache = null;
			return false;
		}

		if (context.Items.TryGetValue(s_contextKey, out object co))
		{
			cache = (IDictionary<string, object>)co;
		}
		else
		{
			cache = new Dictionary<string, object>();
			context.Items[s_contextKey] = cache;
		}

		return true;
	}

	bool ICacheImplementation.TryGet<T>(string cacheKey, out T value)
	{
		if (
			this.TryGetHttpCache(out IDictionary<string, object> cache) &&
			cache.TryGetValue(cacheKey, out object valueObj) &&
			valueObj is T _value
		)
		{
			value = _value;
			return true;
		}

		value = default;
		return false;
	}

	void ICacheImplementation.Set<T>(string cacheKey, T value, int itemSize)
	{
		if (this.TryGetHttpCache(out IDictionary<string, object> cache))
		{
			cache[cacheKey] = value;
		}
	}

	void ICacheImplementation.Remove(string cacheKey)
	{
		if (this.TryGetHttpCache(out IDictionary<string, object> cache))
		{
			cache.Remove(cacheKey);
		}
	}

}
