using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace MurkYamlStore.Caching.Impl;

internal static class DefaultCacheItemSizer<T>
{

	private static readonly Func<T, int> s_itemCounter;

	static DefaultCacheItemSizer()
	{
		if (!typeof(T).IsAssignableTo(typeof(IEnumerable)))
		{
			s_itemCounter = (x) => 1;
			return;
		}

		if (typeof(T).IsAssignableTo(typeof(ICollection)))
		{
			s_itemCounter = (x) => (x as ICollection).Count;
			return;
		}

		IEnumerable<Type> interfaces = typeof(T).GetInterfaces();
		if (typeof(T).IsInterface)
		{
			interfaces = interfaces.Concat(new[] { typeof(T) });
		}

		foreach (Type i in interfaces)
		{
			if (!i.IsGenericType) continue;
			if (i.GenericTypeArguments.Length != 1) continue;

			Type itemType = i.GenericTypeArguments[0];

			Type ct1 = typeof(ICollection<>).MakeGenericType(itemType);
			Type ct2 = typeof(IReadOnlyCollection<>).MakeGenericType(itemType);

			if (!i.Equals(ct1) && !i.Equals(ct2))
			{
				continue;
			}

			PropertyInfo countProp = i.GetProperty("Count");
			if (countProp == null || !countProp.CanRead) continue;

			MethodInfo getter = countProp.GetMethod;
			if (getter == null) continue;

			ParameterExpression input = Expression.Parameter(i);
			s_itemCounter = Expression.Lambda<Func<T, int>>(
				Expression.Call(input, getter),
				input
			).Compile();
			return;
		}

		s_itemCounter = (x) => 1;
	}

	public static int GetSize(
		T item,
		CacheItemSize sizeHint = CacheItemSize.Small
	)
	{
		return s_itemCounter(item) * (int)sizeHint;
	}

}
