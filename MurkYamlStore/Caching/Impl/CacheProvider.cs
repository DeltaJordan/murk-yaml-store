using Microsoft.Extensions.Caching.Memory;

namespace MurkYamlStore.Caching.Impl;

internal sealed class CacheProvider : ICacheProvider
{

    private readonly IHttpContextAccessor httpContextAccessor;
    private readonly IMemoryCache memoryCache;

    public CacheProvider(
        IHttpContextAccessor httpContextAccessor
    )
    {
        this.httpContextAccessor = httpContextAccessor;
        this.memoryCache = new MemoryCache(
            new MemoryCacheOptions { SizeLimit = ServerConfig.Instance.CacheSize }
        );
    }

    ICache ICacheProvider.GetHttpRequestCache(
        string cacheKeyPrefix
    )
    {
        return new CacheAdapter(
            new PrefixingCache(
                new RequestCache(this.httpContextAccessor),
                cacheKeyPrefix
            )
        );
    }

    ICache ICacheProvider.GetPersistentCache(
        string cacheKeyPrefix,
        TimeSpan cacheDuration
    )
    {
        return new CacheAdapter(
            new PrefixingCache(
                new LayeredCache([
                    new RequestCache( this.httpContextAccessor ),
                    new LocalCache( this.memoryCache, cacheDuration )
                ]),
                cacheKeyPrefix
            )
        );
    }

}
