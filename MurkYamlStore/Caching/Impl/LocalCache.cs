using System;
using Microsoft.Extensions.Caching.Memory;

namespace MurkYamlStore.Caching.Impl;

internal sealed class LocalCache(IMemoryCache memoryCache, TimeSpan cacheDuration) : ICacheImplementation
{

	private readonly IMemoryCache cache = memoryCache;
	private readonly TimeSpan cacheDuration = cacheDuration;

	bool ICacheImplementation.TryGet<T>(string cacheKey, out T value)
	{
		return this.cache.TryGetValue(cacheKey, out value);
	}

	void ICacheImplementation.Set<T>(string cacheKey, T value, int itemSize)
	{
		this.cache.Set(cacheKey, value, new MemoryCacheEntryOptions
		{
			Size = itemSize,
			AbsoluteExpirationRelativeToNow = this.cacheDuration
		});
	}

	void ICacheImplementation.Remove(string cacheKey)
	{
		this.cache.Remove(cacheKey);
	}

}
