namespace MurkYamlStore.Caching.Impl;

internal sealed class PrefixingCache : ICacheImplementation
{

	private readonly ICacheImplementation inner;
	private readonly string cacheKeyPrefix;

	public PrefixingCache(
		ICacheImplementation innerCache,
		string prefix
	)
	{
		this.inner = innerCache;
		this.cacheKeyPrefix = prefix;
	}

	void ICacheImplementation.Remove(string cacheKey)
	{
		this.inner.Remove($"{this.cacheKeyPrefix}::{cacheKey}");
	}

	void ICacheImplementation.Set<T>(string cacheKey, T value, int itemSize)
	{
		this.inner.Set($"{this.cacheKeyPrefix}::{cacheKey}", value, itemSize);
	}

	bool ICacheImplementation.TryGet<T>(string cacheKey, out T value)
	{
		return this.inner.TryGet($"{this.cacheKeyPrefix}::{cacheKey}", out value);
	}

}
