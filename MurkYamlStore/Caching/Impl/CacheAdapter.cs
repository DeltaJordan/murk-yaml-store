using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using MurkYamlStore.Core;
using MurkYamlStore.Util;

namespace MurkYamlStore.Caching.Impl;

internal sealed class CacheAdapter : ICache
{

	private readonly ICacheImplementation m_inner;

	public CacheAdapter(
		ICacheImplementation inner
	)
	{
		this.m_inner = inner;
	}

	async Task<T> ICache.GetAsync<T>(
		string cacheKey,
		Func<Task<T>> getter,
		CacheItemSize itemSize
	)
	{
		using (await StringLock.AcquireAsync(cacheKey).SafeAsync())
		{
			if (this.TryGetValue(cacheKey, out T value))
			{
				return value;
			}

			value = await getter().SafeAsync();
			this.StoreValue(cacheKey, value, DefaultCacheItemSizer<T>.GetSize(value, itemSize));
			return value;
		}
	}

	async Task<T> ICache.GetAsync<T>(
		string cacheKey,
		Func<Task<T>> getter,
		Func<T, int> itemSizer
	)
	{
		using (await StringLock.AcquireAsync(cacheKey).SafeAsync())
		{
			if (this.TryGetValue(cacheKey, out T value))
			{
				return value;
			}

			value = await getter().SafeAsync();
			this.StoreValue(cacheKey, value, itemSizer(value));
			return value;
		}
	}

	async Task ICache.RemoveAsync(string cacheKey)
	{
		using (await StringLock.AcquireAsync(cacheKey).SafeAsync())
		{
			this.m_inner.Remove(cacheKey);
		}
	}

	async Task ICache.SetAsync<T>(string cacheKey, T value, CacheItemSize itemSize)
	{
		using (await StringLock.AcquireAsync(cacheKey).SafeAsync())
		{
			this.StoreValue(cacheKey, value, DefaultCacheItemSizer<T>.GetSize(value, itemSize));
		}
	}

	async Task<TryResult<T>> ICache.TryGetAsync<T>(string cacheKey)
	{
		using (await StringLock.AcquireAsync(cacheKey).SafeAsync())
		{
			if (this.TryGetValue(cacheKey, out T value))
			{
				return new TryResult<T>(value);
			}
			return new TryResult<T>();
		}
	}

	private bool TryGetValue<T>(
		string cacheKey,
		out T value
	)
	{
		// If requesting a TryResult<>, wrap the value in a TryResult
		if (typeof(T).IsAssignableTo(typeof(ITryResult)))
		{
			if (this.m_inner.TryGet(cacheKey, out object innerValue))
			{
				value = TryResultCacheHelper<T>.Wrap(innerValue);
				return value != null;
			}
			value = default;
			return false;
		}
		return this.m_inner.TryGet(cacheKey, out value);
	}

	private void StoreValue<T>(
		string cacheKey,
		T value,
		int itemSize
	)
	{
		// Store the inner value of a TryResult<> (or don't store anything if it's empty)
		if (value is ITryResult tryResult)
		{
			if (tryResult.HasValue) this.StoreValue(cacheKey, tryResult.Value, itemSize);
			return;
		}

		this.m_inner.Set(cacheKey, value, itemSize);
	}

	private static class TryResultCacheHelper<T>
	{

		private readonly static Func<object, T> s_wrapper;
		private readonly static Type s_innerType;
		private readonly static bool s_canBeNull;

		static TryResultCacheHelper()
		{
			if (
				!typeof(T).IsGenericType ||
				typeof(T).GetGenericTypeDefinition() != typeof(TryResult<>)
			)
			{
				s_wrapper = (x) => (T)x;
				s_innerType = typeof(T);
				s_canBeNull = true;
			}

			s_innerType = typeof(T).GenericTypeArguments[0];
			s_canBeNull = !s_innerType.IsValueType || s_innerType.IsGenericType && s_innerType.GetGenericTypeDefinition() == typeof(Nullable<>);

			ConstructorInfo constructor = typeof(T).GetConstructor(
				new Type[] { s_innerType }
			);

			ParameterExpression input = Expression.Parameter(typeof(object));
			s_wrapper = Expression.Lambda<Func<object, T>>(
				Expression.New(constructor, new[]{
					Expression.Convert( input, s_innerType )
				}),
				input
			).Compile();
		}

		public static T Wrap(object value)
		{
			if (value == null)
			{
				return s_canBeNull ? s_wrapper(null) : default;
			}

			if (!value.GetType().IsAssignableTo(s_innerType))
			{
				return default;
			}

			return s_wrapper(value);
		}

	}

}
