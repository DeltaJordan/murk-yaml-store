using System.Collections.Generic;

namespace MurkYamlStore.Caching.Impl;

internal sealed class LayeredCache : ICacheImplementation
{

	private readonly IReadOnlyList<ICacheImplementation> cacheLayers;

	public LayeredCache(
		IEnumerable<ICacheImplementation> layers
	)
	{
		this.cacheLayers = layers.CoerceToIReadOnlyList();
	}

	bool ICacheImplementation.TryGet<T>(string cacheKey, out T value)
	{
		foreach (ICacheImplementation layer in this.cacheLayers)
		{
			if (layer.TryGet(cacheKey, out value))
			{
				return true;
			}
		}

		value = default;
		return false;
	}

	void ICacheImplementation.Set<T>(string cacheKey, T value, int itemSize)
	{
		foreach (ICacheImplementation layer in this.cacheLayers)
		{
			layer.Set(cacheKey, value, itemSize);
		}
	}

	void ICacheImplementation.Remove(string cacheKey)
	{
		foreach (ICacheImplementation layer in this.cacheLayers)
		{
			layer.Remove(cacheKey);
		}
	}

}
