using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using MurkYamlStore.Core;

namespace MurkYamlStore.Caching;

[EditorBrowsable(EditorBrowsableState.Never)]
public static class CacheExtensions
{

	public static async Task<IDictionary<K, T>> GetBulkAsync<K, T>(
		this ICache cache,
		IEnumerable<K> keys,
		Func<K, string> cacheKeyGenerator,
		Func<IEnumerable<K>, Task<IDictionary<K, T>>> getter,
		CacheItemSize itemSize = CacheItemSize.Small
	)
	{
		IDictionary<K, T> results = new Dictionary<K, T>();
		List<K> keysToFetch = [];

		foreach (K key in keys)
		{
			TryResult<T> cachedValue = await cache.TryGetAsync<T>(cacheKeyGenerator(key)).SafeAsync();
			if (cachedValue.HasValue)
			{
				results.Add(key, cachedValue.Value);
			}
			else
			{
				keysToFetch.Add(key);
			}
		}

		if (keysToFetch.Count == 0)
		{
			return results;
		}

		IDictionary<K, T> fetchedValues = await getter(keysToFetch).SafeAsync();
		foreach (KeyValuePair<K, T> kvp in fetchedValues)
		{
			await cache.SetAsync(cacheKeyGenerator(kvp.Key), kvp.Value, itemSize).SafeAsync();
			results.Add(kvp);
		}

		return results;
	}

	public static async Task<IReadOnlyDictionary<K, T>> GetBulkAsync<K, T>(
		this ICache cache,
		IEnumerable<K> keys,
		Func<K, string> cacheKeyGenerator,
		Func<IEnumerable<K>, Task<IReadOnlyDictionary<K, T>>> getter,
		CacheItemSize itemSize = CacheItemSize.Small
	)
	{
		Dictionary<K, T> results = [];
		List<K> keysToFetch = [];

		foreach (K key in keys)
		{
			TryResult<T> cachedValue = await cache.TryGetAsync<T>(cacheKeyGenerator(key)).SafeAsync();
			if (cachedValue.HasValue)
			{
				results.TryAdd(key, cachedValue.Value);
			}
			else
			{
				keysToFetch.Add(key);
			}
		}

		if (keysToFetch.Count == 0)
		{
			return results;
		}

		IReadOnlyDictionary<K, T> fetchedValues = await getter(keysToFetch).SafeAsync();
		foreach (KeyValuePair<K, T> kvp in fetchedValues)
		{
			await cache.SetAsync(cacheKeyGenerator(kvp.Key), kvp.Value, itemSize).SafeAsync();
			results.TryAdd(kvp.Key, kvp.Value);
		}

		return results;
	}

}
