namespace MurkYamlStore.Caching;

internal interface ICacheImplementation
{

	bool TryGet<T>(string cacheKey, out T value);
	void Set<T>(string cacheKey, T value, int itemSize);
	void Remove(string cacheKey);

}
