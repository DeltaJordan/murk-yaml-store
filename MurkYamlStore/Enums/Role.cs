using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace MurkYamlStore.Enums;


[JsonConverter(typeof(RoleJsonSerializer))]
public enum Role : int
{
    Banned = 0,
    User = 1,
    Moderator = 2,
    Staff = 3
}

[EditorBrowsable(EditorBrowsableState.Never)]
public static class Role_Extensions
{

    public static bool AtLeast(this Role? @this, Role other)
    {
        return @this.HasValue && (int)@this >= (int)other;
    }

    public static bool AtLeast(this Role @this, Role other)
    {
        return (int)@this >= (int)other;
    }

    public static string ToRoleString(this Role @this)
    {
        return @this switch
        {
            _ => @this.ToString(),
        };
    }

    public static string GetRoleColor(this Role @this)
    {
        return @this switch
        {
            Role.Banned => "red",
            Role.User => "white",
            Role.Moderator => "green",
            Role.Staff => "lime",
            _ => "white",
        };
    }
}

internal sealed class RoleJsonSerializer : JsonConverter<Role>
{

    public override Role ReadJson(
        JsonReader reader,
        Type objectType,
        Role existingValue,
        bool hasExistingValue,
        JsonSerializer serializer
    )
    {
        if (reader.TokenType != JsonToken.String) throw new JsonReaderException("Invalid role");
        string roleString = reader.Value as string;
        return Enum.Parse<Role>(roleString);
    }

    public override void WriteJson(
        JsonWriter writer,
        Role value,
        JsonSerializer serializer
    )
    {
        writer.WriteValue(value.ToRoleString());
    }

}
