namespace MurkYamlStore.Enums;

public enum Gender
{
    Masculine,
    Feminine,
    Neuter
}
