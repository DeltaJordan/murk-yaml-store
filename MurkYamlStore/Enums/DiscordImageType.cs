namespace MurkYamlStore.Enums;

public enum DiscordImageType
{
    UserAvatar,
    UserBanner
}