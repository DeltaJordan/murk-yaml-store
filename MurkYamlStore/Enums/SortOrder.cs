namespace MurkYamlStore.Enums;

public enum SortOrder
{
	Ascending,
	Descending
}
