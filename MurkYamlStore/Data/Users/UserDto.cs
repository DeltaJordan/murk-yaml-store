using System.Data;
using System.Drawing;
using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Data.Users;

public sealed class UserDto
{
    public DiscordRef Discord { get; }
    public Role Role { get; }
    public DateTime JoinedDate { get; }
    public string Bio { get; }
    public Gender? Gender { get; }
    public Color? BannerOverride { get; }


    private UserDto(IDataRecord row)
    {
        this.Discord = DiscordRef.DbReader(row);
        this.Role = row.Get<Role>("role");
        this.JoinedDate = row.Get<DateTime>("joined_date");
        this.Bio = row.Get<string>("bio");
        this.Gender = row.Get<Gender?>("gender");
        this.BannerOverride = row.Get<Color?>("banner_override");
    }

    public static UserDto Reader(IDataRecord row)
    {
        return new UserDto(row);
    }
}