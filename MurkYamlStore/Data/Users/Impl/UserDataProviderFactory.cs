using MurkYamlStore.Activation;
using MurkYamlStore.Caching;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Data.Users.Impl;

internal sealed class UserDataProviderFactory(IPostgresDatabase database, ICacheProvider cacheProvider) : IFactory<IUserDataProvider>
{

    private readonly IPostgresDatabase database = database;
    private readonly ICacheProvider cacheProvider = cacheProvider;

    IUserDataProvider IFactory<IUserDataProvider>.Create()
    {
        return new CachedUserDataProvider(
            new UserDataProvider(this.database),
            this.cacheProvider
        );
    }

}
