using System.Drawing;
using System.Runtime.CompilerServices;
using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.Pagination;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Data.Users.Impl;

internal sealed class UserDataProvider(IPostgresDatabase database) : IUserDataProvider
{
    private const string SELECT_USER = "SELECT user_id, username, display_name, avatar_hash, banner_hash, role, joined_date, bio, gender, banner_override FROM users";

    private readonly IPostgresDatabase database = database;

    async Task<DiscordId> IUserDataProvider.CreateNewUserAsync(DiscordRef newDiscord)
    {
        IPostgresTransaction transaction = await this.database.NewTransactionAsync().SafeAsync();
        await using ConfiguredAsyncDisposable handle = transaction.GetHandle();
        PostgresCommand cmd = new(@"
            INSERT INTO users(
                user_id,
                username,
                display_name,
                avatar_hash,
                banner_hash,
                joined_date,
                role
            ) VALUES (
                @userId,
                @username,
                @displayName,
                @avatarHash,
                @bannerHash,
                @joinedDate,
                @role::enum_role
            )");

        cmd.AddParameter("userId", newDiscord.UserId);
        cmd.AddParameter("username", newDiscord.Username);
        cmd.AddParameter("displayName", newDiscord.DisplayName);
        cmd.AddParameter("avatarHash", newDiscord.AvatarHash);
        cmd.AddParameter("bannerHash", newDiscord.BannerHash);
        cmd.AddParameter("joinedDate", DateTime.UtcNow);
        cmd.AddParameter("role", Role.User);

        await transaction.ExecNonQueryAsync(cmd).SafeAsync();
        await transaction.ExecNonQueryAsync(new PostgresCommand("REFRESH MATERIALIZED VIEW user_context")).SafeAsync();
        await transaction.CommitAsync().SafeAsync();
        return newDiscord.UserId;
    }

    Task<IReadOnlyList<string>> IUserDataProvider.GetAllUsernamesAsync()
    {
        PostgresCommand cmd = new(@"
            SELECT username FROM users
            ORDER BY username ASC"
        );

        return this.database.ExecReadColumnAsync<string>(cmd);
    }

    Task<IReadOnlyList<UserDto>> IUserDataProvider.GetModsAndStaffAsync()
    {
        PostgresCommand cmd = new(SELECT_USER + @" WHERE role = 'Moderator'::enum_role OR role = 'Staff'::enum_role");
        return this.database.ExecReadManyAsync(cmd, UserDto.Reader);
    }

    async Task<IReadOnlyDictionary<DiscordId, UserDto>> IUserDataProvider.GetUsersByIdAsync(IEnumerable<DiscordId> userIds)
    {
        PostgresCommand cmd = new(SELECT_USER + " WHERE user_id = ANY(@userIds)");
        cmd.AddParameter("userIds", userIds);

        IReadOnlyList<UserDto> users = await this.database.ExecReadManyAsync(cmd, UserDto.Reader).SafeAsync();
        return users.ToDictionary(user => user.Discord.UserId);
    }

    async Task<IReadOnlyList<UserDto>> IUserDataProvider.GetUsersPageAsync(int pageSize, BookmarkToken bookmark)
    {
        PostgresCommand cmd = new(SELECT_USER + @$"
            WHERE username > @username
            ORDER BY username ASC
            LIMIT {pageSize}",
            prepare: pageSize == 10
        );
        cmd.AddParameter("username", bookmark.HasValue ? bookmark.ReadNext<string>() : string.Empty);

        IReadOnlyList<UserDto> users = await this.database.ExecReadManyAsync(cmd, UserDto.Reader).SafeAsync();
        bookmark.Clear();
        if (users.Count >= pageSize)
        {
            bookmark.PushValue(users[^1].Discord.Username);
        }
        return users;
    }

    async Task<IReadOnlyDictionary<string, DiscordRef>> IUserDataProvider.ResolveUsernamesAsync(IEnumerable<string> usernames)
    {
        PostgresCommand cmd = new(@"
            SELECT 
                user_id,
                username,
                display_name,
                avatar_hash,
                banner_hash
            FROM users
            WHERE username = ANY(@usernames)"
        );
        cmd.AddParameter("usernames", usernames);

        IEnumerable<DiscordRef> dtos = await this.database.ExecReadManyAsync(cmd, DiscordRef.DbReader).SafeAsync();
        return dtos.ToDictionary(dto => dto.Username, dto => dto);
    }

    Task<IReadOnlyList<UserDto>> IUserDataProvider.SearchUsersPagedAsync(string searchTerm, int pageSize, BookmarkToken bookmark)
    {
        PostgresCommand cmd = new(SELECT_USER + " WHERE username ILIKE @search", prepare: false);
        cmd.AddParameter("search", $"%{this.database.EscapeLike(searchTerm)}%");

        if (bookmark.HasValue)
        {
            cmd.AppendSql(" AND username > :bookmark");
            cmd.AddParameter("bookmark", bookmark.ReadNext<string>());
        }

        cmd.AppendSql(" ORDER BY username ASC");
        cmd.AppendSql($" LIMIT {pageSize}");

        return this.database.ExecReadManyAsync(cmd, UserDto.Reader);
    }

    Task<TryResult<UserDto>> IUserDataProvider.TryGetUserByIdAsync(DiscordId userId)
    {
        PostgresCommand cmd = new(SELECT_USER + " WHERE user_id = @userId");
        cmd.AddParameter("userId", userId);

        return this.database.ExecTryReadFirstAsync(cmd, UserDto.Reader);
    }

    Task<TryResult<UserDto>> IUserDataProvider.TryGetUserByUsernameAsync(string username)
    {
        PostgresCommand cmd = new(SELECT_USER + " WHERE username = @username");
        cmd.AddParameter("username", username);

        return this.database.ExecTryReadFirstAsync(cmd, UserDto.Reader);
    }

    Task<TryResult<UserContextDto>> IUserDataProvider.TryGetUserContextAsync(DiscordId userId)
    {
        PostgresCommand cmd = new(@"SELECT * FROM user_context WHERE user_id = @userId");
        cmd.AddParameter("userId", userId);

        return this.database.ExecTryReadFirstAsync(cmd, UserContextDto.Reader);
    }

    async Task<bool> IUserDataProvider.UpdateUserAsync(
        DiscordRef discord, 
        Role role, 
        string bio, 
        Gender? gender, 
        Color? bannerOverride
    )
    {
        PostgresCommand cmd = new(@"
            UPDATE users SET
                role = @role::enum_role,
                bio = @bio,
                gender = @gender::enum_gender,
                banner_override = @bannerOverride
            WHERE user_id = @userId");
        cmd.AddParameter("role", role);
        cmd.AddParameter("bio", bio);
        cmd.AddParameter("gender", gender);
        cmd.AddParameter("bannerOverride", bannerOverride);
        cmd.AddParameter("userId", discord.UserId);
        
        if (await this.database.ExecNonQueryAsync(cmd).SafeAsync() > 0)
        {
            await this.database.ExecNonQueryAsync(new PostgresCommand("REFRESH MATERIALIZED VIEW user_context")).SafeAsync();
            return true;
        }

        return false;
    }

    async Task<bool> IUserDataProvider.UpdateDiscordContextAsync(DiscordRef newDiscord)
    {
        PostgresCommand cmd = new(@"
            UPDATE users SET
                username = @username,
                display_name = @displayName,
                avatar_hash = @avatarHash,
                banner_hash = @bannerHash
            WHERE user_id = @userId");
        cmd.AddParameter("username", newDiscord.Username);
        cmd.AddParameter("displayName", newDiscord.DisplayName);
        cmd.AddParameter("avatarHash", newDiscord.AvatarHash);
        cmd.AddParameter("bannerHash", newDiscord.BannerHash);
        cmd.AddParameter("userId", newDiscord.UserId);

        if (await this.database.ExecNonQueryAsync(cmd).SafeAsync() > 0)
        {
            await this.database.ExecNonQueryAsync(new PostgresCommand("REFRESH MATERIALIZED VIEW user_context")).SafeAsync();
            return true;
        }

        return false;
    }

    async Task<bool> IUserDataProvider.UpdateUserRoleAsync(DiscordRef discord, Role role)
    {
        PostgresCommand cmd = new(@"
            UPDATE users
            SET role = @role::enum_role
            WHERE user_id = @userId"
        );
        cmd.AddParameter("role", role);
        cmd.AddParameter("userId", discord.UserId);

        int rowsChanged = await this.database.ExecNonQueryAsync(cmd).SafeAsync();
        await this.database.ExecNonQueryAsync(new PostgresCommand("REFRESH MATERIALIZED VIEW user_context")).SafeAsync();
        return rowsChanged > 0;
    }
}