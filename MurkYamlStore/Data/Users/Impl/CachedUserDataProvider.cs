using System.Drawing;
using MurkYamlStore.Caching;
using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Data.Users.Impl;

internal class CachedUserDataProvider(IUserDataProvider innerDataProvider, ICacheProvider cacheProvider) : IUserDataProvider
{
    private readonly IUserDataProvider inner = innerDataProvider;
    private readonly ICache cache = cacheProvider.GetPersistentCache("User", TimeSpan.FromMinutes(2.0));

    async Task<DiscordId> IUserDataProvider.CreateNewUserAsync(DiscordRef discord)
    {
        DiscordId userId = await this.inner.CreateNewUserAsync(discord).SafeAsync();
        await this.cache.RemoveAsync("All::FirstPage").SafeAsync();
        return userId;
    }

    Task<TryResult<UserDto>> IUserDataProvider.TryGetUserByUsernameAsync(string username)
    {
        return this.cache.GetAsync(
            $"Name::{username.ToLower()}",
            () => this.inner.TryGetUserByUsernameAsync(username),
            CacheItemSize.Huge
        );
    }

    Task<IReadOnlyList<UserDto>> IUserDataProvider.GetUsersPageAsync(int pageSize, BookmarkToken bookmark)
    {
        if (pageSize == 10 && !bookmark.HasValue)
        {
            return this.cache.GetAsync(
                "All::FirstPage",
                () => this.inner.GetUsersPageAsync(pageSize, bookmark)
            );
        }

        return this.inner.GetUsersPageAsync(pageSize, bookmark);
    }

    Task<IReadOnlyList<UserDto>> IUserDataProvider.SearchUsersPagedAsync(string searchTerm, int pageSize, BookmarkToken bookmark)
    {
        return this.inner.SearchUsersPagedAsync(searchTerm, pageSize, bookmark);
    }

    Task<IReadOnlyDictionary<DiscordId, UserDto>> IUserDataProvider.GetUsersByIdAsync(IEnumerable<DiscordId> userIds)
    {
        return this.cache.GetBulkAsync(
            userIds,
            userId => $"Id::{userId}",
            this.inner.GetUsersByIdAsync,
            CacheItemSize.Small
        );
    }

    Task<IReadOnlyList<string>> IUserDataProvider.GetAllUsernamesAsync()
    {
        return this.cache.GetAsync("Usernames", this.inner.GetAllUsernamesAsync, CacheItemSize.Small);
    }

    Task<IReadOnlyList<UserDto>> IUserDataProvider.GetModsAndStaffAsync()
    {
        return this.inner.GetModsAndStaffAsync();
    }

    Task<IReadOnlyDictionary<string, DiscordRef>> IUserDataProvider.ResolveUsernamesAsync(IEnumerable<string> usernames)
    {
        return this.inner.ResolveUsernamesAsync(usernames);
    }

    Task<TryResult<UserDto>> IUserDataProvider.TryGetUserByIdAsync(DiscordId userId)
    {
        return this.cache.GetAsync(
            $"Id::{userId}",
            () => this.inner.TryGetUserByIdAsync(userId),
            CacheItemSize.Huge
        );
    }

    async Task<bool> IUserDataProvider.UpdateUserRoleAsync(DiscordRef discord, Role role)
    {
        bool success = await this.inner.UpdateUserRoleAsync(discord, role).SafeAsync();
        if (success)
        {
            await this.cache.RemoveAsync($"Context::{discord.UserId}").SafeAsync();
            await this.RemoveFromCacheAsync(discord, true).SafeAsync();
        }
        return success;
    }

    async Task<bool> IUserDataProvider.UpdateUserAsync(DiscordRef discord, Role role, string bio, Gender? gender, Color? bannerOverride)
    {
        bool success = await this.inner.UpdateUserAsync(discord, role, bio, gender, bannerOverride).SafeAsync();
        if (success)
        {
            await this.cache.RemoveAsync($"Context::{discord.UserId}").SafeAsync();
            await this.RemoveFromCacheAsync(discord, true).SafeAsync();
        }
        return success;
    }

    async Task<bool> IUserDataProvider.UpdateDiscordContextAsync(DiscordRef discord)
    {
        if (await this.inner.UpdateDiscordContextAsync(discord).SafeAsync())
        {
            await this.RemoveFromCacheAsync(discord, true).SafeAsync();
            await this.cache.RemoveAsync($"Context::{discord.UserId}").SafeAsync();
            return true;
        }

        return false;
    }

    Task<TryResult<UserContextDto>> IUserDataProvider.TryGetUserContextAsync(DiscordId userId)
    {
        return this.cache.GetAsync(
            $"Context::{userId}",
            () => this.inner.TryGetUserContextAsync(userId)
        );
    }

    private async Task RemoveFromCacheAsync(DiscordRef discord, bool roleChanged)
    {
        await this.cache.RemoveAsync($"Id::{discord.UserId}").SafeAsync();
        await this.cache.RemoveAsync($"Name::{discord.Username.ToLower()}").SafeAsync();
        await this.cache.RemoveAsync("All::FirstPage").SafeAsync();
        if (roleChanged) await this.cache.RemoveAsync("Usernames").SafeAsync();
    }
}