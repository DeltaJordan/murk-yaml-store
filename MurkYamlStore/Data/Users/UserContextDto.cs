using System.Data;
using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Data.Users;

public sealed class UserContextDto
{
    public DiscordRef Discord { get; }
    public Role Role { get; }

    private UserContextDto(IDataRecord row)
    {
        this.Discord = DiscordRef.DbReader(row);
        this.Role = row.Get<Role>("role");
    }

    public static UserContextDto Reader(IDataRecord row)
    {
        return new UserContextDto(row);
    }
}