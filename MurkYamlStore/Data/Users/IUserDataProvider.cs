using System.Drawing;
using System.Net;
using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Data.Users;

public interface IUserDataProvider
{
    Task<TryResult<UserContextDto>> TryGetUserContextAsync(DiscordId userId);

    Task<TryResult<UserDto>> TryGetUserByIdAsync(DiscordId userId);
    Task<TryResult<UserDto>> TryGetUserByUsernameAsync(string username);

    Task<IReadOnlyList<UserDto>> GetUsersPageAsync(int pageSize, BookmarkToken bookmark);
    Task<IReadOnlyList<UserDto>> SearchUsersPagedAsync(string searchTerm, int pageSize, BookmarkToken bookmark);

    Task<IReadOnlyDictionary<DiscordId, UserDto>> GetUsersByIdAsync(IEnumerable<DiscordId> userIds);

    Task<IReadOnlyList<string>> GetAllUsernamesAsync();
    Task<IReadOnlyList<UserDto>> GetModsAndStaffAsync();

    Task<DiscordId> CreateNewUserAsync(DiscordRef discord);

    Task<bool> UpdateUserRoleAsync(
        DiscordRef discord,
        Role role
    );
    
    Task<bool> UpdateUserAsync(
        DiscordRef discord,
        Role role,
        string bio,
        Gender? gender,
        Color? bannerOverride
    );

    Task<bool> UpdateDiscordContextAsync(DiscordRef discord);

    Task<IReadOnlyDictionary<string, DiscordRef>> ResolveUsernamesAsync(IEnumerable<string> usernames);
}