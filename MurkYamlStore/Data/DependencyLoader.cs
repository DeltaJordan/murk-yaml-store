using MurkYamlStore.Activation;
using MurkYamlStore.Data.Archis;
using MurkYamlStore.Data.Archis.Impl;
using MurkYamlStore.Data.Games;
using MurkYamlStore.Data.Games.Impl;
using MurkYamlStore.Data.News;
using MurkYamlStore.Data.News.Impl;
using MurkYamlStore.Data.Users;
using MurkYamlStore.Data.Users.Impl;
using MurkYamlStore.Data.Yamls;
using MurkYamlStore.Data.Yamls.Impl;

namespace MurkYamlStore.Data;

internal static class DependencyLoader
{
    public static void Load(IServiceCollection registry)
    {
        registry.AddFactory<IArchiDataProvider, ArchiDataProviderFactory>();
        registry.AddFactory<IGameDataProvider, GameDataProviderFactory>();
        registry.AddFactory<INewsDataProvider, NewsDataProviderFactory>();
        registry.AddFactory<IUserDataProvider, UserDataProviderFactory>();
        registry.AddFactory<IYamlDataProvider, YamlDataProviderFactory>();
    }
}