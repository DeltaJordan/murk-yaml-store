using System.Data;
using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Data.Yamls;

public sealed class YamlDto
{
    public Guid YamlId { get; private init; }
    public Guid ArchiId { get; private init; }
    public RichUserRef Player { get; private init; }
    public DateTime DateUploaded { get; private init; }
    public Guid GameId { get; private init; }
    public string FileName { get; private init; }

    public static YamlDto DbReader(IDataRecord row)
    {
        return new YamlDto
        {
            YamlId = row.Get<Guid>("yaml_id"),
            ArchiId = row.Get<Guid>("archi_id"),
            Player = new RichUserRef(
                row.Get<DiscordId>("player"),
                row.Get<string>("player_name"),
                row.Get<Gender?>("player_gender")
            ),
            DateUploaded = row.Get<DateTime>("date_uploaded"),
            GameId = row.Get<Guid>("game_id"),
            FileName = row.Get<string>("filename")
        };
    }
}