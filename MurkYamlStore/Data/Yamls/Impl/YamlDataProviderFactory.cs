using MurkYamlStore.Activation;
using MurkYamlStore.Caching;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Data.Yamls.Impl;

public class YamlDataProviderFactory(
    IPostgresDatabase database, 
    ICacheProvider cacheProvider
) : IFactory<IYamlDataProvider>
{
    IYamlDataProvider IFactory<IYamlDataProvider>.Create()
    {
        return new CachedYamlDataProvider(
            new YamlDataProvider(database),
            cacheProvider
        );
    }
}