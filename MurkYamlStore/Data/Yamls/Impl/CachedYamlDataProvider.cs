using MurkYamlStore.Caching;
using MurkYamlStore.Core;

namespace MurkYamlStore.Data.Yamls.Impl;

internal sealed class CachedYamlDataProvider(IYamlDataProvider inner, ICacheProvider cacheProvider) : IYamlDataProvider
{
    private readonly ICache cache = cacheProvider.GetPersistentCache("Yaml", TimeSpan.FromMinutes(10));
    
    Task<TryResult<YamlDto>> IYamlDataProvider.TryGetYamlAsync(Guid yamlId)
    {
        return this.cache.GetAsync(
            $"Single::{yamlId}",
            () => inner.TryGetYamlAsync(yamlId)
        );
    }

    Task<IReadOnlyList<YamlDto>> IYamlDataProvider.GetYamlsByArchiAsync(Guid archiId, DiscordId? filterUserId, Guid? filterGameId)
    {
        if (!filterUserId.HasValue && !filterGameId.HasValue)
        {
            return this.cache.GetAsync(
                $"Archi::{archiId}",
                () => inner.GetYamlsByArchiAsync(archiId),
                CacheItemSize.Big
            );
        }
        
        return inner.GetYamlsByArchiAsync(archiId, filterUserId, filterGameId);
    }

    Task<IReadOnlyList<YamlDto>> IYamlDataProvider.GetYamlsByUserAsync(DiscordId userId)
    {
        return this.cache.GetAsync(
            $"User::{userId}",
            () => inner.GetYamlsByUserAsync(userId)
        );
    }

    async Task<Guid> IYamlDataProvider.CreateYamlAsync(Guid archiId, DiscordId playerId, Guid gameId, string fileName)
    {
        Guid yamlId = await inner.CreateYamlAsync(archiId, playerId, gameId, fileName).SafeAsync();
        await this.cache.RemoveAsync($"Archi::{archiId}").SafeAsync();
        await this.cache.RemoveAsync($"User::{playerId}").SafeAsync();
        return yamlId;
    }

    async Task IYamlDataProvider.EditYamlAsync(Guid yamlId, string filename)
    {
        TryResult<YamlDto> dto = await inner.TryGetYamlAsync(yamlId).SafeAsync();
        if (dto.HasValue)
        {
            await inner.EditYamlAsync(yamlId, filename).SafeAsync();
            await this.cache.RemoveAsync($"Single::{yamlId}").SafeAsync();
            await this.cache.RemoveAsync($"Archi::{dto.Value.ArchiId}").SafeAsync();
            await this.cache.RemoveAsync($"User::{dto.Value.Player.UserId}").SafeAsync();
        }
    }

    async Task IYamlDataProvider.DeleteYamlAsync(Guid yamlId)
    {
        TryResult<YamlDto> dto = await inner.TryGetYamlAsync(yamlId).SafeAsync();
        if (dto.HasValue)
        {
            await inner.DeleteYamlAsync(yamlId).SafeAsync();
            await this.cache.RemoveAsync($"Single::{yamlId}").SafeAsync();
            await this.cache.RemoveAsync($"Archi::{dto.Value.ArchiId}").SafeAsync();
            await this.cache.RemoveAsync($"User::{dto.Value.Player.UserId}").SafeAsync();
        }
    }
}