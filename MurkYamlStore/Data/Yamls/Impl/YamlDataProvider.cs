using MurkYamlStore.Core;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Data.Yamls.Impl;

internal sealed class YamlDataProvider(
    IPostgresDatabase database
) : IYamlDataProvider
{
    Task<TryResult<YamlDto>> IYamlDataProvider.TryGetYamlAsync(Guid yamlId)
    {
        PostgresCommand cmd = new(@"SELECT * FROM yamls_ext WHERE yaml_id = @yamlId");
        cmd.AddParameter("yamlId", yamlId);
        return database.ExecTryReadFirstAsync(cmd, YamlDto.DbReader);
    }

    Task<IReadOnlyList<YamlDto>> IYamlDataProvider.GetYamlsByArchiAsync(Guid archiId, DiscordId? filterUserId, Guid? filterGameId)
    {
        PostgresCommand cmd = new(@"SELECT * FROM yamls_ext WHERE archi_id = @archiId");
        cmd.AddParameter("archiId", archiId);
        
        if (filterUserId.HasValue)
        {
            cmd.AppendSql(@" AND player = @userId");
            cmd.AddParameter("userId", filterUserId.Value);
        }

        if (filterGameId.HasValue)
        {
            cmd.AppendSql(@" AND game_id = @gameId");
            cmd.AddParameter("gameId", filterGameId.Value);
        }
        
        return database.ExecReadManyAsync(cmd, YamlDto.DbReader);
    }

    Task<IReadOnlyList<YamlDto>> IYamlDataProvider.GetYamlsByUserAsync(DiscordId userId)
    {
        PostgresCommand cmd = new(@"SELECT * FROM yamls_ext WHERE player = @userId");
        cmd.AddParameter("userId", userId);
        return database.ExecReadManyAsync(cmd, YamlDto.DbReader);
    }

    async Task<Guid> IYamlDataProvider.CreateYamlAsync(Guid archiId, DiscordId playerId, Guid gameId, string fileName)
    {
        Guid yamlId = Guid.NewGuid();
        
        PostgresCommand cmd = new(
            """
                INSERT INTO archi_yamls(yaml_id, archi_id, player, date_uploaded, game_id, filename)
                VALUES(@yamlId, @archiId, @player, @dateUploaded, @gameId, @filename)
            """
        );
        cmd.AddParameter("yamlId", yamlId);
        cmd.AddParameter("archiId", archiId);
        cmd.AddParameter("player", playerId);
        cmd.AddParameter("dateUploaded", DateTime.UtcNow);
        cmd.AddParameter("gameId", gameId);
        cmd.AddParameter("filename", fileName);
        
        await database.ExecNonQueryAsync(cmd).SafeAsync();
        return yamlId;
    }

    Task IYamlDataProvider.EditYamlAsync(Guid yamlId, string filename)
    {
        PostgresCommand cmd = new(
            """
                UPDATE archi_yamls SET 
                    filename = @filename,
                    date_uploaded = @dateUploaded
                WHERE yaml_id = @yamlId
            """
        );
        cmd.AddParameter("yamlId", yamlId);
        cmd.AddParameter("filename", filename);
        cmd.AddParameter("dateUploaded", DateTime.UtcNow);
        return database.ExecNonQueryAsync(cmd);
    }

    Task IYamlDataProvider.DeleteYamlAsync(Guid yamlId)
    {
        PostgresCommand cmd = new(@"DELETE FROM archi_yamls WHERE yaml_id = @yamlId");
        cmd.AddParameter("yamlId", yamlId);
        return database.ExecNonQueryAsync(cmd);
    }
}