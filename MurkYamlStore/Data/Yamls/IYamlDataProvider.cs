using MurkYamlStore.Core;

namespace MurkYamlStore.Data.Yamls;

public interface IYamlDataProvider
{
    Task<TryResult<YamlDto>> TryGetYamlAsync(Guid yamlId);
    
    Task<IReadOnlyList<YamlDto>> GetYamlsByArchiAsync(
        Guid archiId, 
        DiscordId? filterUserId = null, 
        Guid? filterGameId = null
    );
    Task<IReadOnlyList<YamlDto>> GetYamlsByUserAsync(DiscordId userId);
    
    Task<Guid> CreateYamlAsync(
        Guid archiId,
        DiscordId playerId,
        Guid gameId,
        string fileName
    );
    
    Task EditYamlAsync(
        Guid yamlId, 
        string filename
    );
    
    Task DeleteYamlAsync(Guid yamlId);
}