using System.Data;
using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Data.News;

public sealed class NewsDto
{
    public Guid NewsPostId { get; private init; }
    public RichUserRef Author { get; private init; }
    public DateTime DateCreated { get; private init; }
    public DateTime? DateEdited { get; private init; }
    public string Title { get; private init; }
    public string Message { get; private init; }
    public string Markdown { get; private init; }
    public Guid? PollId { get; private init; } // TODO: Unused until polls are added.

    public static NewsDto Reader(IDataRecord row)
    {
        return new NewsDto
        {
            NewsPostId = row.Get<Guid>("post_id"),
            Author = new RichUserRef(
                row.Get<DiscordId>("author"),
                row.Get<string>("author_name"),
                row.Get<Gender?>("author_gender")
            ),
            DateCreated = row.Get<DateTime>("date_created"),
            DateEdited = row.Get<DateTime?>("date_edited"),
            Title = row.Get<string>("title"),
            Message = row.Get<string>("message"),
            Markdown = row.Get<string>("markdown")
        };
    }

}
