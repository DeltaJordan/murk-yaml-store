using MurkYamlStore.Core;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Data.News;

public interface INewsDataProvider {
    Task<IReadOnlyList<NewsDto>> GetNewsPostsAsync();
    Task<IReadOnlyList<NewsDto>> GetNewsPostsPagedAsync( int pageSize, BookmarkToken bookmark );
    Task<TryResult<NewsDto>> TryGetNewsPostAsync( Guid postId );

    Task<Guid> CreateNewsPostAsync( DiscordId userId, string title, string content, string markdown );
    Task EditNewsPostAsync( Guid postId, string title, string content, string markdown );
    Task DeleteNewsPostAsync( Guid postId );
}