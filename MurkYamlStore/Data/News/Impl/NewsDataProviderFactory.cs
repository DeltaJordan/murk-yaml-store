using MurkYamlStore.Activation;
using MurkYamlStore.Caching;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Data.News.Impl;

internal sealed class NewsDataProviderFactory(
    IPostgresDatabase database,
    ICacheProvider cacheProvider
) : IFactory<INewsDataProvider>
{
    INewsDataProvider IFactory<INewsDataProvider>.Create() {
        return new CachedNewsDataProvider(
            new NewsDataProvider( database ),
            cacheProvider
        );
    }
}