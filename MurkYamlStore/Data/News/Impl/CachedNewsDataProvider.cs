using MurkYamlStore.Caching;
using MurkYamlStore.Core;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Data.News.Impl;

internal sealed class CachedNewsDataProvider(INewsDataProvider inner, ICacheProvider cacheProvider) : INewsDataProvider
{
    private readonly ICache cache = 
        cacheProvider.GetPersistentCache("News", TimeSpan.FromMinutes(10));
    
    Task<IReadOnlyList<NewsDto>> INewsDataProvider.GetNewsPostsAsync()
    {
        return this.cache.GetAsync(
            "All",
            inner.GetNewsPostsAsync
        );
    }

    Task<IReadOnlyList<NewsDto>> INewsDataProvider.GetNewsPostsPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        if( pageSize == 10 && bookmark is not { HasValue: true } ) {
            return this.cache.GetAsync(
                "FirstPage",
                () => inner.GetNewsPostsPagedAsync( pageSize, bookmark )
            );
        }

        return inner.GetNewsPostsPagedAsync( pageSize, bookmark );
    }

    Task<TryResult<NewsDto>> INewsDataProvider.TryGetNewsPostAsync(Guid postId)
    {
        return this.cache.GetAsync(
            $"Single::{postId}",
            () => inner.TryGetNewsPostAsync( postId )
        );
    }

    async Task<Guid> INewsDataProvider.CreateNewsPostAsync(DiscordId userId, string title, string content, string markdown)
    {
        Guid postId = await inner.CreateNewsPostAsync( userId, title, content, markdown ).SafeAsync();
        await this.cache.RemoveAsync( "All" ).SafeAsync();
        await this.cache.RemoveAsync( "FirstPage" ).SafeAsync();
        await this.cache.RemoveAsync( $"Single::{postId}" ).SafeAsync();
        return postId;
    }

    async Task INewsDataProvider.EditNewsPostAsync(Guid postId, string title, string content, string markdown)
    {
        await inner.EditNewsPostAsync( postId, title, content, markdown ).SafeAsync();
        await this.cache.RemoveAsync( "All" ).SafeAsync();
        await this.cache.RemoveAsync( "FirstPage" ).SafeAsync();
        await this.cache.RemoveAsync( $"Single::{postId}" ).SafeAsync();
    }

    async Task INewsDataProvider.DeleteNewsPostAsync(Guid postId)
    {
        await inner.DeleteNewsPostAsync( postId ).SafeAsync();
        await this.cache.RemoveAsync( "All" ).SafeAsync();
        await this.cache.RemoveAsync( "FirstPage" ).SafeAsync();
        await this.cache.RemoveAsync( $"Single::{postId}" ).SafeAsync();
    }
}