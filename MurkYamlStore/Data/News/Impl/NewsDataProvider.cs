using MurkYamlStore.Core;
using MurkYamlStore.Pagination;
using MurkYamlStore.Postgres;
using NpgsqlTypes;

namespace MurkYamlStore.Data.News.Impl;

internal sealed class NewsDataProvider(IPostgresDatabase database) : INewsDataProvider
{
    Task<IReadOnlyList<NewsDto>> INewsDataProvider.GetNewsPostsAsync()
    {
        var cmd = new PostgresCommand( "SELECT * FROM news_posts_ext ORDER BY date_created DESC" );
        return database.ExecReadManyAsync( cmd, NewsDto.Reader );
    }

    Task<IReadOnlyList<NewsDto>> INewsDataProvider.GetNewsPostsPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        var cmd = new PostgresCommand( @$"
				SELECT * FROM news_posts_ext
				WHERE date_created < @bookmark
				ORDER BY date_created DESC
				LIMIT {pageSize}",
            prepare: pageSize == 10
        );
            
        if( bookmark != null && bookmark.HasValue ) {
            cmd.AddParameter("bookmark", bookmark.ReadNext<DateTime>());
        } else {
            cmd.AddParameter("bookmark", NpgsqlDateTime.Infinity);
        }

        return database.ExecReadManyAsync(cmd, NewsDto.Reader);
    }

    Task<TryResult<NewsDto>> INewsDataProvider.TryGetNewsPostAsync(Guid postId)
    {
        var cmd = new PostgresCommand( "SELECT * FROM news_posts_ext WHERE post_id = @postId" );
        cmd.AddParameter( "postId", postId );
        return database.ExecTryReadFirstAsync( cmd, NewsDto.Reader );
    }

    async Task<Guid> INewsDataProvider.CreateNewsPostAsync(DiscordId userId, string title, string content, string markdown)
    {
        Guid postId = Guid.NewGuid();

        var cmd = new PostgresCommand( @"
				INSERT INTO news_posts( post_id, author, date_created, date_edited, title, message, markdown )
				VALUES( @postId, @author, @dateCreated, NULL, @title, @message, @markdown )"
        );
        cmd.AddParameter( "postId", postId );
        cmd.AddParameter( "author", userId );
        cmd.AddParameter( "dateCreated", DateTime.UtcNow );
        cmd.AddParameter( "title", title );
        cmd.AddParameter( "message", content );
        cmd.AddParameter( "markdown", markdown );

        await database.ExecNonQueryAsync( cmd ).SafeAsync();
        return postId;
    }

    Task INewsDataProvider.EditNewsPostAsync(Guid postId, string title, string content, string markdown)
    {
        var cmd = new PostgresCommand( @"
				UPDATE news_posts SET
					title = @title,
					message = @message,
					markdown = @markdown,
					date_edited = @dateEdited
				WHERE post_id = @postId"
        );
        cmd.AddParameter( "postId", postId );
        cmd.AddParameter( "title", title );
        cmd.AddParameter( "message", content );
        cmd.AddParameter( "markdown", markdown );
        cmd.AddParameter( "dateEdited", DateTime.UtcNow );
        return database.ExecNonQueryAsync( cmd );
    }

    Task INewsDataProvider.DeleteNewsPostAsync(Guid postId)
    {
        var cmd = new PostgresCommand( "DELETE FROM news_posts WHERE post_id = @postId" );
        cmd.AddParameter( "postId", postId );
        return database.ExecNonQueryAsync( cmd );
    }
}