using MurkYamlStore.Core;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Data.Archis;

public interface IArchiDataProvider
{
    Task<IReadOnlyList<ArchiDto>> GetArchisAsync();
    Task<IReadOnlyList<ArchiDto>> GetArchisPagedAsync(int pageSize, BookmarkToken bookmark);
    Task<TryResult<ArchiDto>> TryGetArchiAsync(Guid archiId);
    
    Task<IReadOnlyList<ArchiDto>> GetPastArchisPagedAsync(int pageSize, BookmarkToken bookmark);
    Task<IReadOnlyList<ArchiDto>> GetActiveArchisPagedAsync(int pageSize, BookmarkToken bookmark);
    Task<IReadOnlyList<ArchiDto>> GetUpcomingArchisPagedAsync(int pageSize, BookmarkToken bookmark);
    
    Task<long> GetArchiPlayerCountAsync(Guid archiId);
    Task<long> GetArchiYamlCountAsync(Guid archiId);
    
    Task<Guid> CreateArchiAsync(
        DiscordId authorId,
        string title,
        string description,
        DateTime submissionDeadline,
        DateTime startDate,
        bool? isAsync,
        string archiIp = null,
        ushort? archiPort = null
    );
    
    Task EditArchiAsync(
        Guid archiId, 
        string title,
        string description,
        string icon,
        DateTime submissionDeadline,
        DateTime startDate,
        DateTime? completedDate,
        string seedFileName,
        bool? isAsync,
        string archiIp,
        ushort? archiPort
    );
    
    Task DeleteArchiAsync(Guid archId);
}