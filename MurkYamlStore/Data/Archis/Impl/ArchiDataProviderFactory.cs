using MurkYamlStore.Activation;
using MurkYamlStore.Caching;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Data.Archis.Impl;

internal sealed class ArchiDataProviderFactory(
    IPostgresDatabase database,
    ICacheProvider cacheProvider
) : IFactory<IArchiDataProvider>
{
    IArchiDataProvider IFactory<IArchiDataProvider>.Create()
    {
        return new CachedArchiDataProvider(
            new ArchiDataProvider(database),
            cacheProvider
        );
    }
}