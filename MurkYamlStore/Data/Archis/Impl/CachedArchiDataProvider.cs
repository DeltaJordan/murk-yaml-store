using MurkYamlStore.Caching;
using MurkYamlStore.Core;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Data.Archis.Impl;

internal sealed class CachedArchiDataProvider(
    IArchiDataProvider inner, 
    ICacheProvider cacheProvider
) : IArchiDataProvider
{
    private readonly ICache cache =
        cacheProvider.GetPersistentCache("Archi", TimeSpan.FromMinutes(10));
    
    Task<IReadOnlyList<ArchiDto>> IArchiDataProvider.GetArchisAsync()
    {
        return this.cache.GetAsync(
            "All",
            inner.GetArchisAsync
        );
    }

    Task<IReadOnlyList<ArchiDto>> IArchiDataProvider.GetArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        if (pageSize == 10 && bookmark is not { HasValue: true }) {
            return this.cache.GetAsync(
                "FirstPage",
                () => inner.GetArchisPagedAsync(pageSize, bookmark)
            );
        }

        return inner.GetArchisPagedAsync(pageSize, bookmark);
    }

    Task<TryResult<ArchiDto>> IArchiDataProvider.TryGetArchiAsync(Guid archiId)
    {
        return this.cache.GetAsync(
            $"Single::{archiId}",
            () => inner.TryGetArchiAsync(archiId)
        );
    }

    Task<IReadOnlyList<ArchiDto>> IArchiDataProvider.GetPastArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        if (pageSize == 10 && bookmark is not { HasValue: true }) {
            return this.cache.GetAsync(
                "FirstPage::Past",
                () => inner.GetPastArchisPagedAsync(pageSize, bookmark)
            );
        }

        return inner.GetPastArchisPagedAsync(pageSize, bookmark);
    }

    Task<IReadOnlyList<ArchiDto>> IArchiDataProvider.GetActiveArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        if (pageSize == 10 && bookmark is not { HasValue: true }) {
            return this.cache.GetAsync(
                "FirstPage::Active",
                () => inner.GetActiveArchisPagedAsync(pageSize, bookmark)
            );
        }

        return inner.GetActiveArchisPagedAsync(pageSize, bookmark);
    }

    Task<IReadOnlyList<ArchiDto>> IArchiDataProvider.GetUpcomingArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        if (pageSize == 10 && bookmark is not { HasValue: true }) {
            return this.cache.GetAsync(
                "FirstPage::Future",
                () => inner.GetUpcomingArchisPagedAsync(pageSize, bookmark)
            );
        }

        return inner.GetUpcomingArchisPagedAsync(pageSize, bookmark);
    }

    Task<long> IArchiDataProvider.GetArchiPlayerCountAsync(Guid archiId)
    {
        return inner.GetArchiPlayerCountAsync(archiId);
    }

    Task<long> IArchiDataProvider.GetArchiYamlCountAsync(Guid archiId)
    {
        return inner.GetArchiYamlCountAsync(archiId);
    }

    async Task<Guid> IArchiDataProvider.CreateArchiAsync(
        DiscordId authorId, 
        string title, 
        string description, 
        DateTime submissionDeadline,
        DateTime startDate, 
        bool? isAsync, 
        string archiIp, 
        ushort? archiPort
    )
    {
        Guid postId = await inner.CreateArchiAsync( 
            authorId, 
            title, 
            description, 
            submissionDeadline,
            startDate, 
            isAsync, 
            archiIp, 
            archiPort 
        ).SafeAsync();
        await this.RemoveFromCacheAsync(postId).SafeAsync();
        return postId;
    }

    async Task IArchiDataProvider.EditArchiAsync(
        Guid archiId, 
        string title, 
        string description, 
        string icon,
        DateTime submissionDeadline,
        DateTime startDate,
        DateTime? completedDate,
        string seedFileName, 
        bool? isAsync, 
        string archiIp, 
        ushort? archiPort
    )
    {
        await inner.EditArchiAsync(
            archiId, 
            title, 
            description, 
            icon, 
            submissionDeadline,
            startDate, 
            completedDate, 
            seedFileName,
            isAsync,
            archiIp,
            archiPort
        ).SafeAsync();
        await this.RemoveFromCacheAsync(archiId).SafeAsync();
    }

    async Task IArchiDataProvider.DeleteArchiAsync(Guid archId)
    {
        await inner.DeleteArchiAsync(archId).SafeAsync();
        await this.RemoveFromCacheAsync(archId).SafeAsync();
    }

    private async Task RemoveFromCacheAsync(Guid archiId)
    {
        await this.cache.RemoveAsync( "All" ).SafeAsync();
        await this.cache.RemoveAsync( "FirstPage" ).SafeAsync();
        await this.cache.RemoveAsync( "FirstPage::Past" ).SafeAsync();
        await this.cache.RemoveAsync( "FirstPage::Active" ).SafeAsync();
        await this.cache.RemoveAsync( "FirstPage::Future" ).SafeAsync();
        await this.cache.RemoveAsync( $"Single::{archiId}" ).SafeAsync();
    }
}