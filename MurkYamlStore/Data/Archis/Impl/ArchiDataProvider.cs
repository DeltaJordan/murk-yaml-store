using MurkYamlStore.Core;
using MurkYamlStore.Pagination;
using MurkYamlStore.Postgres;
using NpgsqlTypes;

namespace MurkYamlStore.Data.Archis.Impl;

internal sealed class ArchiDataProvider(IPostgresDatabase database) : IArchiDataProvider
{
    Task<IReadOnlyList<ArchiDto>> IArchiDataProvider.GetArchisAsync()
    {
        PostgresCommand cmd = new(@"SELECT * FROM archis_ext ORDER BY date_created DESC");
        return database.ExecReadManyAsync(cmd, ArchiDto.DbReader);
    }

    Task<IReadOnlyList<ArchiDto>> IArchiDataProvider.GetArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        PostgresCommand cmd = new($"""
               SELECT * FROM archis_ext
               WHERE date_created < @bookmark
               ORDER BY date_created DESC
               LIMIT {pageSize}
            """,
            prepare: pageSize == 10
        );

        if (bookmark != null && bookmark.HasValue)
        {
            cmd.AddParameter("bookmark", bookmark.ReadNext<DateTime>());
        }
        else
        {
            cmd.AddParameter("bookmark", NpgsqlDateTime.Infinity);
        }
        
        return database.ExecReadManyAsync(cmd, ArchiDto.DbReader);
    }

    Task<TryResult<ArchiDto>> IArchiDataProvider.TryGetArchiAsync(Guid archiId)
    {
        PostgresCommand cmd = new(@"SELECT * FROM archis_ext WHERE archi_id = @archiId");
        cmd.AddParameter("archiId", archiId);
        return database.ExecTryReadFirstAsync(cmd, ArchiDto.DbReader);
    }

    Task<IReadOnlyList<ArchiDto>> IArchiDataProvider.GetPastArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        PostgresCommand cmd = new($"""
                SELECT * FROM archis_ext
                WHERE date_created < @bookmark
                AND date_completed IS NOT NULL
                ORDER BY start_date DESC
                LIMIT {pageSize}
            """,
            prepare: pageSize == 10
        );

        if (bookmark != null && bookmark.HasValue)
        {
            cmd.AddParameter("bookmark", bookmark.ReadNext<DateTime>());
        }
        else
        {
            cmd.AddParameter("bookmark", NpgsqlDateTime.Infinity);
        }
        
        return database.ExecReadManyAsync(cmd, ArchiDto.DbReader);
    }

    Task<IReadOnlyList<ArchiDto>> IArchiDataProvider.GetActiveArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        PostgresCommand cmd = new($"""
                SELECT * FROM archis_ext
                WHERE date_created < @bookmark
                AND date_completed IS NULL
                AND start_date <= @now
                ORDER BY start_date ASC
                LIMIT {pageSize}
            """,
            prepare: pageSize == 10
        );
        
        cmd.AddParameter("now", DateTime.UtcNow);

        if (bookmark != null && bookmark.HasValue)
        {
            cmd.AddParameter("bookmark", bookmark.ReadNext<DateTime>());
        }
        else
        {
            cmd.AddParameter("bookmark", NpgsqlDateTime.Infinity);
        }
        
        return database.ExecReadManyAsync(cmd, ArchiDto.DbReader);
    }

    Task<IReadOnlyList<ArchiDto>> IArchiDataProvider.GetUpcomingArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        PostgresCommand cmd = new($"""
                SELECT * FROM archis_ext
                WHERE date_created < @bookmark
                AND start_date > @now
                ORDER BY start_date ASC
                LIMIT {pageSize}
            """,
            prepare: pageSize == 10
        );
        
        cmd.AddParameter("now", DateTime.UtcNow);

        if (bookmark != null && bookmark.HasValue)
        {
            cmd.AddParameter("bookmark", bookmark.ReadNext<DateTime>());
        }
        else
        {
            cmd.AddParameter("bookmark", NpgsqlDateTime.Infinity);
        }
        
        return database.ExecReadManyAsync(cmd, ArchiDto.DbReader);
    }

    Task<long> IArchiDataProvider.GetArchiPlayerCountAsync(Guid archiId)
    {
        PostgresCommand cmd = new(
            """
                SELECT COUNT(*) FROM (
                    SELECT DISTINCT player FROM archi_yamls
                    WHERE archi_id = @archiId
                )
            """
        );
        cmd.AddParameter("archiId", archiId);

        return database.ExecReadScalarAsync<long>(cmd);
    }

    Task<long> IArchiDataProvider.GetArchiYamlCountAsync(Guid archiId)
    {
        PostgresCommand cmd = new(
            """
                SELECT COUNT(*) FROM (
                    SELECT yaml_id FROM archi_yamls
                    WHERE archi_id = @archiId
                )
            """
        );
        cmd.AddParameter("archiId", archiId);

        return database.ExecReadScalarAsync<long>(cmd);
    }

    async Task<Guid> IArchiDataProvider.CreateArchiAsync(
        DiscordId authorId,
        string title,
        string description, 
        DateTime submissionDeadline,
        DateTime startDate,
        bool? isAsync,
        string archiIp,
        ushort? archiPort
    ) {
        Guid archiId = Guid.NewGuid();

        PostgresCommand cmd = new(
            """
                INSERT INTO archis(
                    archi_id, 
                    author, 
                    title, 
                    description, 
                    date_created, 
                    submission_deadline,
                    start_date, 
                    is_async, 
                    archi_ip, 
                    archi_port
                ) VALUES (
                    @archiId, 
                    @author, 
                    @title, 
                    @description, 
                    @dateCreated, 
                    @submissionDeadline,
                    @startDate, 
                    @isAsync, 
                    @archiIp, 
                    @archiPort
                )
            """
        );
        
        cmd.AddParameter("archiId", archiId);
        cmd.AddParameter("author", authorId);
        cmd.AddParameter("title", title);
        cmd.AddParameter("description", description);
        cmd.AddParameter("dateCreated", DateTime.UtcNow);
        cmd.AddParameter("submissionDeadline", submissionDeadline);
        cmd.AddParameter("startDate", startDate);
        cmd.AddParameter("isAsync", isAsync);
        cmd.AddParameter("archiIp", archiIp);
        cmd.AddParameter("archiPort", archiPort);
        
        await database.ExecNonQueryAsync(cmd).SafeAsync();
        return archiId;
    }

    Task IArchiDataProvider.EditArchiAsync(
        Guid archiId, 
        string title, 
        string description, 
        string icon, 
        DateTime submissionDeadline,
        DateTime startDate,
        DateTime? completedDate,
        string seedFileName,
        bool? isArchi,
        string archiIp,
        ushort? archiPort
    ) {
        PostgresCommand cmd = new(
            """
                UPDATE archis SET
                    title = @title,
                    description = @description,
                    icon = @icon,
                    submission_deadline = @submissionDeadline,
                    start_date = @startDate,
                    date_completed = @completedDate,
                    seed_filename = @seedFileName,
                    is_async = @isArchi,
                    archi_ip = @archiIp,
                    archi_port = @archiPort
                WHERE archi_id = @archiId
            """
        );
        cmd.AddParameter("title", title);
        cmd.AddParameter("description", description);
        cmd.AddParameter("icon", icon);
        cmd.AddParameter("submissionDeadline", submissionDeadline);
        cmd.AddParameter("startDate", startDate);
        cmd.AddParameter("completedDate", completedDate);
        cmd.AddParameter("seedFileName", seedFileName);
        cmd.AddParameter("isArchi", isArchi);
        cmd.AddParameter("archiIp", archiIp);
        cmd.AddParameter("archiPort", archiPort);
        cmd.AddParameter("archiId", archiId);
        return database.ExecNonQueryAsync(cmd);
    }

    Task IArchiDataProvider.DeleteArchiAsync(Guid archId)
    {
        PostgresCommand cmd = new(@"DELETE FROM archis WHERE archi_id = @archiId");
        cmd.AddParameter("archiId", archId);
        return database.ExecNonQueryAsync(cmd);
    }
}