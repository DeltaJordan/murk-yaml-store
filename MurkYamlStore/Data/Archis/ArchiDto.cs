using System.Data;
using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Data.Archis;

public class ArchiDto
{
    public Guid ArchiId { get; private init; }
    public RichUserRef Author { get; private init; }
    public string Title { get; private init; }
    public string Description { get; private init; }
    public string Icon { get; private init; }
    public DateTime DateCreated { get; private init; }
    public DateTime StartDate { get; private init; }
    public DateTime SubmissionDeadline { get; private init; }
    public DateTime? DateCompleted { get; private init; }
    public string SeedFileName { get; private init; }
    
#pragma warning disable MURK003
    public bool? IsAsync { get; private init; }
#pragma warning restore MURK003
    
    public string ArchiIp { get; private init; }
    public ushort? ArchiPort { get; private init; }

    public static ArchiDto DbReader(IDataRecord row)
    {
        return new ArchiDto
        {
            ArchiId = row.Get<Guid>("archi_id"),
            Author = new RichUserRef(
                row.Get<DiscordId>("author"),
                row.Get<string>("author_name"),
                row.Get<Gender?>("author_gender")
            ),
            Title = row.Get<string>("title"),
            Description = row.Get<string>("description"),
            Icon = row.Get<string>("icon"),
            DateCreated = row.Get<DateTime>("date_created"),
            StartDate = row.Get<DateTime>("start_date"),
            SubmissionDeadline = row.Get<DateTime>("submission_deadline"),
            DateCompleted = row.Get<DateTime?>("date_completed"),
            SeedFileName = row.Get<string>("seed_filename"),
            IsAsync = row.Get<bool?>("is_async"),
            ArchiIp = row.Get<string>("archi_ip"),
            ArchiPort = row.Get<ushort?>("archi_port")
        };
    }
}