using System.Data;
using System.Diagnostics.CodeAnalysis;
using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Data.Games;

public sealed class GameDto
{
    public Guid GameId { get; private init; }
    public Guid ArchiId { get; private init; }
    public RichUserRef Uploader { get; private init; }
    public string GameName { get; private init; }
    public bool IsOfficial { get; private init; }
    
    /// <summary>
    /// Null when <see cref="IsOfficial"/> is true.
    /// </summary>
    public string FileName { get; private init; }
    public string Icon { get; private init; }

    public static GameDto DbReader(IDataRecord row)
    {
        return new GameDto
        {
            GameId = row.Get<Guid>("game_id"),
            ArchiId = row.Get<Guid>("archi_id"),
            Uploader = new RichUserRef(
                row.Get<DiscordId>("uploader"),
                row.Get<string>("uploader_name"),
                row.Get<Gender?>("uploader_gender")
            ),
            GameName = row.Get<string>("name"),
            IsOfficial = row.Get<bool>("is_official"),
            FileName = row.Get<string>("filename"),
            Icon = row.Get<string>("icon")
        };
    }
}