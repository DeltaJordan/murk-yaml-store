using MurkYamlStore.Core;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Data.Games.Impl;

internal sealed class GameDataProvider(IPostgresDatabase database) : IGameDataProvider
{
    Task<IReadOnlyList<GameDto>> IGameDataProvider.GetGamesAsync()
    {
        PostgresCommand cmd = new(@"SELECT * FROM games_ext ORDER BY date_uploaded DESC");
        return database.ExecReadManyAsync(cmd, GameDto.DbReader);
    }

    Task<TryResult<GameDto>> IGameDataProvider.TryGetGameAsync(Guid gameId)
    {
        PostgresCommand cmd = new(@"SELECT * FROM games_ext WHERE game_id = @gameId");
        cmd.AddParameter("gameId", gameId);
        return database.ExecTryReadFirstAsync(cmd, GameDto.DbReader);
    }

    Task<TryResult<GameDto>> IGameDataProvider.TryGetGameByNameAsync(Guid archiId, string name)
    {
        PostgresCommand cmd = new(
            """
                SELECT * FROM games_ext 
                WHERE name = @name
                AND archi_id = @archiId
            """
        );
        cmd.AddParameter("name", name);
        cmd.AddParameter("archiId", archiId);
        return database.ExecTryReadFirstAsync(cmd, GameDto.DbReader);
    }

    Task<IReadOnlyList<GameDto>> IGameDataProvider.GetGamesByArchiAsync(Guid archiId)
    {
        PostgresCommand cmd = new(@"SELECT * FROM games_ext WHERE archi_id = @archiId");
        cmd.AddParameter("archiId", archiId);
        return database.ExecReadManyAsync(cmd, GameDto.DbReader);
    }

    async Task<Guid> IGameDataProvider.CreateGameAsync(
        DiscordId uploaderId, 
        Guid archiId,
        string name, 
        bool isOfficial, 
        string fileName
    )
    {
        Guid gameId = Guid.NewGuid();
        
        PostgresCommand cmd = new(
            """
                INSERT INTO archi_games(
                    game_id, 
                    archi_id,
                    uploader, 
                    date_uploaded, 
                    name, 
                    is_official, 
                    filename
                ) VALUES (
                    @gameId,
                    @archiId,
                    @uploader,
                    @dateUploaded,
                    @name,
                    @isOfficial,
                    @filename
                )
            """
        );
        cmd.AddParameter("gameId", gameId);
        cmd.AddParameter("archiId", archiId);
        cmd.AddParameter("uploader", uploaderId);
        cmd.AddParameter("dateUploaded", DateTime.UtcNow);
        cmd.AddParameter("name", name);
        cmd.AddParameter("isOfficial", isOfficial);
        cmd.AddParameter("filename", fileName);

        await database.ExecNonQueryAsync(cmd).SafeAsync();
        return gameId;
    }

    Task IGameDataProvider.EditGameAsync(
        Guid gameId, 
        DiscordId editorId, 
        string name, 
        bool isOfficial, 
        string fileName, 
        string icon
    ) {
        PostgresCommand cmd = new(
            """
                UPDATE archi_games SET
                    uploader = @uploader,
                    date_uploaded = @dateUploaded,
                    name = @name,
                    is_official = @isOfficial,
                    filename = @filename,
                    icon = @icon
                WHERE game_id = @gameId
            """
        );
        cmd.AddParameter("uploader", editorId);
        cmd.AddParameter("dateUploaded", DateTime.UtcNow);
        cmd.AddParameter("name", name);
        cmd.AddParameter("isOfficial", isOfficial);
        cmd.AddParameter("filename", fileName);
        cmd.AddParameter("icon", icon);
        cmd.AddParameter("gameId", gameId);
        
        return database.ExecNonQueryAsync(cmd);
    }

    Task IGameDataProvider.DeleteGameAsync(Guid gameId)
    {
        PostgresCommand cmd = new("DELETE FROM archi_games WHERE game_id = @gameId");
        cmd.AddParameter("gameId", gameId);
        return database.ExecNonQueryAsync(cmd);
    }
}