using MurkYamlStore.Activation;
using MurkYamlStore.Caching;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Data.Games.Impl;

internal sealed class GameDataProviderFactory(
    IPostgresDatabase database,
    ICacheProvider cacheProvider
) : IFactory<IGameDataProvider>
{
    IGameDataProvider IFactory<IGameDataProvider>.Create()
    {
        return new CachedGameDataProvider(
            new GameDataProvider(database),
            cacheProvider
        );
    }
}