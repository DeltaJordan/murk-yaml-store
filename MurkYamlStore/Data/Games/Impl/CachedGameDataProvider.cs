using MurkYamlStore.Caching;
using MurkYamlStore.Core;

namespace MurkYamlStore.Data.Games.Impl;

internal sealed class CachedGameDataProvider(IGameDataProvider inner, ICacheProvider cacheProvider) : IGameDataProvider
{
    private readonly ICache cache = cacheProvider.GetPersistentCache("Game", TimeSpan.FromMinutes(10));
    
    Task<IReadOnlyList<GameDto>> IGameDataProvider.GetGamesAsync()
    {
        return this.cache.GetAsync(
            "All",
            inner.GetGamesAsync
        );
    }

    Task<TryResult<GameDto>> IGameDataProvider.TryGetGameAsync(Guid gameId)
    {
        return this.cache.GetAsync(
            $"Single::{gameId}",
            () => inner.TryGetGameAsync(gameId)
        );
    }

    Task<TryResult<GameDto>> IGameDataProvider.TryGetGameByNameAsync(Guid archiId, string name)
    {
        return this.cache.GetAsync(
            $"Single::{name}",
            () => inner.TryGetGameByNameAsync(archiId, name)
        );
    }

    Task<IReadOnlyList<GameDto>> IGameDataProvider.GetGamesByArchiAsync(Guid archiId)
    {
        return inner.GetGamesByArchiAsync(archiId);
    }

    async Task<Guid> IGameDataProvider.CreateGameAsync(
        DiscordId uploaderId, 
        Guid archiId,
        string name, 
        bool isOfficial, 
        string fileName
    ) {
        Guid gameId = await inner.CreateGameAsync(uploaderId, archiId, name, isOfficial, fileName).SafeAsync();
        await this.cache.RemoveAsync("All").SafeAsync();
        return gameId;
    }

    async Task IGameDataProvider.EditGameAsync(
        Guid gameId, 
        DiscordId editorId, 
        string name, 
        bool isOfficial, 
        string fileName, 
        string icon
    ) {
        await inner.EditGameAsync(gameId, editorId, name, isOfficial, fileName, icon).SafeAsync();
        await this.cache.RemoveAsync("All").SafeAsync();
        await this.cache.RemoveAsync($"Single::{gameId}").SafeAsync();
    }

    async Task IGameDataProvider.DeleteGameAsync(Guid gameId)
    {
        await inner.DeleteGameAsync(gameId).SafeAsync();
        await this.cache.RemoveAsync("All").SafeAsync();
        await this.cache.RemoveAsync($"Single::{gameId}").SafeAsync();
    }
}