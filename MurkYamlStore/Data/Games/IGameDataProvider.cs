using System.Diagnostics.CodeAnalysis;
using MurkYamlStore.Core;

namespace MurkYamlStore.Data.Games;

public interface IGameDataProvider
{
    Task<IReadOnlyList<GameDto>> GetGamesAsync();
    Task<TryResult<GameDto>> TryGetGameAsync(Guid gameId);
    Task<TryResult<GameDto>> TryGetGameByNameAsync(Guid archiId, string name);

    Task<IReadOnlyList<GameDto>> GetGamesByArchiAsync(Guid archiId);
    
    Task<Guid> CreateGameAsync(
        DiscordId uploaderId,
        Guid archiId,
        string name,
        bool isOfficial,
        string filename
    );

    Task EditGameAsync(
        Guid gameId,
        DiscordId editorId,
        string name,
        bool isOfficial,
        string fileName,
        string icon
    );
    
    Task DeleteGameAsync(Guid gameId);
}