using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MurkYamlStore.Core;
using MurkYamlStore.Domain.Games;
using MurkYamlStore.HttpException;
using MurkYamlStore.Util;

namespace MurkYamlStore.Controllers;

public class GameController(
    IGameManager gameManager
) : ControllerBase
{
    [AllowAnonymous]
    [HttpGet]
    [Route("/api/game/{gameSlug}/icon")]
    [Produces("image/png")]
    public async Task<IActionResult> GetGameIconAsync([FromRoute] string gameSlug)
    {
        if (!Guid.TryParse(gameSlug, out Guid gameId))
        {
            throw new BadRequestException("Invalid game slug.");
        }
        
        TryResult<IGame> game = await gameManager.TryGetGameByIdAsync(gameId).SafeAsync();
        if (!game.HasValue)
        {
            throw new NotFoundException("Game not found.");
        }

        if (string.IsNullOrWhiteSpace(game.Value.Icon))
        {
            return FileHelpers.ReturnDefaultImage();
        }
        
        return ReturnGameFile(gameId, game.Value.ArchiId, game.Value.Icon);
    }

    [AllowAnonymous]
    [HttpGet]
    [Route("/api/game/{gameSlug}/world")]
    [Produces("application/x-apworld")]
    public async Task<IActionResult> GetGameApWorldAsync([FromRoute] string gameSlug)
    {
        if (!Guid.TryParse(gameSlug, out Guid gameId))
        {
            throw new BadRequestException("Invalid game slug.");
        }
        
        TryResult<IGame> game = await gameManager.TryGetGameByIdAsync(gameId).SafeAsync();
        if (!game.HasValue)
        {
            throw new NotFoundException("Game not found.");
        }

        if (game.Value.IsOfficial)
        {
            throw new NotFoundException("Official Archi games are bundled with the launcher and cannot be downloaded from here.");
        }

        return ReturnGameFile(gameId, game.Value.ArchiId, game.Value.FileName);
    }

    private static FileStreamResult ReturnGameFile(
        Guid gameId,
        Guid archiId,
        string filename
    )
    {
        string realFilePath = Path.Combine(FileHelpers.GetGameDirectory(gameId, archiId), filename);
        DateTime lastModified;

        try
        {
            lastModified = System.IO.File.GetLastWriteTime(realFilePath);
        }
        catch (Exception ex)
        {
            throw new NotFoundException(innerException: ex);
        }
        
        Stream fileStream = System.IO.File.OpenRead(realFilePath);
        return new FileStreamResult(fileStream, FileHelpers.GuessMimeType(filename))
        {
            FileDownloadName = filename,
            LastModified = lastModified
        };
    }
}