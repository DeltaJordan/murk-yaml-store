using System.IO.Compression;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MurkYamlStore.Core;
using MurkYamlStore.Domain.Archis;
using MurkYamlStore.Domain.Games;
using MurkYamlStore.Domain.Yamls;
using MurkYamlStore.HttpException;
using MurkYamlStore.Util;

namespace MurkYamlStore.Controllers;

public class ArchiController(
    IArchiManager archiManager,
    IGameManager gameManager,
    IYamlManager yamlManager
) : ControllerBase
{
    [AllowAnonymous]
    [Route("/api/archi/{archiSlug}/seed")]
    public async Task<IActionResult> GetArchiSeedAsync([FromRoute] string archiSlug)
    {
        if (!Guid.TryParse(archiSlug, out Guid archiId))
        {
            throw new BadRequestException("Invalid archipelago ID.");
        }

        TryResult<IArchi> archi = await archiManager.TryGetArchiAsync(archiId).SafeAsync();
        if (!archi.HasValue)
        {
            throw new NotFoundException("Archipelago with requested ID not found.");
        }

        if (string.IsNullOrWhiteSpace(archi.Value.SeedFileName))
        {
            throw new NotFoundException("This archi does not have a seed uploaded yet.");
        }
        
        return ReturnArchiFile(archiId, archi.Value.SeedFileName);
    }
    
    [AllowAnonymous]
    [Route("/api/archi/{archiSlug}/yamls")]
    [HttpGet]
    [Produces("application/zip")]
    public async Task<IActionResult> GetArchiFilesAsync([FromRoute] string archiSlug)
    {
        if (!Guid.TryParse(archiSlug, out Guid archiId))
        {
            throw new BadRequestException("Invalid archipelago ID.");
        }

        TryResult<IArchi> archi = await archiManager.TryGetArchiAsync(archiId).SafeAsync();
        if (!archi.HasValue)
        {
            throw new NotFoundException("Archipelago with requested ID not found.");
        }

        IReadOnlyList<IYaml> archiYamls = await yamlManager.FetchYamlsByArchiAsync(archiId).SafeAsync();
        List<IGame> unofficialArchiGames = [];

        foreach (Guid gameId in archiYamls.Select(x => x.GameId).Distinct().ToList())
        {
            TryResult<IGame> game = await gameManager.TryGetGameByIdAsync(gameId).SafeAsync();
            if (!game.HasValue)
            {
                throw new NotFoundException("One or more game apworlds could not found.");
            }

            if (game.Value.IsOfficial) continue;

            unofficialArchiGames.Add(game.Value);
        }

        await using MemoryStream ms = new();
        using (ZipArchive seedZip = new(ms, ZipArchiveMode.Create, true))
        {
            foreach (IYaml archiYaml in archiYamls)
            {
                string archiPath = Path.Combine(FileHelpers.GetYamlDirectory(archiId), archiYaml.FileName);
                seedZip.CreateEntryFromFile(archiPath, archiYaml.FileName);
            }

            foreach (IGame archiGame in unofficialArchiGames)
            {
                string apWorldPath =
                    Path.Combine(FileHelpers.GetGameDirectory(archiGame.GameId, archiId), archiGame.FileName);
                seedZip.CreateEntryFromFile(apWorldPath, archiGame.FileName);
            }
        }

        return new FileContentResult(ms.ToArray(), "application/zip")
        {
            FileDownloadName = archiId + ".zip",
            LastModified = DateTime.UtcNow
        };
    }

    [AllowAnonymous]
    [Route("/api/archi/{archiSlug}/icon")]
    [HttpGet]
    [Produces("image/png")]
    public async Task<IActionResult> GetLogoAsync([FromRoute] string archiSlug)
    {
        if (!Guid.TryParse(archiSlug, out Guid archiId))
        {
            throw new BadRequestException("Invalid archipelago ID.");
        }

        TryResult<IArchi> archi = await archiManager.TryGetArchiAsync(archiId).SafeAsync();
        if (!archi.HasValue)
        {
            throw new NotFoundException("Archipelago with requested ID not found.");
        }

        if (string.IsNullOrWhiteSpace(archi.Value.Icon))
        {
            throw new NotFoundException("This archipelago does not have an icon.");
        }

        return ReturnArchiFile(archiId, archi.Value.Icon);
    }

    private static FileStreamResult ReturnArchiFile(
        Guid archiId,
        string filename
    )
    {
        string realFilePath = Path.Combine(FileHelpers.GetArchiDirectory(archiId), filename);
        DateTime lastModified;

        try
        {
            lastModified = System.IO.File.GetLastWriteTime(realFilePath);
        }
        catch (Exception ex)
        {
            throw new NotFoundException(innerException: ex);
        }

        Stream fileStream = System.IO.File.OpenRead(realFilePath);
        return new FileStreamResult(fileStream, FileHelpers.GuessMimeType(filename))
        {
            FileDownloadName = filename,
            LastModified = lastModified
        };
    }
}