using System.Diagnostics;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using MurkYamlStore.Models;

namespace MurkYamlStore.Controllers;

public sealed class AuthenticationController : ControllerBase
{
    [Route("/auth/login")]
    [HttpGet]
    public IActionResult SignIn()
    {
        return this.Challenge(new AuthenticationProperties { RedirectUri = "/" }, "Discord");
    }

    [Route("/auth/logout")]
    [HttpGet, HttpPost]
    public IActionResult SignOutCurrentUser([FromQuery] string returnUrl)
    {
        returnUrl = string.IsNullOrWhiteSpace(returnUrl) ? "/" : returnUrl;

        return this.SignOut(new AuthenticationProperties { RedirectUri = returnUrl },
            CookieAuthenticationDefaults.AuthenticationScheme);
    }
}