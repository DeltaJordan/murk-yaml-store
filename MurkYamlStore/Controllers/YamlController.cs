using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MurkYamlStore.Core;
using MurkYamlStore.Domain.Yamls;
using MurkYamlStore.HttpException;
using MurkYamlStore.Util;

namespace MurkYamlStore.Controllers;

public class YamlController(IYamlManager yamlManager) : ControllerBase
{

    [AllowAnonymous]
    [HttpGet]
    [Route("/api/yaml/{yamlSlug}/file")]
    [Produces("application/yaml")]
    public async Task<IActionResult> GetYamlAsync([FromRoute] string yamlSlug)
    {
        if (!Guid.TryParse(yamlSlug, out Guid yamlId))
        {
            throw new BadRequestException("Invalid YAML slug.");
        }
        
        TryResult<IYaml> yaml = await yamlManager.TryGetYamlAsync(yamlId).SafeAsync();
        if (!yaml.HasValue)
        {
            throw new NotFoundException("YAML not found.");
        }

        return ReturnYamlFile(yaml.Value.ArchiId, yaml.Value.FileName);
    }

    private static FileStreamResult ReturnYamlFile(
        Guid archiId,
        string filename
    )
    {
        string realFilePath = Path.Combine(FileHelpers.GetYamlDirectory(archiId), filename);
        DateTime lastModified;

        try
        {
            lastModified = System.IO.File.GetLastWriteTime(realFilePath);
        }
        catch (Exception ex)
        {
            throw new NotFoundException(innerException: ex);
        }
        
        Stream fileStream = System.IO.File.OpenRead(realFilePath);
        return new FileStreamResult(fileStream, FileHelpers.GuessMimeType(filename))
        {
            FileDownloadName = filename,
            LastModified = lastModified
        };
    }
}