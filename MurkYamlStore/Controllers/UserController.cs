using System.Reflection.Metadata.Ecma335;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MurkYamlStore.Components.Pages;
using MurkYamlStore.Core;
using MurkYamlStore.Domain.Users;
using MurkYamlStore.Enums;
using MurkYamlStore.Util;

namespace MurkYamlStore.Controllers;

public sealed class UserController(
    ILogger<UserController> logger,
    IUserManager userManager,
    IUserResolver userResolver,
    IUserContextProvider userContextProvider
) : ControllerBase
{
    private readonly ILogger<UserController> logger = logger;
    private readonly IUserManager userManager = userManager;
    private readonly IUserResolver userResolver = userResolver;
    private readonly IUserContextProvider userContextProvider = userContextProvider;

    [AllowAnonymous]
    [Route("/api/user/{userSlug}/avatar")]
    [HttpGet]
    [Produces("image/png")]
    public async Task<IActionResult> AvatarAsync(string userSlug)
    {
        TryResult<IUser> user = await this.userResolver.TryResolveUserAsync(userSlug).SafeAsync();
        DiscordRef? discord = user.Value?.Discord;
        return await this.GetAvatarImageAsync(discord.GetValueOrDefault()).SafeAsync();
    }

    [AllowAnonymous]
    [Route("/api/user/{userSlug}/banner")]
    [HttpGet]
    [Produces("image/png")]
    public async Task<IActionResult> BannerAsync(string userSlug)
    {
        TryResult<IUser> user = await this.userResolver.TryResolveUserAsync(userSlug).SafeAsync();
        DiscordRef? discord = user.Value?.Discord;
        return await this.GetBannerImageAsync(discord.GetValueOrDefault()).SafeAsync();
    }

    private Task<IActionResult> GetBannerImageAsync(DiscordRef discord)
    {
        return this.GetDiscordImageAsync(DiscordImageType.UserBanner, discord.UserId, discord.BannerHash);
    }

    private Task<IActionResult> GetAvatarImageAsync(DiscordRef discord)
    {
        return this.GetDiscordImageAsync(DiscordImageType.UserAvatar, discord.UserId, discord.AvatarHash);
    }

    private async Task<IActionResult> GetDiscordImageAsync(DiscordImageType imageType, DiscordId userId, string hash)
    {
        if (string.IsNullOrWhiteSpace(hash))
        {
            string defaultAvatar = "/assets/img/discord.svg";
            return this.File(defaultAvatar, "image/png");
        }

        string imageExtension = hash.StartsWith("a_") ? "gif" : "png";

        string avatarUrl = imageType switch
        {
            DiscordImageType.UserAvatar => $"{DiscordUtil.DISCORD_CDN}/avatars/{userId}/{hash}.{imageExtension}?size=256",
            DiscordImageType.UserBanner => $"{DiscordUtil.DISCORD_CDN}/banners/{userId}/{hash}.{imageExtension}?size=4096",
            _ => null
        };

        try
        {
            using HttpClient client = new();
            await using Stream s = await client.GetStreamAsync(avatarUrl).SafeAsync();
            using MemoryStream ms = new();
            await s.CopyToAsync(ms).SafeAsync();

            return new FileContentResult(ms.ToArray(), $"image/{imageExtension}");
        }
        catch
        {
            this.logger.LogError("Failed to get avatar from Discord. Responding with default avatar.");
            return FileHelpers.ReturnDefaultImage();
        }
    }
}