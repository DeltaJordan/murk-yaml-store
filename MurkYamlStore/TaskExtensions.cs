using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace MurkYamlStore;

#pragma warning disable VSTHRD003
#pragma warning disable VSTHRD200

[EditorBrowsable(EditorBrowsableState.Never)]
public static class TaskExtensions
{

	public static ConfiguredTaskAwaitable SafeAsync(this Task task)
	{
		var test = DateTime.Now;
		
		return task.ConfigureAwait(false);
	}

	public static ConfiguredTaskAwaitable<T> SafeAsync<T>(this Task<T> task)
	{
		return task.ConfigureAwait(false);
	}

	public static void SafeWait(this Task task)
	{
		task.ConfigureAwait(false).GetAwaiter().GetResult();
	}

	public static T SafeWait<T>(this Task<T> task)
	{
		return task.ConfigureAwait(false).GetAwaiter().GetResult();
	}

#pragma warning disable MURK003
	public static ConfiguredValueTaskAwaitable SafeAsync(this ValueTask task)
	{
		return task.ConfigureAwait(false);
	}

	public static ConfiguredValueTaskAwaitable<T> SafeAsync<T>(this ValueTask<T> task)
	{
		return task.ConfigureAwait(false);
	}
#pragma warning restore MURK003

	public static void SafeWait(this ValueTask task)
	{
		task.ConfigureAwait(false).GetAwaiter().GetResult();
	}

	public static T SafeWait<T>(this ValueTask<T> task)
	{
		return task.ConfigureAwait(false).GetAwaiter().GetResult();
	}

#pragma warning disable MURK002
	public static async Task<TOut> Then<TIn, TOut>(
		this Task<TIn> task,
		Func<TIn, TOut> syncFunc
	)
	{
		return syncFunc(await task.SafeAsync());
	}

	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static async Task<TOut> As<TIn, TOut>(this Task<TIn> task) where TIn : TOut
	{
		return await task.SafeAsync();
	}
#pragma warning restore MURK002

	public static async Task<TOut> ThenAsync<TIn, TOut>(
		this Task<TIn> task,
		Func<TIn, Task<TOut>> asyncFunc
	)
	{
		return await asyncFunc(await task.SafeAsync()).SafeAsync();
	}

}
