using MurkYamlStore.Core;

namespace MurkYamlStore.Domain.Archis;

public interface IArchi
{
    Guid ArchiId { get; }
    RichUserRef Author { get; }
    string Title { get; set; }
    string Description { get; set; }
    string Icon { get; set; }
    DateTime DateCreated { get; }
    DateTime SubmissionDeadline { get; set; }
    DateTime StartDate { get; set; }
    DateTime? DateCompleted { get; set; }
    string SeedFileName { get; set; }
    
#pragma warning disable MURK003
    bool? IsAsync { get; set; }
#pragma warning restore MURK003
    string ArchiIp { get; set; }
    ushort? ArchiPort { get; set; }
    
    Task SaveAsync();
    Task DeleteAsync();
}