using MurkYamlStore.Core;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Domain.Archis;

public interface IUnsecuredArchiManager
{
    Task<IReadOnlyList<IArchi>> GetAllArchisAsync();
    Task<IReadOnlyList<IArchi>> GetArchisPagedAsync(int pageSize, BookmarkToken bookmark);
    Task<TryResult<IArchi>> TryGetArchiAsync(Guid archiId);
    
    Task<IReadOnlyList<IArchi>> GetPastArchisPagedAsync(int pageSize, BookmarkToken bookmark);
    Task<IReadOnlyList<IArchi>> GetActiveArchisPagedAsync(int pageSize, BookmarkToken bookmark);
    Task<IReadOnlyList<IArchi>> GetUpcomingArchisPagedAsync(int pageSize, BookmarkToken bookmark);
    
    Task<long> GetArchiPlayerCountAsync(Guid archiId);
    Task<long> GetArchiYamlCountAsync(Guid archiId);
    
    Task<IArchi> CreateArchiAsync(
        DiscordId authorId,
        string title,
        string description,
        DateTime submissionDeadline,
        DateTime startDate,
        bool? isAsync,
        string archiIp = null,
        ushort? archiPort = null
    );
}