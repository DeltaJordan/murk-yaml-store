using System.Diagnostics.CodeAnalysis;
using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.HttpException;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Domain.Archis.Impl;

[SuppressMessage("Performance", "CA1859:Use concrete types when possible for improved performance")]
public class SecuredArchiManager(
    IUnsecuredArchiManager inner, 
    IUserContextProvider user
) : IArchiManager
{
    Task<IReadOnlyList<IArchi>> IUnsecuredArchiManager.GetAllArchisAsync()
    {
        IUserContext userContext = user.Context;
        return inner.GetAllArchisAsync().Then(
            x => x.SelectOrTransform(p => Secure(p, userContext))
        );
    }

    Task<IReadOnlyList<IArchi>> IUnsecuredArchiManager.GetArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        IUserContext userContext = user.Context;
        return inner.GetArchisPagedAsync(pageSize, bookmark).Then(
            x => x.SelectOrTransform(p => Secure(p, userContext))
        );
    }

    Task<TryResult<IArchi>> IUnsecuredArchiManager.TryGetArchiAsync(Guid archiId)
    {
        return inner.TryGetArchiAsync(archiId).Then(x => x.Apply(this.Secure));
    }

    Task<IReadOnlyList<IArchi>> IUnsecuredArchiManager.GetPastArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        IUserContext userContext = user.Context;
        return inner.GetPastArchisPagedAsync(pageSize, bookmark).Then(
            x => x.SelectOrTransform(p => Secure(p, userContext))
        );
    }

    Task<IReadOnlyList<IArchi>> IUnsecuredArchiManager.GetActiveArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        IUserContext userContext = user.Context;
        return inner.GetActiveArchisPagedAsync(pageSize, bookmark).Then(
            x => x.SelectOrTransform(p => Secure(p, userContext))
        );
    }

    Task<IReadOnlyList<IArchi>> IUnsecuredArchiManager.GetUpcomingArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        IUserContext userContext = user.Context;
        return inner.GetUpcomingArchisPagedAsync(pageSize, bookmark).Then(
            x => x.SelectOrTransform(p => Secure(p, userContext))
        );
    }

    Task<long> IUnsecuredArchiManager.GetArchiPlayerCountAsync(Guid archiId)
    {
        return inner.GetArchiPlayerCountAsync(archiId);
    }

    Task<long> IUnsecuredArchiManager.GetArchiYamlCountAsync(Guid archiId)
    {
        return inner.GetArchiYamlCountAsync(archiId);
    }

    Task<IArchi> IUnsecuredArchiManager.CreateArchiAsync(
        DiscordId authorId,
        string title,
        string description,
        DateTime submissionDeadline,
        DateTime startDate,
        bool? isAsync,
        string archiIp,
        ushort? archiPort
    )
    {
        IUserContext userContext = user.Context;
        if (authorId != userContext?.Discord.UserId || userContext?.Role != Role.Staff)
            throw new AuthorizationException();
        return inner
            .CreateArchiAsync(authorId, title, description, submissionDeadline, startDate, isAsync, archiIp, archiPort)
            .Then(x => Secure(x, userContext));
    }

    private IArchi Secure(IArchi archi)
    {
        return new SecuredArchi(archi, user.Context);
    }

    private static IArchi Secure(IArchi archi, IUserContext user)
    {
        return new SecuredArchi(archi, user);
    }
}