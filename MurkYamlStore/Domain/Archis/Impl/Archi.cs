using MurkYamlStore.Core;
using MurkYamlStore.Data.Archis;

namespace MurkYamlStore.Domain.Archis.Impl;

public sealed class Archi(IArchiDataProvider dataProvider, ArchiDto inner) : IArchi
{
    private readonly Field<string> title = new(inner.Title);
    private readonly Field<string> description = new(inner.Description);
    private readonly Field<string> icon = new(inner.Icon);
    private readonly Field<DateTime> submissionDeadline = new(inner.SubmissionDeadline);
    private readonly Field<DateTime> startDate = new(inner.StartDate);
    private readonly Field<DateTime?> dateCompleted = new(inner.DateCompleted);
    private readonly Field<string> seedFileName = new(inner.SeedFileName);
    private readonly Field<bool?> isAsync = new(inner.IsAsync);
    private readonly Field<string> archiIp = new(inner.ArchiIp);
    private readonly Field<ushort?> archiPort = new(inner.ArchiPort);
    
    Guid IArchi.ArchiId { get; } = inner.ArchiId;
    RichUserRef IArchi.Author { get; } = inner.Author;
    DateTime IArchi.DateCreated { get; } = inner.DateCreated;

    string IArchi.Title
    {
        get => this.title.Value;
        set => this.title.SetIfDifferent(value);
    }

    string IArchi.Description
    {
        get => this.description.Value;
        set => this.description.SetIfDifferent(value);
    }

    string IArchi.Icon
    {
        get => this.icon.Value;
        set => this.icon.SetIfDifferent(value);
    }

    DateTime IArchi.SubmissionDeadline
    {
        get => this.submissionDeadline.Value;
        set => this.submissionDeadline.SetIfDifferent(value);
    }

    DateTime IArchi.StartDate
    {
        get => this.startDate.Value;
        set => this.startDate.SetIfDifferent(value);
    }

    DateTime? IArchi.DateCompleted
    {
        get => this.dateCompleted.Value;
        set => this.dateCompleted.SetIfDifferent(value);
    }

    string IArchi.SeedFileName
    {
        get => this.seedFileName.Value;
        set => this.seedFileName.SetIfDifferent(value);
    }

    bool? IArchi.IsAsync
    {
#pragma warning disable MURK003
        get => this.isAsync.Value;
        set => this.isAsync.SetIfDifferent(value);
#pragma warning restore MURK003
    }

    string IArchi.ArchiIp
    {
        get => this.archiIp.Value;
        set => this.archiIp.SetIfDifferent(value);
    }

    ushort? IArchi.ArchiPort
    {
        get => this.archiPort.Value;
        set => this.archiPort.SetIfDifferent(value);
    }

    Task IArchi.SaveAsync()
    {
        IArchi @this = this;
        
        if (
            this.title.HasChanged || 
            this.description.HasChanged || 
            this.icon.HasChanged || 
            this.submissionDeadline.HasChanged || 
            this.dateCompleted.HasChanged ||
            this.startDate.HasChanged ||
            this.seedFileName.HasChanged ||
            this.isAsync.HasChanged ||
            this.archiIp.HasChanged ||
            this.archiPort.HasChanged
        ) {
            return dataProvider.EditArchiAsync(
                @this.ArchiId, 
                this.title.Value, 
                this.description.Value, 
                this.icon.Value, 
                this.submissionDeadline.Value, 
                this.startDate.Value,
                this.dateCompleted.Value,
                this.seedFileName.Value,
                this.isAsync.Value,
                this.archiIp.Value,
                this.archiPort.Value
            );
        }
        
        return Task.CompletedTask;
    }

    Task IArchi.DeleteAsync()
    {
        IArchi @this = this;
        return dataProvider.DeleteArchiAsync(@this.ArchiId);
    }
}