using MurkYamlStore.Core;
using MurkYamlStore.Data.Archis;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Domain.Archis.Impl;

internal sealed class UnsecuredArchiManager(IArchiDataProvider dataProvider) : IUnsecuredArchiManager
{
    async Task<IReadOnlyList<IArchi>> IUnsecuredArchiManager.GetAllArchisAsync()
    {
        IEnumerable<ArchiDto> dtos = await dataProvider.GetArchisAsync().SafeAsync();
        return dtos.Select(this.Build).ToList();
    }

    async Task<IReadOnlyList<IArchi>> IUnsecuredArchiManager.GetArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        IReadOnlyList<ArchiDto> dtos = await dataProvider.GetArchisPagedAsync(pageSize, bookmark).SafeAsync();
        bookmark.Clear();
        if (dtos.Count >= pageSize)
        {
            bookmark.PushValue(dtos[^1].DateCreated);
        }
        
        return dtos.Select(this.Build).ToList();
    }

    async Task<TryResult<IArchi>> IUnsecuredArchiManager.TryGetArchiAsync(Guid archiId)
    {
        TryResult<ArchiDto> dto = await dataProvider.TryGetArchiAsync(archiId).SafeAsync();
        return dto.Apply(this.Build);
    }

    async Task<IReadOnlyList<IArchi>> IUnsecuredArchiManager.GetPastArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        IReadOnlyList<ArchiDto> dtos = await dataProvider.GetPastArchisPagedAsync(pageSize, bookmark).SafeAsync();
        bookmark.Clear();
        if (dtos.Count >= pageSize)
        {
            bookmark.PushValue(dtos[^1].DateCreated);
        }
        
        return dtos.Select(this.Build).ToList();
    }

    async Task<IReadOnlyList<IArchi>> IUnsecuredArchiManager.GetActiveArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        IReadOnlyList<ArchiDto> dtos = await dataProvider.GetActiveArchisPagedAsync(pageSize, bookmark).SafeAsync();
        bookmark.Clear();
        if (dtos.Count >= pageSize)
        {
            bookmark.PushValue(dtos[^1].DateCreated);
        }
        
        return dtos.Select(this.Build).ToList();
    }

    async Task<IReadOnlyList<IArchi>> IUnsecuredArchiManager.GetUpcomingArchisPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        IReadOnlyList<ArchiDto> dtos = await dataProvider.GetUpcomingArchisPagedAsync(pageSize, bookmark).SafeAsync();
        bookmark.Clear();
        if (dtos.Count >= pageSize)
        {
            bookmark.PushValue(dtos[^1].DateCreated);
        }
        
        return dtos.Select(this.Build).ToList();
    }

    Task<long> IUnsecuredArchiManager.GetArchiPlayerCountAsync(Guid archiId)
    {
        return dataProvider.GetArchiPlayerCountAsync(archiId);
    }

    Task<long> IUnsecuredArchiManager.GetArchiYamlCountAsync(Guid archiId)
    {
        return dataProvider.GetArchiYamlCountAsync(archiId);
    }

    async Task<IArchi> IUnsecuredArchiManager.CreateArchiAsync(
        DiscordId authorId,
        string title,
        string description,
        DateTime submissionDeadline,
        DateTime startDate,
        bool? isAsync,
        string archiIp,
        ushort? archiPort
    )
    {
        Guid archiId = await dataProvider.CreateArchiAsync(
            authorId, 
            title, 
            description, 
            submissionDeadline,
            startDate,
            isAsync,
            archiIp,
            archiPort
        ).SafeAsync();
        TryResult<ArchiDto> dto = await dataProvider.TryGetArchiAsync(archiId).SafeAsync();
        return this.Build(dto.Value);
    }

    private IArchi Build(ArchiDto archiDto)
    {
        return new Archi(dataProvider, archiDto);
    }
}