using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.HttpException;

namespace MurkYamlStore.Domain.Archis.Impl;

internal sealed class SecuredArchi(IArchi inner, IUserContext userContext) : IArchi
{
    Guid IArchi.ArchiId { get; } = inner.ArchiId;
    RichUserRef IArchi.Author { get; } = inner.Author;
    DateTime IArchi.DateCreated { get; } = inner.DateCreated;

    string IArchi.Title
    {
        get => inner.Title;
        set => inner.Title = value;
    }

    string IArchi.Description
    {
        get => inner.Description;
        set => inner.Description = value;
    }

    string IArchi.Icon
    {
        get => inner.Icon;
        set => inner.Icon = value;
    }

    DateTime IArchi.SubmissionDeadline
    {
        get => inner.SubmissionDeadline;
        set => inner.SubmissionDeadline = value;
    }

    DateTime IArchi.StartDate
    {
        get => inner.StartDate;
        set => inner.StartDate = value;
    }

    DateTime? IArchi.DateCompleted
    {
        get => inner.DateCompleted;
        set => inner.DateCompleted = value;
    }

    string IArchi.SeedFileName
    {
        get => inner.SeedFileName;
        set => inner.SeedFileName = value;
    }

    bool? IArchi.IsAsync
    {
#pragma warning disable MURK003
        get => inner.IsAsync;
        set => inner.IsAsync = value;
#pragma warning restore MURK003
    }

    string IArchi.ArchiIp
    {
        get => inner.ArchiIp;
        set => inner.ArchiIp = value;
    }

    ushort? IArchi.ArchiPort
    {
        get => inner.ArchiPort;
        set => inner.ArchiPort = value;
    }

    Task IArchi.SaveAsync()
    {
        if (!userContext.IsAtLeast(Role.Staff)) throw new AuthorizationException();

        return inner.SaveAsync();
    }

    Task IArchi.DeleteAsync()
    {
        if (!userContext.IsAtLeast(Role.Staff)) throw new AuthorizationException();
        
        return inner.DeleteAsync();
    }
}