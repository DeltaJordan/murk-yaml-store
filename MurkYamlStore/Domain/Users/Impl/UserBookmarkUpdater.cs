using MurkYamlStore.Pagination;

namespace MurkYamlStore.Domain.Users.Impl;

internal sealed class UserBookmarkUpdater : IUserBookmarkUpdater
{
    void IUserBookmarkUpdater.UpdateForAllUsers(
            BookmarkToken bookmark,
            IUser user
        )
    {
        bookmark.Clear();
        if (user != null)
        {
            bookmark.PushValue(user.Discord.Username);
        }
    }
}