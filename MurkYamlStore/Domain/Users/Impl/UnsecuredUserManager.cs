using MurkYamlStore.Core;
using MurkYamlStore.Data.Users;
using MurkYamlStore.HttpException;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Domain.Users.Impl;

internal sealed class UnsecuredUserManager(
    IUserDataProvider dataProvider,
    IUserBookmarkUpdater bookmarkUpdater
) : IUnsecuredUserManager
{
    private readonly IUserDataProvider dataProvider = dataProvider;
    private readonly IUserBookmarkUpdater bookmarkUpdater = bookmarkUpdater;

    Task<IReadOnlyList<string>> IUnsecuredUserManager.GetAllUsernamesAsync()
    {
        return this.dataProvider.GetAllUsernamesAsync();
    }

    async Task<IReadOnlyList<IUser>> IUnsecuredUserManager.GetModsAndStaffAsync()
    {
        IReadOnlyList<UserDto> userDtos = await this.dataProvider.GetModsAndStaffAsync().SafeAsync();
        return userDtos.Select(this.Build).CoerceToIReadOnlyList();
    }

    async Task<IUser> IUnsecuredUserManager.GetUserByIdAsync(DiscordId userId)
    {
        IUnsecuredUserManager @this = this;
        TryResult<IUser> user = await @this.TryGetUserByIdAsync(userId).SafeAsync();
        if (!user.HasValue) throw new NotFoundException("User not found.");
        return user.Value;
    }

    async Task<IUser> IUnsecuredUserManager.GetUserByUsernameAsync(string username)
    {
        IUnsecuredUserManager @this = this;
        TryResult<IUser> user = await @this.TryGetUserByUsernameAsync(username).SafeAsync();
        if (!user.HasValue) throw new NotFoundException("User not found.");
        return user.Value;
    }

    async Task<IReadOnlyDictionary<DiscordId, IUser>> IUnsecuredUserManager.GetUsersByIdsAsync(IEnumerable<DiscordId> userIds)
    {
        IReadOnlyDictionary<DiscordId, UserDto> dtos = await this.dataProvider.GetUsersByIdAsync(userIds).SafeAsync();
        return new Dictionary<DiscordId, IUser>(
            dtos.Select(kvp => new KeyValuePair<DiscordId, IUser>(kvp.Key, this.Build(kvp.Value)))
        );
    }

    async Task<IReadOnlyList<IUser>> IUnsecuredUserManager.GetUsersPageAsync(int pageSize, BookmarkToken bookmark)
    {
        IReadOnlyList<UserDto> dtos = await this.dataProvider.GetUsersPageAsync(pageSize, bookmark).SafeAsync();
        IReadOnlyList<IUser> users = dtos.Select(this.Build).CoerceToIReadOnlyList();
        this.bookmarkUpdater.UpdateForAllUsers(bookmark, users.Count >= pageSize ? users[users.Count - 1] : null);
        return users;
    }

    Task<IReadOnlyDictionary<string, DiscordRef>> IUnsecuredUserManager.ResolveUsernamesAsync(IEnumerable<string> usernames)
    {
        return this.dataProvider.ResolveUsernamesAsync(usernames);
    }

    async Task<IReadOnlyList<IUser>> IUnsecuredUserManager.SearchUsersPagedAsync(string searchTerm, int pageSize, BookmarkToken bookmark)
    {
        IReadOnlyList<UserDto> dtos = await this.dataProvider.SearchUsersPagedAsync(searchTerm, pageSize, bookmark).SafeAsync();
        IReadOnlyList<IUser> users = dtos.Select(this.Build).CoerceToIReadOnlyList();
        this.bookmarkUpdater.UpdateForAllUsers(bookmark, users.Count >= pageSize ? users[users.Count - 1] : null);
        return users;
    }

    async Task<TryResult<IUser>> IUnsecuredUserManager.TryGetUserByIdAsync(DiscordId userId)
    {
        TryResult<UserDto> user = await this.dataProvider.TryGetUserByIdAsync(userId).SafeAsync();
        return user.Apply(this.Build);
    }

    async Task<TryResult<IUser>> IUnsecuredUserManager.TryGetUserByUsernameAsync(string username)
    {
        TryResult<UserDto> user = await this.dataProvider.TryGetUserByUsernameAsync(username).SafeAsync();
        return user.Apply(this.Build);
    }

    private IUser Build(UserDto dto)
    {
        return new UnsecuredUser(this.dataProvider, dto);
    }
}