using System.Drawing;
using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.HttpException;

namespace MurkYamlStore.Domain.Users.Impl;

public sealed class SecuredUser(IUser inner, IUserContext userContext) : IUser
{
    DiscordRef IUser.Discord { get; } = inner.Discord;

    DateTime IUser.JoinedAt { get; } = inner.JoinedAt;

    Role IUser.Role
    {
        get => inner.Role;
        set
        {
            if (!UserSecurity.CanChangeRole(this, value, userContext)) throw new AuthorizationException();
            inner.Role = value;
        }
    }

    string IUser.Bio
    {
        get => inner.Bio;
        set => inner.Bio = value;
    }

    Color? IUser.BannerOverride
    {
        get => inner.BannerOverride;
        set => inner.BannerOverride = value;
    }

    Gender? IUser.Gender
    {
        get => inner.Gender;
        set => inner.Gender = value;
    }

    Task IUser.SaveAsync()
    {
        if (!UserSecurity.CanEdit(this, userContext)) throw new AuthorizationException();
        return inner.SaveAsync();
    }
}