using MurkYamlStore.Core;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Domain.Users.Impl;

internal sealed class SecuredUserManager(
    IUnsecuredUserManager inner,
    IUserContextProvider userContext,
    IUserBookmarkUpdater bookmarkUpdater
) : IUserManager
{
    private readonly IUnsecuredUserManager inner = inner;
    private readonly IUserContextProvider userContext = userContext;
    private readonly IUserBookmarkUpdater bookmarkUpdater = bookmarkUpdater;

    Task<IReadOnlyList<string>> IUnsecuredUserManager.GetAllUsernamesAsync()
    {
        return this.inner.GetAllUsernamesAsync();
    }

    Task<IReadOnlyList<IUser>> IUnsecuredUserManager.GetModsAndStaffAsync()
    {
        return this.inner.GetModsAndStaffAsync().Then(this.SecureMany);
    }

    Task<IUser> IUnsecuredUserManager.GetUserByIdAsync(DiscordId userId)
    {
        return this.inner.GetUserByIdAsync(userId).Then(this.Secure);
    }

    Task<IUser> IUnsecuredUserManager.GetUserByUsernameAsync(string username)
    {
        return this.inner.GetUserByUsernameAsync(username).Then(this.Secure);
    }

    async Task<IReadOnlyDictionary<DiscordId, IUser>> IUnsecuredUserManager.GetUsersByIdsAsync(IEnumerable<DiscordId> userIds)
    {
        IUserContext userContext = this.userContext.Context;
        IReadOnlyDictionary<DiscordId, IUser> users = await this.inner.GetUsersByIdsAsync(userIds).SafeAsync();
        return new Dictionary<DiscordId, IUser>(
            users.Select(kvp => new KeyValuePair<DiscordId, IUser>(kvp.Key, new SecuredUser(kvp.Value, userContext)))
        );
    }

    Task<IReadOnlyList<IUser>> IUnsecuredUserManager.GetUsersPageAsync(int pageSize, BookmarkToken bookmark)
    {
        IUserContext context = this.userContext.Context;
        return SecuredPageHelper.FetchPageAsync(
            pageFetcher: this.inner.GetUsersPageAsync,
            filter: user => UserSecurity.CanSeeInBulkFetch(user, context),
            bookmarkUpdater: this.bookmarkUpdater.UpdateForAllUsers,
            pageSize: pageSize,
            bookmark: bookmark
        ).Then(this.SecureMany);
    }

    Task<IReadOnlyDictionary<string, DiscordRef>> IUnsecuredUserManager.ResolveUsernamesAsync(IEnumerable<string> usernames)
    {
        return this.inner.ResolveUsernamesAsync(usernames);
    }

    Task<IReadOnlyList<IUser>> IUnsecuredUserManager.SearchUsersPagedAsync(string searchTerm, int pageSize, BookmarkToken bookmark)
    {
        // Banned users are not filtered out from this API call
        return this.inner.SearchUsersPagedAsync(searchTerm, pageSize, bookmark).Then(this.SecureMany);
    }

    async Task<TryResult<IUser>> IUnsecuredUserManager.TryGetUserByIdAsync(DiscordId userId)
    {
        TryResult<IUser> user = await this.inner.TryGetUserByIdAsync(userId).SafeAsync();
        return user.Apply(this.Secure);
    }

    async Task<TryResult<IUser>> IUnsecuredUserManager.TryGetUserByUsernameAsync(string username)
    {
        TryResult<IUser> user = await this.inner.TryGetUserByUsernameAsync(username).SafeAsync();
        return user.Apply(this.Secure);
    }

    private static SecuredUser Secure(IUser user, IUserContext userContext)
    {
        return new SecuredUser(user, userContext);
    }

    private IUser Secure(IUser user)
    {
        return new SecuredUser(user, this.userContext.Context);
    }

    private IReadOnlyList<IUser> SecureMany(IReadOnlyList<IUser> user)
    {
        IUserContext userContext = this.userContext.Context;
        return user.SelectOrTransform(user => Secure(user, userContext));
    }
}