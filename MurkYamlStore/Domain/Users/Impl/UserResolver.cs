using MurkYamlStore.Core;
using MurkYamlStore.HttpException;

namespace MurkYamlStore.Domain.Users.Impl;

internal sealed class UserResolver(
    IUserContextProvider userContextProvider,
    IUserManager userManager
) : IUserResolver
{

    private readonly IUserContextProvider userContextProvider = userContextProvider;
    private readonly IUserManager userManager = userManager;

    async Task<IUser> IUserResolver.ResolveUserAsync(string userSlug)
    {
        IUserResolver @this = this;
        TryResult<IUser> user = await @this.TryResolveUserAsync(userSlug).SafeAsync();
        if (user.HasValue) return user.Value;
        throw new NotFoundException();
    }

    async Task<DiscordId> IUserResolver.ResolveUserIdAsync(string userSlug)
    {
        IUserResolver @this = this;
        TryResult<DiscordId> userId = await @this.TryResolveUserIdAsync(userSlug).SafeAsync();
        if (userId.HasValue) return userId.Value;
        throw new NotFoundException();
    }

    Task<TryResult<IUser>> IUserResolver.TryResolveUserAsync(string userSlug)
    {
        if (string.IsNullOrEmpty(userSlug)) return Task.FromResult(new TryResult<IUser>());

        if (userSlug.Equals("self") || userSlug.Equals("current"))
        {
            DiscordId? _userId = this.userContextProvider.Context?.Discord.UserId;
            if (!_userId.HasValue) return Task.FromResult(new TryResult<IUser>());
            return this.userManager.TryGetUserByIdAsync(_userId.Value);
        }

        if (DiscordId.TryParse(userSlug, out DiscordId userId))
        {
            return this.userManager.TryGetUserByIdAsync(userId);
        }

        return this.userManager.TryGetUserByUsernameAsync(userSlug);
    }

    async Task<TryResult<DiscordId>> IUserResolver.TryResolveUserIdAsync(string userSlug)
    {
        if (string.IsNullOrEmpty(userSlug)) return new TryResult<DiscordId>();

        if (userSlug.Equals("self") || userSlug.Equals("current"))
        {
            DiscordId? _userId = this.userContextProvider.Context?.Discord.UserId;
            return _userId.HasValue ? new TryResult<DiscordId>(_userId.Value) : new TryResult<DiscordId>();
        }

        if (DiscordId.TryParse(userSlug, out DiscordId userId))
        {
            return new TryResult<DiscordId>(userId);
        }

        TryResult<IUser> user = await this.userManager.TryGetUserByUsernameAsync(userSlug).SafeAsync();
        return user.Apply(u => u.Discord.UserId);
    }

}