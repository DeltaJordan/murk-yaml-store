using System.Drawing;
using MurkYamlStore.Core;
using MurkYamlStore.Data.Users;
using MurkYamlStore.Enums;

namespace MurkYamlStore.Domain.Users.Impl;

public class UnsecuredUser(
    IUserDataProvider dataProvider,
    UserDto dto
) : IUser
{
    private readonly FieldExt<Role> role = new(dto.Role);

    DiscordRef IUser.Discord { get; } = dto.Discord;

    Role IUser.Role
    {
        get => this.role.Value;
        set => this.role.SetIfDifferent(value);
    }

    DateTime IUser.JoinedAt { get; } = dto.JoinedDate;

    string IUser.Bio { get; set; } = dto.Bio;

    Color? IUser.BannerOverride { get; set; } = dto.BannerOverride;

    Gender? IUser.Gender { get; set; } = dto.Gender;

    async Task IUser.SaveAsync()
    {
        IUser @this = this;
        
        await dataProvider.UpdateUserAsync(
            @this.Discord,
            @this.Role,
            @this.Bio,
            @this.Gender,
            @this.BannerOverride
        ).SafeAsync();
    }
}