using MurkYamlStore.Core;

namespace MurkYamlStore.Domain.Users;

public interface IUserResolver
{
    Task<DiscordId> ResolveUserIdAsync(string userSlug);
    Task<IUser> ResolveUserAsync(string userSlug);

    Task<TryResult<DiscordId>> TryResolveUserIdAsync(string userSlug);
    Task<TryResult<IUser>> TryResolveUserAsync(string userSlug);
}
