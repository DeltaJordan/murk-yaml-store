using MurkYamlStore.Pagination;

namespace MurkYamlStore.Domain.Users;

public interface IUserBookmarkUpdater
{
    void UpdateForAllUsers(
        BookmarkToken bookmark,
        IUser user
    );
}