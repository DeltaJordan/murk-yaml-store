using MurkYamlStore.Core;
using MurkYamlStore.Enums;

namespace MurkYamlStore.Domain.Users;

internal static class UserSecurity
{

    public static bool CanEdit(
        IUser user,
        IUserContext userContext
    )
    {
        return userContext.IsAtLeast(Role.Moderator) || (
            userContext.IsAtLeast(Role.User) &&
            user.Discord == userContext?.Discord
        );
    }

    public static bool CanSeeEmail(
        IUser user,
        IUserContext userContext
    )
    {
        return userContext.IsAtLeast(Role.Staff) || user.Discord.UserId == userContext?.Discord.UserId;
    }

    public static bool CanChangeRole(
        IUser user,
        Role intendedRole,
        IUserContext userContext
    )
    {
        if (userContext.IsAtLeast(Role.Staff)) return true;
        if (!userContext.IsAtLeast(Role.Moderator)) return false;
        if (user.Role.AtLeast(Role.Moderator)) return false;
        if (intendedRole.AtLeast(Role.Moderator)) return false;
        return true;
    }

    public static bool CanSeeInBulkFetch(
        IUser user,
        IUserContext userContext
    )
    {
        return user.Role.AtLeast(Role.User) || userContext.IsAtLeast(Role.Moderator);
    }

}
