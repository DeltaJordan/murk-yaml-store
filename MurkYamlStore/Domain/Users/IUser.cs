using System.Drawing;
using MurkYamlStore.Core;
using MurkYamlStore.Enums;

namespace MurkYamlStore.Domain.Users;

public interface IUser
{
    DiscordRef Discord { get; }
    Role Role { get; set; }
    DateTime JoinedAt { get; }
    string Bio { get; set; }
    Color? BannerOverride { get; set; }
    Gender? Gender { get; set; }

    Task SaveAsync();
}