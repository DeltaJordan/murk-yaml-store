using MurkYamlStore.Core;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Domain.Users;

public interface IUnsecuredUserManager
{
    Task<IUser> GetUserByUsernameAsync(string username);
    Task<TryResult<IUser>> TryGetUserByUsernameAsync(string username);
    Task<IUser> GetUserByIdAsync(DiscordId userId);
    Task<TryResult<IUser>> TryGetUserByIdAsync(DiscordId userId);

    Task<IReadOnlyDictionary<DiscordId, IUser>> GetUsersByIdsAsync(IEnumerable<DiscordId> userIds);
    Task<IReadOnlyDictionary<string, DiscordRef>> ResolveUsernamesAsync(IEnumerable<string> usernames);

    Task<IReadOnlyList<IUser>> GetUsersPageAsync(int pageSize, BookmarkToken bookmark);

    Task<IReadOnlyList<IUser>> SearchUsersPagedAsync(string searchTerm, int pageSize, BookmarkToken bookmark);

    Task<IReadOnlyList<IUser>> GetModsAndStaffAsync();
    Task<IReadOnlyList<string>> GetAllUsernamesAsync();
}
