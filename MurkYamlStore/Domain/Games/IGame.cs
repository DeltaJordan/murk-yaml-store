using MurkYamlStore.Core;
using MurkYamlStore.Domain.Users;

namespace MurkYamlStore.Domain.Games;

public interface IGame
{
    Guid GameId { get; }
    Guid ArchiId { get; }
    RichUserRef Uploader { get; }
    string GameName { get; set; }
    bool IsOfficial { get; set; }
    string FileName { get; set; }
    string Icon { get; set; }
    
    Task SaveAsync(IUser newUploader);
    Task DeleteAsync();
}