using MurkYamlStore.Core;
using MurkYamlStore.Data.Games;

namespace MurkYamlStore.Domain.Games.Impl;

public class UnsecuredGameManager(IGameDataProvider dataProvider) : IUnsecuredGameManager
{
    async Task<IReadOnlyList<IGame>> IUnsecuredGameManager.GetAllGamesAsync()
    {
        IReadOnlyList<GameDto> dtos = await dataProvider.GetGamesAsync().SafeAsync();
        return dtos.Select(this.Build).ToList();
    }

    Task<TryResult<IGame>> IUnsecuredGameManager.TryGetGameByIdAsync(Guid gameId)
    {
        return dataProvider.TryGetGameAsync(gameId).Then(x => x.Apply(this.Build));
    }

    Task<TryResult<IGame>> IUnsecuredGameManager.TryGetGameByNameAsync(Guid archiId, string name)
    {
        return dataProvider.TryGetGameByNameAsync(archiId, name).Then(x => x.Apply(this.Build));
    }

    async Task<IReadOnlyList<IGame>> IUnsecuredGameManager.GetGamesByArchiAsync(Guid archiId)
    {
        IReadOnlyList<GameDto> dtos = await dataProvider.GetGamesByArchiAsync(archiId).SafeAsync();
        return dtos.Select(this.Build).ToList();
    }

    Task<Guid> IUnsecuredGameManager.CreateOfficialGameAsync(
        DiscordId uploaderId, 
        Guid archiId, 
        string name
    ) {
        return dataProvider.CreateGameAsync(uploaderId, archiId, name, true, null);
    }

    Task<Guid> IUnsecuredGameManager.CreateUnofficialGameAsync(
        DiscordId uploaderId, 
        Guid archiId, 
        string name, 
        string filename
    ) {
        return dataProvider.CreateGameAsync(uploaderId, archiId, name, false, filename);
    }

    private IGame Build(GameDto dto)
    {
        return new Game(dataProvider, dto);
    }
}