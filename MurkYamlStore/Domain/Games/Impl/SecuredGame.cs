using MurkYamlStore.Core;
using MurkYamlStore.Domain.Users;
using MurkYamlStore.Enums;
using MurkYamlStore.HttpException;

namespace MurkYamlStore.Domain.Games.Impl;

public sealed class SecuredGame(IGame inner, IUserContext userContext) : IGame
{
    Guid IGame.GameId { get; } = inner.GameId;
    Guid IGame.ArchiId { get; } = inner.ArchiId;
    RichUserRef IGame.Uploader { get; } = inner.Uploader;

    string IGame.GameName
    {
        get => inner.GameName;
        set
        {
            if (!userContext.IsAtLeast(Role.Staff)) throw new AuthorizationException();
            inner.GameName = value;
        }
    }

    bool IGame.IsOfficial
    {
        get => inner.IsOfficial;
        set
        {
            if (!userContext.IsAtLeast(Role.Staff)) throw new AuthorizationException();
            inner.IsOfficial = value;
        }
    }

    string IGame.FileName
    {
        get => inner.FileName;
        set => inner.FileName = value;
    }

    string IGame.Icon
    {
        get => inner.Icon;
        set => inner.Icon = value;
    }

    Task IGame.SaveAsync(IUser newUploader)
    {
        if (userContext.Role == Role.Banned) throw new AuthorizationException();
        return inner.SaveAsync(newUploader);
    }

    Task IGame.DeleteAsync()
    {
        if (!userContext.IsAtLeast(Role.Staff)) throw new AuthorizationException();
        return inner.DeleteAsync();
    }
}