using MurkYamlStore.Core;
using MurkYamlStore.Data.Games;
using MurkYamlStore.Enums;
using MurkYamlStore.HttpException;

namespace MurkYamlStore.Domain.Games.Impl;

public class SecuredGameManager(
    IUnsecuredGameManager inner, 
    IUserContextProvider user
) : IGameManager
{
    async Task<IReadOnlyList<IGame>> IUnsecuredGameManager.GetAllGamesAsync()
    {
        IReadOnlyList<IGame> games = await inner.GetAllGamesAsync().SafeAsync();
        return games.Select(this.Secure).ToList();
    }

    Task<TryResult<IGame>> IUnsecuredGameManager.TryGetGameByIdAsync(Guid gameId)
    {
        return inner.TryGetGameByIdAsync(gameId).Then(x => x.Apply(this.Secure));
    }

    Task<TryResult<IGame>> IUnsecuredGameManager.TryGetGameByNameAsync(Guid archiId, string name)
    {
        return inner.TryGetGameByNameAsync(archiId, name).Then(x => x.Apply(this.Secure));
    }

    async Task<IReadOnlyList<IGame>> IUnsecuredGameManager.GetGamesByArchiAsync(Guid archiId)
    {
        IReadOnlyList<IGame> games = await inner.GetGamesByArchiAsync(archiId).SafeAsync();
        return games.Select(this.Secure).ToList();
    }

    Task<Guid> IUnsecuredGameManager.CreateOfficialGameAsync(
        DiscordId uploaderId, 
        Guid archiId, 
        string name
    ) {
        if (user.Context.Role == Role.Banned) throw new AuthorizationException();
        return inner.CreateOfficialGameAsync(uploaderId, archiId, name);
    }

    Task<Guid> IUnsecuredGameManager.CreateUnofficialGameAsync(
        DiscordId uploaderId, 
        Guid archiId,
        string name, 
        string filename
    ) {
        if (user.Context.Role == Role.Banned) throw new AuthorizationException();
        return inner.CreateUnofficialGameAsync(uploaderId, archiId, name, filename);
    }

    private IGame Secure(IGame game)
    {
        return new SecuredGame(game, user.Context);
    }
}