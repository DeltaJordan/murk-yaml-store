using MurkYamlStore.Core;
using MurkYamlStore.Data.Games;
using MurkYamlStore.Domain.Users;
using MurkYamlStore.Util;

namespace MurkYamlStore.Domain.Games.Impl;

public sealed class Game(IGameDataProvider dataProvider, GameDto inner) : IGame
{
    private readonly Field<string> gameName = new(inner.GameName);
    private readonly Field<bool> isOfficial = new(inner.IsOfficial);
    private readonly Field<string> fileName = new(inner.FileName);
    private readonly Field<string> icon = new(inner.Icon);
    
    private RichUserRef uploader = inner.Uploader;
    
    Guid IGame.GameId { get; } = inner.GameId;
    Guid IGame.ArchiId { get; } = inner.ArchiId;
    RichUserRef IGame.Uploader => this.uploader;

    string IGame.GameName
    {
        get => this.gameName.Value;
        set => this.gameName.SetIfDifferent(value);
    }

    bool IGame.IsOfficial
    {
        get => this.isOfficial.Value;
        set => this.isOfficial.SetIfDifferent(value);
    }

    string IGame.FileName
    {
        get => this.fileName.Value;
        set => this.fileName.SetIfDifferent(value);
    }

    string IGame.Icon
    {
        get => this.icon.Value;
        set => this.icon.SetIfDifferent(value);
    }

    Task IGame.SaveAsync(IUser newUploader)
    {
        if (
            this.gameName.HasChanged || 
            this.isOfficial.HasChanged || 
            (!this.isOfficial.Value && this.fileName.HasChanged) || 
            this.icon.HasChanged || 
            newUploader.Discord.UserId != this.uploader.UserId
        ) {
            this.uploader = 
                new RichUserRef(newUploader.Discord.UserId, newUploader.Discord.Username, newUploader.Gender);

            if (!string.IsNullOrWhiteSpace(this.fileName.Value) && this.isOfficial.Value)
            {
                string apWorldPath = 
                    Path.Combine(FileHelpers.GetGameDirectory(inner.GameId, inner.ArchiId), this.fileName.Value);
                if (File.Exists(apWorldPath))
                {
                    File.Delete(apWorldPath);
                }
            }
            
            return dataProvider.EditGameAsync(
                inner.GameId, 
                this.uploader.UserId, 
                this.gameName.Value, 
                this.isOfficial.Value, 
                this.isOfficial.Value ? null : this.fileName.Value, 
                this.icon.Value
            );
        }
        
        return Task.CompletedTask;
    }

    Task IGame.DeleteAsync()
    {
        string gameDirectory = FileHelpers.GetGameDirectory(inner.GameId, inner.ArchiId);
        if (Directory.Exists(gameDirectory))
        {
            Directory.Delete(gameDirectory, true);
        }

        return dataProvider.DeleteGameAsync(inner.GameId);
    }
}