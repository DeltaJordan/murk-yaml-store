using MurkYamlStore.Core;

namespace MurkYamlStore.Domain.Games;

public interface IUnsecuredGameManager
{
    Task<IReadOnlyList<IGame>> GetAllGamesAsync();
    
    Task<TryResult<IGame>> TryGetGameByIdAsync(Guid gameId);
    Task<TryResult<IGame>> TryGetGameByNameAsync(Guid archiId, string name);
    
    Task<IReadOnlyList<IGame>> GetGamesByArchiAsync(Guid archiId);
    
    Task<Guid> CreateOfficialGameAsync(
        DiscordId uploaderId, 
        Guid archiId,
        string name
    );

    Task<Guid> CreateUnofficialGameAsync(
        DiscordId uploaderId,
        Guid archiId,
        string name,
        string filename
    );
}