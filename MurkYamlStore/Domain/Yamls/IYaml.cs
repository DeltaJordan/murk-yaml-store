using MurkYamlStore.Core;

namespace MurkYamlStore.Domain.Yamls;

public interface IYaml
{
    Guid YamlId { get; }
    Guid ArchiId { get; }
    RichUserRef Player { get; }
    DateTime DateUploaded { get; }
    Guid GameId { get; }
    string FileName { get; set; }
    
    Task SaveAsync();
    Task DeleteAsync();
}