using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.HttpException;

namespace MurkYamlStore.Domain.Yamls.Impl;

public sealed class SecuredYaml(IYaml inner, IUserContext user) : IYaml
{
    Guid IYaml.YamlId { get; } = inner.YamlId;
    Guid IYaml.ArchiId { get; } = inner.ArchiId;
    RichUserRef IYaml.Player { get; } = inner.Player;
    DateTime IYaml.DateUploaded { get; } = inner.DateUploaded;
    Guid IYaml.GameId { get; } = inner.GameId;

    string IYaml.FileName
    {
        get => inner.FileName;
        set => inner.FileName = value;
    }

    Task IYaml.SaveAsync()
    {
        if (!user.IsAtLeast(Role.Staff) && user.Discord.UserId != inner.Player.UserId)
            throw new AuthorizationException();
        
        return inner.SaveAsync();
    }

    Task IYaml.DeleteAsync()
    {
        if (!user.IsAtLeast(Role.Staff) && user.Discord.UserId != inner.Player.UserId)
            throw new AuthorizationException();
        
        return inner.DeleteAsync();
    }
}