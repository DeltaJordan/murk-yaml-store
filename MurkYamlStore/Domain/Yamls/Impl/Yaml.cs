using MurkYamlStore.Core;
using MurkYamlStore.Data.Yamls;
using MurkYamlStore.Util;

namespace MurkYamlStore.Domain.Yamls.Impl;

public sealed class Yaml(YamlDto inner, IYamlDataProvider dataProvider) : IYaml
{
    private readonly Field<string> fileName = new(inner.FileName);
    
    Guid IYaml.YamlId { get; } = inner.YamlId;
    Guid IYaml.ArchiId { get; } = inner.ArchiId;
    RichUserRef IYaml.Player { get; } = inner.Player;
    DateTime IYaml.DateUploaded { get; } = inner.DateUploaded;
    Guid IYaml.GameId { get; } = inner.GameId;

    string IYaml.FileName
    {
        get => this.fileName.Value;
        set => this.fileName.SetIfDifferent(value);
    }

    Task IYaml.SaveAsync()
    {
        if (this.fileName.HasChanged)
        {
            return dataProvider.EditYamlAsync(inner.YamlId, this.fileName.Value);
        }
        
        return Task.CompletedTask;
    }

    Task IYaml.DeleteAsync()
    {
        File.Delete(Path.Combine(FileHelpers.GetYamlDirectory(inner.ArchiId), inner.FileName));
        return dataProvider.DeleteYamlAsync(inner.YamlId);
    }
}