using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.HttpException;

namespace MurkYamlStore.Domain.Yamls.Impl;

public class SecuredYamlManager(IUnsecuredYamlManager inner, IUserContextProvider user) : IYamlManager
{
    Task<TryResult<IYaml>> IUnsecuredYamlManager.TryGetYamlAsync(Guid yamlId)
    {
        return inner.TryGetYamlAsync(yamlId).Then(x => x.Apply(this.Secure));
    }

    async Task<IReadOnlyList<IYaml>> IUnsecuredYamlManager.FetchYamlsByArchiAsync(Guid archiId, DiscordId? filterUserId, Guid? filterGameId)
    {
        IReadOnlyList<IYaml> yamls =
            await inner.FetchYamlsByArchiAsync(archiId, filterUserId, filterGameId).SafeAsync();
        return yamls.Select(this.Secure).ToList();
    }

    async Task<IReadOnlyList<IYaml>> IUnsecuredYamlManager.FetchYamlsByUserAsync(DiscordId userId)
    {
        IReadOnlyList<IYaml> yamls = await inner.FetchYamlsByUserAsync(userId).SafeAsync();
        return yamls.Select(this.Secure).ToList();
    }

    Task<Guid> IUnsecuredYamlManager.CreateYamlAsync(Guid archiId, DiscordId playerId, Guid gameId, string fileName)
    {
        if (user.Context.Role == Role.Banned) throw new AuthorizationException();
        return inner.CreateYamlAsync(archiId, playerId, gameId, fileName);
    }

    private IYaml Secure(IYaml yaml)
    {
        return new SecuredYaml(yaml, user.Context);
    }
}