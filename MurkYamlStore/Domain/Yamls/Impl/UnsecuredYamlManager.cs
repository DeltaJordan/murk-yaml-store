using MurkYamlStore.Core;
using MurkYamlStore.Data.Yamls;

namespace MurkYamlStore.Domain.Yamls.Impl;

internal sealed class UnsecuredYamlManager(
    IYamlDataProvider dataProvider
) : IUnsecuredYamlManager
{
    Task<TryResult<IYaml>> IUnsecuredYamlManager.TryGetYamlAsync(Guid yamlId)
    {
        return dataProvider.TryGetYamlAsync(yamlId).Then(x => x.Apply(this.Build));
    }

    async Task<IReadOnlyList<IYaml>> IUnsecuredYamlManager.FetchYamlsByArchiAsync(
        Guid archiId, 
        DiscordId? filterUserId, 
        Guid? filterGameId
    )
    {
        IReadOnlyList<YamlDto> dtos = 
            await dataProvider.GetYamlsByArchiAsync(archiId, filterUserId, filterGameId).SafeAsync();
        return dtos.Select(this.Build).ToList();
    }

    async Task<IReadOnlyList<IYaml>> IUnsecuredYamlManager.FetchYamlsByUserAsync(DiscordId userId)
    {
        IReadOnlyList<YamlDto> dtos = 
            await dataProvider.GetYamlsByUserAsync(userId).SafeAsync();
        return dtos.Select(this.Build).ToList();
    }

    Task<Guid> IUnsecuredYamlManager.CreateYamlAsync(Guid archiId, DiscordId playerId, Guid gameId, string fileName)
    {
        return dataProvider.CreateYamlAsync(archiId, playerId, gameId, fileName);
    }

    private IYaml Build(YamlDto dto)
    {
        return new Yaml(dto, dataProvider);
    }
}