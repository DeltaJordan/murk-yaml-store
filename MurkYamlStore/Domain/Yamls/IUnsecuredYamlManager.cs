using MurkYamlStore.Core;

namespace MurkYamlStore.Domain.Yamls;

public interface IUnsecuredYamlManager
{
    Task<TryResult<IYaml>> TryGetYamlAsync(Guid yamlId);

    Task<IReadOnlyList<IYaml>> FetchYamlsByArchiAsync(
        Guid archiId,
        DiscordId? filterUserId = null,
        Guid? filterGameId = null
    );
    
    Task<IReadOnlyList<IYaml>> FetchYamlsByUserAsync(DiscordId userId);
    
    Task<Guid> CreateYamlAsync(
        Guid archiId,
        DiscordId playerId,
        Guid gameId,
        string fileName
    );
}