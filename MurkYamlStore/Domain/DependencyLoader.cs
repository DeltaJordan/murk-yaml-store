using MurkYamlStore.Domain.Archis;
using MurkYamlStore.Domain.Archis.Impl;
using MurkYamlStore.Domain.Games;
using MurkYamlStore.Domain.Games.Impl;
using MurkYamlStore.Domain.News;
using MurkYamlStore.Domain.News.Impl;
using MurkYamlStore.Domain.Users;
using MurkYamlStore.Domain.Users.Impl;
using MurkYamlStore.Domain.Yamls;
using MurkYamlStore.Domain.Yamls.Impl;

namespace MurkYamlStore.Domain;

public static class DependencyLoader
{
    public static void Load(IServiceCollection registry)
    {
        registry.AddSingleton<IUserBookmarkUpdater, UserBookmarkUpdater>();

        registry.AddSingleton<IUserResolver, UserResolver>();

        registry.AddSingleton<IUnsecuredArchiManager, UnsecuredArchiManager>();
        registry.AddSingleton<IUnsecuredGameManager, UnsecuredGameManager>();
        registry.AddSingleton<IUnsecuredNewsPostManager, UnsecuredNewsPostManager>();
        registry.AddSingleton<IUnsecuredUserManager, UnsecuredUserManager>();
        registry.AddSingleton<IUnsecuredYamlManager, UnsecuredYamlManager>();

        registry.AddSingleton<IArchiManager, SecuredArchiManager>();
        registry.AddSingleton<IGameManager, SecuredGameManager>();
        registry.AddSingleton<INewsPostManager, SecuredNewsPostManager>();
        registry.AddSingleton<IUserManager, SecuredUserManager>();
        registry.AddSingleton<IYamlManager, SecuredYamlManager>();
    }
}