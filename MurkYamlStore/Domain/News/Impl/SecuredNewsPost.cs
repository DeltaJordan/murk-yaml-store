using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.HttpException;

namespace MurkYamlStore.Domain.News.Impl;

internal sealed class SecuredNewsPost(INewsPost inner, IUserContext userContext) : INewsPost
{
    Guid INewsPost.NewsPostId => inner.NewsPostId;
    RichUserRef INewsPost.Author => inner.Author;
    DateTime INewsPost.DatePosted => inner.DatePosted;
    DateTime? INewsPost.DateEdited => inner.DateEdited;

    string INewsPost.Title
    {
        get => inner.Title;
        set => inner.Title = value;
    }

    string INewsPost.Markdown
    {
        get => inner.Markdown;
        set => inner.Markdown = value;
    }
    
    string INewsPost.Message
    {
        get => inner.Message;
        set => inner.Message = value;
    }

    Task INewsPost.SaveAsync()
    {
        if (userContext?.Role != Role.Staff) throw new AuthorizationException();
        return inner.SaveAsync();
    }

    Task INewsPost.DeleteAsync()
    {
        if (userContext?.Role != Role.Staff) throw new AuthorizationException();
        return inner.DeleteAsync();
    }
}