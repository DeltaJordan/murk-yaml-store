using System.Diagnostics.CodeAnalysis;
using MurkYamlStore.Core;
using MurkYamlStore.Enums;
using MurkYamlStore.HttpException;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Domain.News.Impl;

[SuppressMessage("Performance", "CA1859:Use concrete types when possible for improved performance")]
public class SecuredNewsPostManager(
    IUnsecuredNewsPostManager inner, 
    IUserContextProvider user
) : INewsPostManager
{
    Task<IReadOnlyList<INewsPost>> IUnsecuredNewsPostManager.GetAllNewsPostsAsync()
    {
        IUserContext userContext = user.Context;
        return inner.GetAllNewsPostsAsync().Then(
            x => x.SelectOrTransform(p => Secure(p, userContext))
        );
    }

    Task<IReadOnlyList<INewsPost>> IUnsecuredNewsPostManager.GetNewsPostsPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        IUserContext userContext = user.Context;
        return inner.GetNewsPostsPagedAsync(pageSize, bookmark).Then(
            x => x.SelectOrTransform(p => Secure(p, userContext))
        );
    }

    Task<TryResult<INewsPost>> IUnsecuredNewsPostManager.TryGetNewsPostAsync(Guid postId)
    {
        return inner.TryGetNewsPostAsync(postId).Then(x => x.Apply(this.Secure));
    }

    Task<INewsPost> IUnsecuredNewsPostManager.CreateNewsPostAsync(DiscordId userId, string title, string content, string markdown)
    {
        IUserContext userContext = user.Context;
        if (userId != userContext?.Discord.UserId || userContext?.Role != Role.Staff)
            throw new AuthorizationException();
        return inner.CreateNewsPostAsync(userId, title, content, markdown).Then(p => Secure(p, userContext));
    }

    private INewsPost Secure(INewsPost post)
    {
        return new SecuredNewsPost(post, user.Context);
    }

    private static INewsPost Secure(INewsPost post, IUserContext userContext)
    {
        return new SecuredNewsPost(post, userContext);
    }
}