using MurkYamlStore.Core;
using MurkYamlStore.Data.News;

namespace MurkYamlStore.Domain.News.Impl;

internal sealed class NewsPost(INewsDataProvider dataProvider, NewsDto inner) : INewsPost
{
    private DateTime? dateEdited = inner.DateEdited;
    private readonly Field<string> message = new(inner.Message);
    private readonly Field<string> markdown = new(inner.Markdown);
    private readonly Field<string> title = new(inner.Title);

    Guid INewsPost.NewsPostId { get; } = inner.NewsPostId;
    RichUserRef INewsPost.Author { get; } = inner.Author;
    DateTime INewsPost.DatePosted { get; } = inner.DateCreated;

    DateTime? INewsPost.DateEdited => this.dateEdited;

    string INewsPost.Message
    {
        get => this.message.Value;
        set => this.message.SetIfDifferent(value);
    }

    string INewsPost.Markdown
    {
        get => this.markdown.Value;
        set => this.markdown.SetIfDifferent(value);
    }

    string INewsPost.Title
    {
        get => this.title.Value;
        set => this.title.SetIfDifferent(value);
    }

    async Task INewsPost.SaveAsync()
    {
        INewsPost @this = this;
        
        if (this.title.HasChanged || this.message.HasChanged || this.markdown.HasChanged)
        {
            this.dateEdited = DateTime.UtcNow;
            await dataProvider
                .EditNewsPostAsync(
                    @this.NewsPostId, 
                    this.title.Value,
                    this.message.Value, 
                    this.markdown.Value
                ).SafeAsync();
        }
    }

    Task INewsPost.DeleteAsync()
    {
        INewsPost @this = this;
        return dataProvider.DeleteNewsPostAsync(@this.NewsPostId);
    }
}