using MurkYamlStore.Core;
using MurkYamlStore.Data.News;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Domain.News.Impl;

internal sealed class UnsecuredNewsPostManager(INewsDataProvider dataProvider) : IUnsecuredNewsPostManager
{
    async Task<IReadOnlyList<INewsPost>> IUnsecuredNewsPostManager.GetAllNewsPostsAsync()
    {
        IEnumerable<NewsDto> dtos = await dataProvider.GetNewsPostsAsync().SafeAsync();
        return dtos.Select(this.Build).ToList();
    }

    async Task<IReadOnlyList<INewsPost>> IUnsecuredNewsPostManager.GetNewsPostsPagedAsync(int pageSize, BookmarkToken bookmark)
    {
        IReadOnlyList<NewsDto> dtos = await dataProvider.GetNewsPostsPagedAsync(pageSize, bookmark).SafeAsync();
        bookmark.Clear();
        if(dtos.Count >= pageSize) 
        {
            bookmark.PushValue(dtos[^1].DateCreated);
        }

        return dtos.Select(this.Build).ToList();
    }

    async Task<TryResult<INewsPost>> IUnsecuredNewsPostManager.TryGetNewsPostAsync(Guid postId)
    {
        TryResult<NewsDto> dto = await dataProvider.TryGetNewsPostAsync(postId).SafeAsync();
        return dto.Apply(this.Build);
    }

    async Task<INewsPost> IUnsecuredNewsPostManager.CreateNewsPostAsync(DiscordId userId, string title, string content, string markdown)
    {
        Guid postId = await dataProvider.CreateNewsPostAsync(userId, title, content, markdown).SafeAsync();
        TryResult<NewsDto> dto = await dataProvider.TryGetNewsPostAsync(postId).SafeAsync();
        return this.Build(dto.Value);
    }

    private INewsPost Build(NewsDto newsDto)
    {
        return new NewsPost(dataProvider, newsDto);
    }
}