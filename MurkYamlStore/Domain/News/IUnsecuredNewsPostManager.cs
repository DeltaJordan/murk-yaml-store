using MurkYamlStore.Core;
using MurkYamlStore.Pagination;

namespace MurkYamlStore.Domain.News;

public interface IUnsecuredNewsPostManager {

    Task<IReadOnlyList<INewsPost>> GetAllNewsPostsAsync();
    Task<IReadOnlyList<INewsPost>> GetNewsPostsPagedAsync( int pageSize, BookmarkToken bookmark );
    Task<TryResult<INewsPost>> TryGetNewsPostAsync( Guid postId );

    Task<INewsPost> CreateNewsPostAsync( DiscordId userId, string title, string content, string markdown );

}
