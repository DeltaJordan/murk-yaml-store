using MurkYamlStore.Core;

namespace MurkYamlStore.Domain.News;

public interface INewsPost
{
    Guid NewsPostId { get; }
    RichUserRef Author { get; }
    DateTime DatePosted { get; }
    DateTime? DateEdited { get; }
    string Message { get; set; }
    string Markdown { get; set; }
    string Title { get; set; }
    
    Task SaveAsync();
    Task DeleteAsync();
}