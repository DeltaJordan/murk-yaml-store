using System.Data;
using System.Diagnostics.CodeAnalysis;
using MurkYamlStore.Postgres;
using Newtonsoft.Json;

namespace MurkYamlStore.Core;

[JsonObject]
[method: JsonConstructor]
public readonly struct UserRef(
    DiscordId userId,
    [NotNull] string username
)
{
    [JsonProperty("userId")]
    public DiscordId UserId { get; } = userId;

    [JsonProperty("username")]
    public string Username { get; } = username;

    public static implicit operator WeakUserRef(UserRef userRef)
    {
        return new WeakUserRef(userRef.UserId, userRef.Username);
    }

    public static UserRef DbReader(IDataRecord row)
    {
        return new UserRef(
            row.Get<DiscordId>("user_id"),
            row.Get<string>("username")
        );
    }

    public override bool Equals(object obj)
    {
        return obj is UserRef other && this.UserId == other.UserId;
    }

    public override int GetHashCode()
    {
        return this.UserId.GetHashCode();
    }

}
