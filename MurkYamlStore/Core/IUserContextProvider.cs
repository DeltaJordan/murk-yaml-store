namespace MurkYamlStore.Core;

public interface IUserContextProvider
{
    bool TryGet(out IUserContext userContext);
    IUserContext Context { get; }
}