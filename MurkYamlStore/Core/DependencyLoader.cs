using MurkYamlStore.Core.Impl;

namespace MurkYamlStore.Core;

public static class DependencyLoader
{
    public static void Load(IServiceCollection registry)
    {
        registry.AddSingleton<UserContextInjectionSetupHandler>();
        registry.AddSingleton<IUserContextProvider, UserContextProvider>();
    }
}