using MurkYamlStore.Enums;
using Newtonsoft.Json;

namespace MurkYamlStore.Core;

[JsonObject]
public sealed class WeakRichUserRef
{
    public WeakRichUserRef(
        DiscordId? userId,
        string username,
        Gender? gender
    )
    {
        this.UserId = userId;
        this.Username = username;
        this.Gender = gender;
    }

    // Workaround JSON.NET bug that fails to serialize nullable structs in constructors
    [JsonConstructor]
    private WeakRichUserRef(
        [JsonProperty("userId", Required = Required.AllowNull)] string userId,
        [JsonProperty("username", Required = Required.Always)] string username,
        [JsonProperty("gender", Required = Required.AllowNull)] string gender
    )
    {
        this.UserId = (userId != null) ? DiscordId.Parse(userId) : null;
        this.Username = username;
        this.Gender = (gender != null) ? Enum.Parse<Gender>(gender) : null;
    }

    [JsonProperty("userId", NullValueHandling = NullValueHandling.Include)]
    public DiscordId? UserId { get; }

    [JsonProperty("username")]
    public string Username { get; }

    [JsonProperty("gender", NullValueHandling = NullValueHandling.Include)]
    public Gender? Gender { get; }

    public static implicit operator WeakUserRef(WeakRichUserRef userRef)
    {
        return new WeakUserRef(userRef.UserId, userRef.Username);
    }

    public override bool Equals(object obj)
    {
        return obj is RichUserRef other && this.UserId == other.UserId;
    }

    public override int GetHashCode()
    {
        return this.UserId.GetHashCode();
    }

}
