namespace MurkYamlStore.Core;

public sealed class FieldExt<T>(T initialValue) : Field<T>(initialValue)
{
    private T initialValue = initialValue;

    public T InitialValue => this.initialValue;

    public FieldExt<U> ApplyExt<U>(Func<T, U> transform)
    {
        FieldExt<U> field = new(transform(this.initialValue));
        field.value = transform(this.value);
        field.HasChanged = this.HasChanged;
        return field;
    }

    public override Field<U> Apply<U>(Func<T, U> transform)
    {
        return this.ApplyExt(transform);
    }

    public override void Commit()
    {
        this.initialValue = this.value;
        base.Commit();
    }
}