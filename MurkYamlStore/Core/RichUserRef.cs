using System.Diagnostics.CodeAnalysis;
using MurkYamlStore.Enums;
using Newtonsoft.Json;

namespace MurkYamlStore.Core;

[JsonObject]
[method: JsonConstructor]
public sealed class RichUserRef(
    DiscordId userId,
    [NotNull] string username,
    Gender? gender
)
{
    [JsonProperty("userId")]
    public DiscordId UserId { get; } = userId;

    [JsonProperty("username")]
    public string Username { get; } = username;

    [JsonProperty("gender", NullValueHandling = NullValueHandling.Include)]
    public Gender? Gender { get; } = gender;

    public static implicit operator WeakRichUserRef(RichUserRef userRef)
    {
        return new WeakRichUserRef(userRef.UserId, userRef.Username, userRef.Gender);
    }

    public static implicit operator WeakUserRef(RichUserRef userRef)
    {
        return new WeakUserRef(userRef.UserId, userRef.Username);
    }

    public static implicit operator UserRef(RichUserRef userRef)
    {
        return new UserRef(userRef.UserId, userRef.Username);
    }

    public override bool Equals(object obj)
    {
        return obj is RichUserRef other && this.UserId == other.UserId;
    }

    public override int GetHashCode()
    {
        return this.UserId.GetHashCode();
    }

}
