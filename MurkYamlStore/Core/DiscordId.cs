using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using MurkYamlStore.Postgres.TypeConverters;
using Newtonsoft.Json;
using NpgsqlTypes;

namespace MurkYamlStore.Core;

[TypeConverter(typeof(StringConverter))]
[JsonConverter(typeof(JsonSerializer))]
[PostgresTypeConverter(typeof(DbConverter))]
public readonly struct DiscordId(ulong value) : IComparable<DiscordId>, IEquatable<DiscordId>
{
    private readonly ulong value = value;

    public static DiscordId Empty { get; } = new(default);

    public static bool TryParse(string id, out DiscordId userId)
    {
        if (ulong.TryParse(id, out ulong oid))
        {
            userId = new DiscordId(oid);
            return true;
        }

        userId = default;
        return false;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static DiscordId Parse(string id)
    {
        return new DiscordId(ulong.Parse(id));
    }

    public readonly ReadOnlySpan<byte> Bytes => BitConverter.GetBytes(this.value);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly byte[] ToByteArray()
    {
        return BitConverter.GetBytes(this.value);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator ulong(DiscordId userId) => userId.value;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator DiscordId(ulong userId) => new(userId);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator ulong?(DiscordId? userId) => userId?.value;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static explicit operator DiscordId?(ulong? userId) => userId.HasValue ? new DiscordId(userId.Value) : null;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override int GetHashCode()
    {
        return this.value.GetHashCode();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override bool Equals(object obj)
    {
        return
            obj is DiscordId other &&
            this.value == other.value;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public readonly override string ToString()
    {
        return this.value.ToString();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator ==(DiscordId a, DiscordId b)
    {
        return a.value == b.value;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator !=(DiscordId a, DiscordId b)
    {
        return a.value != b.value;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator <(DiscordId a, DiscordId b)
    {
        return a.value < b.value;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator >(DiscordId a, DiscordId b)
    {
        return a.value > b.value;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator <=(DiscordId a, DiscordId b)
    {
        return a.value <= b.value;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool operator >=(DiscordId a, DiscordId b)
    {
        return a.value >= b.value;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    readonly bool IEquatable<DiscordId>.Equals(DiscordId other)
    {
        return this.value == other.value;
    }

    readonly int IComparable<DiscordId>.CompareTo(DiscordId other)
    {
        return this.value.CompareTo(other.value);
    }

    private sealed class StringConverter : TypeConverter
    {

        public override bool CanConvertFrom(
            ITypeDescriptorContext context,
            Type sourceType
        )
        {
            return sourceType == typeof(string) || sourceType == typeof(ulong);
        }

        public override object ConvertFrom(
            ITypeDescriptorContext context,
            CultureInfo culture,
            object value
        )
        {
            if (value is ulong idUlong) return new DiscordId(idUlong);
            if (value is not string idString) throw new NotSupportedException();
            return new DiscordId(ulong.Parse(idString));
        }

    }

    private sealed class JsonSerializer : JsonConverter<DiscordId>
    {

        public override DiscordId ReadJson(
            JsonReader reader,
            Type objectType,
            DiscordId existingValue,
            bool hasExistingValue,
            Newtonsoft.Json.JsonSerializer serializer
        )
        {
            return reader.TokenType switch
            {
                JsonToken.String => Parse(reader.Value as string),
                JsonToken.Integer => new DiscordId((reader.Value as ulong?).GetValueOrDefault()),
                JsonToken.Bytes => new DiscordId(BitConverter.ToUInt64(reader.Value as byte[])),
                _ => throw new JsonReaderException("Invalid UserId Format")
            };
        }

        public override void WriteJson(
            JsonWriter writer,
            DiscordId value,
            Newtonsoft.Json.JsonSerializer serializer
        )
        {
            writer.WriteValue(value.ToString());
        }

    }

    private sealed class DbConverter : IPostgresTypeConverter<DiscordId>
    {

        NpgsqlDbType IPostgresTypeConverter<DiscordId>.DatabaseType => NpgsqlDbType.Bytea;

        DiscordId IPostgresTypeConverter<DiscordId>.FromDbValue(object dbValue)
        {
            return new DiscordId(BitConverter.ToUInt64(dbValue as byte[]));
        }

        object IPostgresTypeConverter<DiscordId>.ToDbValue(DiscordId value)
        {
            return value.ToByteArray();
        }

    }
}
