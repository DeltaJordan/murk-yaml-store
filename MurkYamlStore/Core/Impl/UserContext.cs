using MurkYamlStore.Data.Users;
using MurkYamlStore.Enums;

namespace MurkYamlStore.Core.Impl;

internal sealed class UserContext(UserContextDto dto) : IUserContext
{

    private readonly DiscordRef discord = dto.Discord;
    private readonly Role role = dto.Role;

    DiscordRef IUserContext.Discord => this.discord;
    Role IUserContext.Role => this.role;
}
