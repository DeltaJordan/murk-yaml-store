using MurkYamlStore.Data.Users;
using MurkYamlStore.Util;

namespace MurkYamlStore.Core.Impl;

internal sealed class UserContextProvider(IHttpContextAccessor httpContextAccessor, IUserDataProvider userDataProvider) : IUserContextProvider
{
    private readonly IHttpContextAccessor httpContextAccessor = httpContextAccessor;
    private readonly IUserDataProvider userDataProvider = userDataProvider;

    IUserContext IUserContextProvider.Context
    {
        get
        {
            HttpContext context = this.httpContextAccessor.HttpContext;
            if (context != null && context.Items.TryGetValue("MurkUserContext", out object userContext))
            {
                return userContext as IUserContext;
            }

            if (context != null)
            {
                ulong? discordId = context.User.GetDiscordId();
                if (discordId.HasValue)
                {
                    TryResult<UserContextDto> user = 
                        AsyncHelper.RunSync(
                            () => this.userDataProvider.TryGetUserContextAsync((DiscordId)discordId.Value)
                        );

                    if (user.HasValue)
                    {
                        return new UserContext(user.Value);
                    }
                }
            }

            return null;
        }
    }

    bool IUserContextProvider.TryGet(out IUserContext userContext)
    {
        IUserContextProvider @this = this;
        userContext = @this.Context;
        return userContext != null;
    }
}
