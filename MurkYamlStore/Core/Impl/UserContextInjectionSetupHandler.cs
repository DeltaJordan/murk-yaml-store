using System.Security.Claims;
using Microsoft.AspNetCore.Mvc.Filters;
using MurkYamlStore.Data.Users;
using MurkYamlStore.Util;

namespace MurkYamlStore.Core.Impl;

internal sealed class UserContextInjectionSetupHandler(IUserDataProvider userDataProvider) : IAsyncActionFilter
{
    private readonly IUserDataProvider userDataProvider = userDataProvider;

    async Task IAsyncActionFilter.OnActionExecutionAsync(
            ActionExecutingContext context,
            ActionExecutionDelegate next
        )
    {
        TryResult<UserContextDto> contextDto = new();

        ClaimsPrincipal claimsPrincipal = context.HttpContext?.User;
        if (claimsPrincipal != null)
        {
            ulong? discordId = claimsPrincipal.GetDiscordId();
            string username = claimsPrincipal.GetUsername();
            string displayName = claimsPrincipal.GetDisplayName();
            string avatarHash = claimsPrincipal.GetAvatarHash();
            string bannerHash = claimsPrincipal.GetBannerHash();

            if (discordId.HasValue)
            {
                DiscordRef newDiscord = new(new DiscordId(discordId.Value), username, displayName, avatarHash, bannerHash);

                contextDto = await this.userDataProvider.TryGetUserContextAsync(new DiscordId(discordId.Value)).SafeAsync();
                if (!contextDto.HasValue)
                {
                    // This is a first time login, create user.
                    DiscordId newId = await this.userDataProvider.CreateNewUserAsync(newDiscord).SafeAsync();
                    contextDto = await this.userDataProvider.TryGetUserContextAsync(newId).SafeAsync();
                }
                else if (!contextDto.Value.Discord.ExactEquals(newDiscord))
                {
                    // The user changed stuff on their discord, update it.
                    bool changed = await this.userDataProvider.UpdateDiscordContextAsync(newDiscord).SafeAsync();
                    if (changed)
                    {
                        contextDto = await this.userDataProvider.TryGetUserContextAsync(newDiscord.UserId).SafeAsync();
                    }
                }
            }
        }

        if (!contextDto.HasValue)
        {
            await next().SafeAsync();
            return;
        }

        IUserContext userContext = new UserContext(contextDto.Value);

        context.HttpContext.Items.Add("MurkUserContext", userContext);
        await next().SafeAsync();
    }

}