namespace MurkYamlStore.Core;

public interface ITryResult
{
	bool HasValue { get; }
	object Value { get; }
}
