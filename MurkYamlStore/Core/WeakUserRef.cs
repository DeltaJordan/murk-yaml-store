using Newtonsoft.Json;

namespace MurkYamlStore.Core;

[JsonObject]
public readonly struct WeakUserRef
{
    public WeakUserRef(
        DiscordId? userId,
        string username
    )
    {
        this.UserId = userId;
        this.Username = username;
    }

    public WeakUserRef(
        string username
    )
    {
        this.UserId = null;
        this.Username = username;
    }

    // Workaround JSON.NET bug that fails to serialize nullable structs in constructors
    [JsonConstructor]
    private WeakUserRef(
        [JsonProperty("userId", Required = Required.AllowNull)] string userId,
        [JsonProperty("username", Required = Required.Always)] string username
    )
    {
        this.UserId = (userId != null) ? DiscordId.Parse(userId) : null;
        this.Username = username;
    }

    [JsonProperty("userId", NullValueHandling = NullValueHandling.Include)]
    public DiscordId? UserId { get; }

    [JsonProperty("username")]
    public string Username { get; }

    public override bool Equals(object obj)
    {
        return obj is WeakUserRef other &&
            this.UserId.Equals(other.UserId) &&
            this.Username.Equals(other.Username);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(this.UserId, this.Username);
    }

    public static bool operator ==(WeakUserRef left, WeakUserRef right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(WeakUserRef left, WeakUserRef right)
    {
        return !(left == right);
    }

}
