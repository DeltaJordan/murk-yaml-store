using System.ComponentModel;
using MurkYamlStore.Enums;

namespace MurkYamlStore.Core;

public interface IUserContext
{
    DiscordRef Discord { get; }
    Role Role { get; }
}

[EditorBrowsable(EditorBrowsableState.Never)]
public static class UserContextExtensions
{
    public static bool IsAtLeast(this IUserContext userContext, Role minimumRole)
    {
        return (userContext?.Role).AtLeast(minimumRole);
    }
}

