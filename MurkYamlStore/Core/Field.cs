namespace MurkYamlStore.Core;

public class Field<T>(T initialValue)
{

    protected T value = initialValue;

    public T Value
    {
        get => this.value;
        set
        {
            this.value = value;
            this.HasChanged = true;
        }
    }

    public bool HasChanged { get; protected set; } = false;

    public void Touch() { this.HasChanged = true; }

    public bool SetIfDifferent(T value)
    {
        if (Equals(this.value, value))
        {
            return false;
        }

        this.Value = value;
        return true;
    }

    public virtual Field<U> Apply<U>(Func<T, U> transform)
    {
        Field<U> field = new(transform(this.value))
        {
            HasChanged = this.HasChanged
        };

        return field;
    }

    public virtual void Commit()
    {
        this.HasChanged = false;
    }

}