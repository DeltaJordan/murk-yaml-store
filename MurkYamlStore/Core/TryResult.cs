namespace MurkYamlStore.Core;

public sealed class TryResult<T> : ITryResult
{

	public TryResult()
	{
		this.Value = default;
		this.HasValue = false;
	}

	public TryResult(T value)
	{
		this.Value = value;
		this.HasValue = true;
	}

	public bool HasValue { get; }
	public T Value { get; }

	object ITryResult.Value => this.Value;

	public TryResult<U> Apply<U>(Func<T, U> transform)
	{
		if (!this.HasValue) return new TryResult<U>();
		return new TryResult<U>(transform(this.Value));
	}

	public T ValueOrDefault(T defaultValue = default)
	{
		return this.HasValue ? this.Value : defaultValue;
	}

	public override bool Equals(object obj)
	{
		return obj is TryResult<T> other && (
			this.HasValue ? Equals(this.Value, other.Value) : !other.HasValue
		);
	}

	public override int GetHashCode()
	{
		return this.HasValue ? this.Value.GetHashCode() : 0;
	}

}
