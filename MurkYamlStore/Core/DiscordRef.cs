using System.Data;
using System.Diagnostics.CodeAnalysis;
using MurkYamlStore.Postgres;
using Newtonsoft.Json;

namespace MurkYamlStore.Core;

[JsonObject]
[method: JsonConstructor]
public readonly struct DiscordRef(
    DiscordId userId,
    [NotNull] string username,
    string displayName,
    string avatarHash,
    string bannerHash
) : IEquatable<DiscordRef>
{
    [JsonProperty("user_id")]
    public DiscordId UserId { get; } = userId;

    [JsonProperty("username")]
    public string Username { get; } = username;

    [JsonProperty("display_name")]
    public string DisplayName { get; } = displayName;

    [JsonProperty("avatar_hash")]
    public string AvatarHash { get; } = avatarHash;

    [JsonProperty("banner_hash")]
    public string BannerHash { get; } = bannerHash;

    public static DiscordRef DbReader(IDataRecord row)
    {
        return new DiscordRef(
            row.Get<DiscordId>("user_id"),
            row.Get<string>("username"),
            row.Get<string>("display_name"),
            row.Get<string>("avatar_hash"),
            row.Get<string>("banner_hash")
        );
    }

    /// <summary>
    /// Returns <see cref="DisplayName"/> if not null,
    /// otherwise returns <see cref="Username"/>.
    /// </summary>
    /// <returns></returns>
    public string GetDisplayNameOrUsername()
    {
        return string.IsNullOrWhiteSpace(this.DisplayName) ? this.Username : this.DisplayName;
    }

    /// <summary>
    /// Returns <see cref="true"/> if <paramref name="other"/> 
    /// is exactly the same (all properties match).
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public bool ExactEquals(DiscordRef other)
    {
        return this.UserId == other.UserId &&
            this.Username == other.Username &&
            this.DisplayName == other.DisplayName &&
            this.AvatarHash == other.AvatarHash &&
            this.BannerHash == other.BannerHash;
    }

    /// <summary>
    /// Returns <see cref="true"/> if <paramref name="obj"/> 
    /// has the same <see cref="UserId"/>.
    /// Does not check if they are exactly the same;
    /// see <see cref="ExactEquals"/> if that is needed.
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals([NotNullWhen(true)] object obj)
    {
        return obj is DiscordRef other && this.UserId == other.UserId;
    }

    public override int GetHashCode()
    {
        return this.UserId.GetHashCode();
    }

    /// <summary>
    /// Checks if these <see cref="DiscordRef"/>s 
    /// have the same <see cref="UserId"/>.
    /// Does not check if they are exactly the same;
    /// see <see cref="ExactEquals"/> if that is needed.
    /// </summary>
    /// <param name="left"></param>
    /// <param name="right"></param>
    /// <returns></returns>
    public static bool operator ==(DiscordRef left, DiscordRef right)
    {
        return left.Equals(right);
    }

    /// <summary>
    /// Returns true if <see cref="DiscordRef"/>s are different users
    /// by comparing each instance's <see cref="UserId"/>.
    /// </summary>
    /// <param name="left"></param>
    /// <param name="right"></param>
    /// <returns></returns>
    public static bool operator !=(DiscordRef left, DiscordRef right)
    {
        return !(left == right);
    }

    public bool Equals(DiscordRef other)
    {
        return this.UserId.Equals(other.UserId);
    }
}