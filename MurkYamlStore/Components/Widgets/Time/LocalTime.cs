using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using Microsoft.JSInterop;
using MurkYamlStore.Tools;
using MurkYamlStore.Tools.Impl;
using MurkYamlStore.Util;

namespace MurkYamlStore.Components.Widgets.Time;

public sealed class LocalTime : ComponentBase, IDisposable
{
    [Inject]
    public IBrowserTimeProvider BrowserTimeProvider { get; set; }
    [Inject]
    public IJSRuntime JSRuntime { get; set; }
    
    [Parameter]
    public DateTime? DateTime { get; set; }
    [Parameter]
    public string Format { get; set; }
    
    /// <summary>
    /// If true, ignores <see cref="DateTime.Kind"/> if it is <see cref="DateTimeKind.Unspecified"/>.
    /// </summary>
    [Parameter]
    public bool IgnoreKind { get; set; }

    protected override void OnInitialized()
    {
        this.BrowserTimeProvider.LocalTimeZoneChanged += this.LocalTimeZoneChangedAsync;
    }

    protected override void BuildRenderTree(RenderTreeBuilder builder)
    {
        if (this.DateTime.HasValue)
        {
            DateTime localTime = this.BrowserTimeProvider.ToLocalDateTime(this.DateTime.Value, this.IgnoreKind);

            if (string.IsNullOrWhiteSpace(this.Format))
            {
                builder.AddContent(0, localTime.ToString("U"));
            }
            else
            {
                builder.AddContent(0, localTime.ToString(this.Format));
            }
        }
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (firstRender)
        {
            try
            {
                await using var module = 
                    await this.JSRuntime.InvokeAsync<IJSObjectReference>("import", "./scripts/timezone.js").SafeAsync();
                string timeZone = await module.InvokeAsync<string>("getBrowserTimeZone").SafeAsync();
                await this.BrowserTimeProvider.SetBrowserTimeZoneAsync(timeZone).SafeAsync();
            }
            catch (JSDisconnectedException)
            {
            }
        }
    }

    private async Task LocalTimeZoneChangedAsync(object sender, EventArgs e)
    {
        await this.InvokeAsync(this.StateHasChanged).SafeAsync();
    }

    public void Dispose()
    {
        if (this.BrowserTimeProvider is BrowserTimeProvider browserTimeProvider)
        {
            browserTimeProvider.LocalTimeZoneChanged -= this.LocalTimeZoneChangedAsync;
        }
    }
}