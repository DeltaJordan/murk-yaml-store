using Blazored.Toast.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace MurkYamlStore.Components.Widgets.Error;

public class CustomErrorBoundary : ErrorBoundary
{
    [Inject]
    public IToastService ToastService { get; set; }
    [Inject]
    public ILogger<CustomErrorBoundary> Logger { get; set; }

    protected override Task OnErrorAsync(Exception exception)
    {
        this.ToastService.ShowError(
            $"""
            An exception occured while processing your request.
            {exception}                        
            """);
        this.Logger.LogError(exception, "Error occured while processing user request.");

        return Task.CompletedTask;
    }
}