@page "/NewArchi"
@using Cropper.Blazor.Models
@using MurkYamlStore.Core
@using MurkYamlStore.Domain.Archis
@using MurkYamlStore.Enums
@using MurkYamlStore.Util
@using MurkYamlStore.Components.Widgets
@using MurkYamlStore.Components.Widgets.Time
@using MurkYamlStore.Tools

@inject NavigationManager NavigationManager
@inject IToastService ToastService
@inject IUserContextProvider UserAuth
@inject IArchiManager ArchiManager
@inject ILogger<NewArchi> Logger
@inject IBrowserTimeProvider BrowserTimeProvider

<PageTitle>Create New Archipelago</PageTitle>

<div class="container">
    <div class="m-3 d-flex flex-column gap-2">
        <div class="card w-100">
            <form>
                <fieldset disabled="@this.isDisabled">
                    <div class="card-body d-flex flex-column gap-3">
                        <MurkCropper @ref="this.cropperRef"/>
                        <div class="input-group">
                            <span class="input-group-text">Title</span>
                            <input type="text" class="form-control" placeholder="My Awesome Archi Async"
                                   maxlength="@NewArchiModel.MAX_TITLE_LENGTH" @bind="@this.newArchi.Title"/>
                        </div>
                        <div class="input-group">
                            <span class="input-group-text">Description</span>
                            <textarea class="form-control" maxlength="@NewArchiModel.MAX_DESCRIPTION_LENGTH"
                                      @bind="@this.newArchi.Description">
                            </textarea>
                        </div>
                        <div class="input-group">
                            <span class="input-group-text">Submission Deadline</span>
                            <InputDate id="submissionDeadlineInput" class="form-control" TValue="DateTime?"
                                       @bind-Value="@this.newArchi.SubmissionDeadline" Type="InputDateType.DateTimeLocal"/>
                        </div>
                        <div class="input-group">
                            <span class="input-group-text">Start Date</span>
                            <InputDate id="startDateInput" class="form-control" TValue="DateTime?"
                                       @bind-Value="@this.newArchi.StartDate" Type="InputDateType.DateTimeLocal"/>
                        </div>
                        <div class="input-group">
                            <input type="checkbox" class="form-check-input" id="checkIsAsync"
                                   @bind="this.newArchi.IsAsync" role="switch" />
                            <label class="form-check-label" for="checkIsAsync">
                                Is this archi meant to be played asynchronously (over a long period of time)?
                            </label>
                        </div>
                        <div class="align-self-end">
                            <button type="button" class="btn btn-success" @onclick="@this.SaveNewArchiAsync">
                                <span class="fa fa-save"></span> Save
                            </button>
                            <button type="reset" class="btn btn-secondary">
                                Reset
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
        <h3 class="mb-0">Preview: </h3>
        <div class="card w-100">
            <div class="d-flex">
                <div class="archi-icon-preview rounded-start flex-shrink-0" @ref="@this.previewRef"></div>
                <div class="d-flex flex-column m-3 w-100">
                    <a href="javascript:void(0)" class="card-link fs-5 text-decoration-none">@this.newArchi.Title</a>
                    <p class="card-text flex-fill">@this.newArchi.Description</p>
                    <div class="d-flex justify-content-between">
                        <div class="d-flex flex-column">
                            <span class="card-text" style="font-size: .825rem;">Players</span>
                            <span class="card-text text-success">
                                @(new Random().Next(10, 100)) <span class="fa fa-user"></span>
                            </span>
                        </div>
                        <div class="d-flex flex-column">
                            <span class="card-text" style="font-size: .825rem;">Submission Deadline</span>
                            <span class="card-text">
                                <span class="fa fa-clock"></span>
                                <LocalTime DateTime="this.newArchi.SubmissionDeadline.GetValueOrDefault(DateTime.UtcNow)"
                                           IgnoreKind="true"/>
                            </span>
                            <span class="card-text" style="font-size: .825rem;">Start Date</span>
                            <span class="card-text">
                                <span class="fa fa-clock"></span>
                                <LocalTime DateTime="this.newArchi.StartDate.GetValueOrDefault(DateTime.UtcNow)"
                                           IgnoreKind="true"/>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@code 
{
    private TryResult<IUserContext> selfUser = new();
    
    private readonly NewArchiModel newArchi = new();
    private bool isDisabled;
    
    private ElementReference previewRef;
    private MurkCropper cropperRef;

    private sealed class NewArchiModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? SubmissionDeadline { get; set; }
        public bool IsAsync { get; set; }

        public const int MAX_TITLE_LENGTH = 32;
        public const int MAX_DESCRIPTION_LENGTH = 800;

        public bool IsValid()
        {
            return
                !string.IsNullOrWhiteSpace(this.Title) &&
                !string.IsNullOrWhiteSpace(this.Description) &&
                this.StartDate.HasValue &&
                this.SubmissionDeadline.HasValue &&
                this.Title.Length <= MAX_TITLE_LENGTH &&
                this.Description.Length <= MAX_DESCRIPTION_LENGTH;
        }
    }

    private async Task SaveNewArchiAsync()
    {
        this.isDisabled = true;

        if (!this.newArchi.IsValid())
        {
            // Shouldn't reach here, but still should fail here in case we do.
            this.ToastService.ShowError("Some or all fields are invalid.");
            this.isDisabled = false;
            return;
        }

        if (this.BrowserTimeProvider.LocalTimeZone == null)
        {
            this.ToastService.ShowError("Unable to determine time zone. " +
                                        "This information is needed for submission deadlines. " +
                                        "Aborting Archi creation.");
            this.isDisabled = false;
            return;
        }

        DateTime submissionDeadline = DateTime.SpecifyKind(
            TimeZoneInfo.ConvertTimeToUtc(
                this.newArchi.SubmissionDeadline!.Value, 
                this.BrowserTimeProvider.LocalTimeZone
            ),
            DateTimeKind.Utc
        );

        DateTime startDateUtc = DateTime.SpecifyKind(
            TimeZoneInfo.ConvertTimeToUtc(
                this.newArchi.StartDate!.Value, 
                this.BrowserTimeProvider.LocalTimeZone
            ),
            DateTimeKind.Utc
        );
        
        IArchi archi = await this.ArchiManager.CreateArchiAsync(
            this.selfUser.Value.Discord.UserId,
            this.newArchi.Title,
            this.newArchi.Description,
            submissionDeadline,
            startDateUtc,
            this.newArchi.IsAsync
        ).SafeAsync();

        if (this.cropperRef.HasNewImage)
        {
            string iconPath;
            try
            {
                iconPath = Path.Combine(FileHelpers.GetArchiDirectory(archi.ArchiId), "icon.png");
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex);
                throw;
            }

            try
            {
                await this.cropperRef.SaveCroppedImageToFile(iconPath);
            }
            catch (Exception ex)
            {
                this.Logger.LogWarning(
                    ex, 
                    "An error occured while saving cropped Archi icon. Will attempt to salvage state."
                );
            }

            if (File.Exists(iconPath))
            {
                archi.Icon = "icon.png";
                await archi.SaveAsync().SafeAsync();
            }
        }
        
        await this.InvokeAsync(
            () => this.NavigationManager.NavigateTo($"/Archi/{archi.ArchiId}")
        ).SafeAsync();
    }
    
    protected override Task OnInitializedAsync()
    {
        if (this.UserAuth?.Context != null)
        {
            this.selfUser = new TryResult<IUserContext>(this.UserAuth.Context);
        }
        
        if (!this.selfUser.HasValue || !this.selfUser.Value.IsAtLeast(Role.Staff))
        {
            this.NavigationManager.NavigateTo("/");
            return Task.CompletedTask;
        }
        
        return Task.CompletedTask;
    }

    protected override void OnAfterRender(bool firstRender)
    {
        if (firstRender)
        {
            this.cropperRef.SetPreviewReference(this.previewRef);
        }
    }
}