using Blazored.Toast.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using MurkYamlStore.Components.Widgets;
using MurkYamlStore.Core;
using MurkYamlStore.Domain.Archis;
using MurkYamlStore.Domain.Games;
using MurkYamlStore.Domain.Users;
using MurkYamlStore.Domain.Yamls;
using MurkYamlStore.Enums;
using MurkYamlStore.Util;
using YamlDotNet.RepresentationModel;

namespace MurkYamlStore.Components.Pages.Archi
{
    public partial class Archi
    {
        [Inject]
        public NavigationManager NavigationManager { get; set; }
        [Inject]
        public IToastService ToastService { get; set; }
        [Inject]
        public IUserContextProvider UserAuth { get; set; }
        [Inject]
        public IUserManager UserManager { get; set; }
        [Inject]
        public IArchiManager ArchiManager { get; set; }
        [Inject]
        public IGameManager GameManager { get; set; }
        [Inject]
        public IYamlManager YamlManager { get; set; }

        [Parameter]
        public string ArchiSlug { get; set; }

        private TryResult<IUserContext> selfUser = new();

        private long playerCount;
        private long yamlCount;

        private IReadOnlyList<IYaml> userYamls;
        private Dictionary<Guid, IGame> userYamlGames;

        private ICollection<IYaml> allArchiYamls;
        private Dictionary<Guid, IGame> allArchiGames;

        private IArchi currentArchi;

        private string yamlUploadId = Guid.NewGuid().ToString();
        private string apWorldUploadId = Guid.NewGuid().ToString();

        private NewGameModel newGameModel;
        private sealed class NewGameModel
        {
            public bool IsOfficial { get; set; }
            public string GameName { get; set; }
            public IBrowserFile ApWorldFile { get; set; }
        }

        private NewYamlModel newYamlModel;
        private sealed class NewYamlModel
        {
            public string FileName { get; init; }
        }

        private MurkCropper murkCropper;
        private ElementReference gamePreviewReference;

        private IBrowserFile newSeedFile;
        private string seedUploadId = Guid.NewGuid().ToString();

        protected override async Task OnInitializedAsync()
        {
            if (!Guid.TryParse(this.ArchiSlug, out Guid archiId))
            {
                this.NavigationManager.NavigateTo("404");
                return;
            }

            TryResult<IArchi> archi = await this.ArchiManager.TryGetArchiAsync(archiId).SafeAsync();
            if (!archi.HasValue)
            {
                this.NavigationManager.NavigateTo("404");
                return;
            }

            this.currentArchi = archi.Value;

            IReadOnlyList<IYaml> yamls = await this.YamlManager.FetchYamlsByArchiAsync(archiId).SafeAsync();
            this.allArchiYamls = yamls.CoerceToICollection();
            IReadOnlyList<IGame> games = await this.GameManager.GetGamesByArchiAsync(archiId).SafeAsync();
            this.allArchiGames = games.ToDictionary(x => x.GameId);

            this.playerCount = await this.ArchiManager.GetArchiPlayerCountAsync(archiId).SafeAsync();
            this.yamlCount = await this.ArchiManager.GetArchiYamlCountAsync(archiId).SafeAsync();

            if (this.UserAuth?.Context == null)
            {
                return;
            }

            this.selfUser = new TryResult<IUserContext>(this.UserAuth.Context);

            this.userYamls = await this.YamlManager
                .FetchYamlsByArchiAsync(archiId, this.selfUser.Value.Discord.UserId)
                .SafeAsync();
            
            this.userYamlGames = [];
            foreach (IYaml yaml in this.userYamls)
            {
                if (!this.userYamlGames.ContainsKey(yaml.GameId))
                {
                    TryResult<IGame> game = await this.GameManager.TryGetGameByIdAsync(yaml.GameId).SafeAsync();
                    this.userYamlGames[yaml.GameId] = game.Value;
                }
            }
        }

        private bool CanDownloadAllYamls()
        {
            return (
                this.selfUser.HasValue && (
                    this.selfUser.Value.IsAtLeast(Role.Staff) ||
                    this.selfUser.Value.Discord.UserId == this.currentArchi.Author.UserId
                )
            ) || this.currentArchi.SubmissionDeadline <= DateTime.UtcNow.Date;
        }

        private bool CanUploadSeed()
        {
            return
                this.currentArchi.SubmissionDeadline <= DateTime.UtcNow &&
                this.selfUser.HasValue && (
                    this.selfUser.IsAtLeast(Role.Staff) ||
                    this.selfUser.Value.Discord.UserId == this.currentArchi.Author.UserId
                );
        }

        private bool CanMarkAsCompleted()
        {
            return !this.currentArchi.DateCompleted.HasValue &&
                   this.selfUser.HasValue && (
                       this.selfUser.IsAtLeast(Role.Staff) ||
                       this.selfUser.Value.Discord.UserId == this.currentArchi.Author.UserId
                   );
        }

        private async Task MarkAsCompletedAsync()
        {
            this.currentArchi.DateCompleted = DateTime.UtcNow;
            await this.currentArchi.SaveAsync().SafeAsync();
        }
            
        private async Task DeleteYamlAsync(IYaml yaml)
        {
            await yaml.DeleteAsync().SafeAsync();
            await this.OnInitializedAsync().SafeAsync();
            await this.InvokeAsync(this.StateHasChanged).SafeAsync();
        }

        private async Task SaveYamlAndGameAsync()
        {
            if (!this.selfUser.HasValue)
            {
                this.ToastService.ShowError("You are not logged in.");
                return;
            }
            
            this.ToastService.ShowInfo("Saving a lot of data... Don't turn off the power.");

            DiscordId userId = this.selfUser.Value.Discord.UserId;

            Guid gameId;
            
            // Check to make sure game doesn't already exist.
            TryResult<IGame> game = await this.GameManager.TryGetGameByNameAsync(
                this.currentArchi.ArchiId, 
                this.newGameModel.GameName
            ).SafeAsync();
            
            if (game.HasValue)
            {
                gameId = game.Value.GameId;
                this.ToastService.ShowWarning(
                    "It seems like this game already exists in this archi. " +
                    "Your icon won't be used, but everything else should work properly. " +
                    "Also, don't touch anything; the upload is still processing. " +
                    "If you see this warning, please report this to DeltaJordan on the Discord."
                );
            }
            else if (this.newGameModel.IsOfficial)
            {
                gameId = await this.GameManager.CreateOfficialGameAsync(
                    userId,
                    this.currentArchi.ArchiId,
                    this.newGameModel.GameName
                ).SafeAsync();
            }
            else
            {
                string fileName = Path.GetFileNameWithoutExtension(this.newGameModel.ApWorldFile.Name) + ".apworld";
                gameId = await this.GameManager.CreateUnofficialGameAsync(
                    userId,
                    this.currentArchi.ArchiId,
                    this.newGameModel.GameName,
                    fileName
                ).SafeAsync();
                
                string filePath =
                    Path.Combine(FileHelpers.GetGameDirectory(gameId, this.currentArchi.ArchiId), fileName);

                try
                {
                    await FileTransferUtil.UploadAndSaveAsync(filePath, this.newGameModel.ApWorldFile).SafeAsync();
                }
                catch (Exception)
                {
                    this.ToastService.ShowError(
                        "Fatal error occured while saving game file. YAML will not be created."
                    );
                    
                    if (Directory.Exists(FileHelpers.GetGameDirectory(gameId, this.currentArchi.ArchiId)))
                    {
                        Directory.Delete(FileHelpers.GetGameDirectory(gameId, this.currentArchi.ArchiId), true);
                    }
                    
                    game = await this.GameManager.TryGetGameByIdAsync(gameId).SafeAsync();
                    if (game.HasValue)
                    {
                        await game.Value.DeleteAsync().SafeAsync();
                    }
                    
                    return;
                }
            }

            game = await this.GameManager.TryGetGameByIdAsync(gameId).SafeAsync();
            if (!game.HasValue)
            {
                this.ToastService.ShowError("Fatal error occured while saving game data. YAML will not be created.");
                    
                if (Directory.Exists(FileHelpers.GetGameDirectory(gameId, this.currentArchi.ArchiId)))
                {
                    Directory.Delete(FileHelpers.GetGameDirectory(gameId, this.currentArchi.ArchiId), true);
                }
                
                return;
            }

            if (this.murkCropper.HasNewImage)
            {
                string iconPath = Path.Combine(FileHelpers.GetGameDirectory(gameId, this.currentArchi.ArchiId), "icon.png");
                await this.murkCropper.SaveCroppedImageToFile(iconPath).SafeAsync();
                game.Value.Icon = "icon.png";

                IUser user = await this.UserManager.GetUserByIdAsync(userId).SafeAsync();
                await game.Value.SaveAsync(user).SafeAsync();
            }

            await this.YamlManager.CreateYamlAsync(
                this.currentArchi.ArchiId,
                userId,
                gameId,
                this.newYamlModel.FileName
            ).SafeAsync();

            this.newYamlModel = null;
            this.newGameModel = null;
            this.yamlUploadId = Guid.NewGuid().ToString();
            
            this.ToastService.ShowSuccess("Your entry to this archi has been saved!");

            await this.OnInitializedAsync().SafeAsync();
            await this.InvokeAsync(this.StateHasChanged).SafeAsync();
        }

        private async Task CancelSaveYamlAndGameAsync()
        {
            string yamlPath =
                Path.Combine(FileHelpers.GetYamlDirectory(this.currentArchi.ArchiId), this.newYamlModel.FileName);
            if (File.Exists(yamlPath))
            {
                File.Delete(yamlPath);
            }

            this.newGameModel = null;
            this.newYamlModel = null;

            await this.InvokeAsync(this.StateHasChanged).SafeAsync();
        }

        private bool HasSubmissionDeadlinePassed()
        {
            return this.currentArchi.SubmissionDeadline >= DateTime.UtcNow;
        }

        private async Task SaveSeedAsync()
        {
            if (
                !this.selfUser.HasValue || (
                    !this.selfUser.IsAtLeast(Role.Staff) &&
                    this.selfUser.Value.Discord.UserId != this.currentArchi.Author.UserId
                )
            )
            {
                this.ToastService.ShowError("You are not permitted to perform this action. Are you logged in?");
                return;
            }

            if (!string.IsNullOrWhiteSpace(this.currentArchi.SeedFileName))
            {
                string oldFilePath = Path.Combine(
                    FileHelpers.GetArchiDirectory(this.currentArchi.ArchiId), 
                    this.currentArchi.SeedFileName
                );

                if (File.Exists(oldFilePath))
                {
                    File.Delete(oldFilePath);
                }
            }
        
            string fileName = Path.GetFileNameWithoutExtension(this.newSeedFile.Name) + ".zip";
            string filePath =
                Path.Combine(FileHelpers.GetArchiDirectory(this.currentArchi.ArchiId), fileName);

            try
            {
                this.ToastService.ShowInfo(
                    "Saving seed, this will take a while. " +
                    "Do not reload the page or navigate to other pages until you see a confirmation toast.",
                    settings =>
                    {
                        settings.DisableTimeout = true;
                        settings.ShowCloseButton = true;
                    }
                );
                
                await FileTransferUtil
                    .UploadAndSaveAsync(filePath, this.newSeedFile, 1024L * 1024L * 1024L).SafeAsync();
            }
            catch (Exception e)
            {
                this.ToastService.ShowError($"Fatal error occured while saving seed.\n{e.Message}");
                return;
            }

            this.currentArchi.SeedFileName = fileName;
            await this.currentArchi.SaveAsync().SafeAsync();
            this.seedUploadId = Guid.NewGuid().ToString();
        
            this.ToastService.ShowSuccess("Successfully uploaded seed. You may close all toasts.", settings =>
            {
                settings.DisableTimeout = true;
                settings.ShowCloseButton = true;
            });
        }

        private void OnSeedFileChanged(InputFileChangeEventArgs e)
        {
            this.newSeedFile = e.File;
        }

        private void OnApWorldFileChanged(InputFileChangeEventArgs e)
        {
            if (!FileHelpers.IsFileSizeValid(e.File.Size))
            {
                this.ToastService.ShowError(
                    $"apworlds cannot be larger than {ServerConfig.Instance.UploadSizeLimit}MiB." +
                    "\nIf a larger file size is needed for your apworld, contact DeltaJordan on Discord."
                );
                this.newGameModel.ApWorldFile = null;
                this.apWorldUploadId = Guid.NewGuid().ToString();
                return;
            }

            this.newGameModel.ApWorldFile = e.File;
        }

        private async Task OnYamlFileChangedAsync(InputFileChangeEventArgs e)
        {
            if (!this.selfUser.HasValue)
            {
                this.ToastService.ShowError("You are not logged in.");
                return;
            }

            if (FileHelpers.IsFileSizeValid(e.File.Size))
            {
                string fileName = Path.GetFileNameWithoutExtension(e.File.Name) + ".yaml";
                string filePath = FileHelpers.MakeUniqueFilename(
                Path.Combine(
                    FileHelpers.GetYamlDirectory(this.currentArchi.ArchiId),
                        fileName
                    )
                );
                fileName = Path.GetFileName(filePath);

                string gameName = null;
                try
                {
                    await FileTransferUtil.UploadAndSaveAsync(filePath, e.File, 512L * 1024L).SafeAsync();

                    string yamlContent = await File.ReadAllTextAsync(filePath).SafeAsync();
                    StringReader stringReader = new(yamlContent);
                    YamlStream yaml = [];
                    yaml.Load(stringReader);
                    YamlMappingNode mapping = (YamlMappingNode)yaml.Documents[0].RootNode;
                    gameName = mapping.Children[new YamlScalarNode("game")].ToString();
                }
                catch (Exception)
                {
                    this.ToastService.ShowError("Failed to read YAML; your file is probably invalid.");
                    this.yamlUploadId = Guid.NewGuid().ToString();
                    return;
                }

                if (string.IsNullOrWhiteSpace(gameName))
                {
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                    }

                    return;
                }

                TryResult<IGame> game = 
                    await this.GameManager.TryGetGameByNameAsync(this.currentArchi.ArchiId, gameName).SafeAsync();
                if (game.HasValue)
                {
                    await this.YamlManager.CreateYamlAsync(
                        this.currentArchi.ArchiId,
                        this.selfUser.Value.Discord.UserId,
                        game.Value.GameId,
                        fileName
                    ).SafeAsync();
                    this.yamlUploadId = Guid.NewGuid().ToString();
                    this.ToastService.ShowSuccess("Successfully uploaded YAML.");
                    await this.OnInitializedAsync().SafeAsync();
                }
                else
                {
                    // There is not a game associated with this;
                    // The user needs to add one.
                    this.newGameModel = new NewGameModel
                    {
                        GameName = gameName
                    };

                    this.newYamlModel = new NewYamlModel
                    {
                        FileName = fileName
                    };
                }

                await this.InvokeAsync(this.StateHasChanged).SafeAsync();
            }
            else
            {
                this.ToastService.ShowError($"YAMLs cannot be larger than {ServerConfig.Instance.UploadSizeLimit}MiB.");
                this.yamlUploadId = Guid.NewGuid().ToString();
            }
        }

        private void MurkCropperAfterRender()
        {
            if (this.gamePreviewReference.Context != null)
            {
                this.murkCropper.SetPreviewReference(this.gamePreviewReference);
            }
        }
    }
}