#pragma warning disable MURK002

global using LogLevel = NLog.LogLevel;
using Blazored.Toast;
using Cropper.Blazor.Extensions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using MurkYamlStore;
using MurkYamlStore.Components;
using MurkYamlStore.Core;
using MurkYamlStore.Core.Impl;
using MurkYamlStore.Postgres.Migration;
using MurkYamlStore.Tools;
using MurkYamlStore.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NLog;
using NLog.Targets;
using NLog.Web;

using ErrorEventArgs = Newtonsoft.Json.Serialization.ErrorEventArgs;

const string LOG_LAYOUT = "${longdate}|${event-properties:item=EventId:whenEmpty=0}|${level:uppercase=true}|${logger}|${message} ${exception:format=tostring}";

Logger logger = LogManager.Setup().LoadConfiguration(builder =>
{
    builder
    .ForLogger()
    .FilterMinLevel(ServerConfig.Instance.LogLevel)
    .WriteTo(new FileTarget
    {
        Name = "allfile",
        FileName = ServerConfig.Instance.LogFilePath,
        Layout = LOG_LAYOUT,
        KeepFileOpen = false,
        ArchiveAboveSize = 1024 * 1024,
        ArchiveNumbering = ArchiveNumberingMode.Rolling,
        MaxArchiveFiles = 10
    })
    .WriteToColoredConsole(LOG_LAYOUT);
}).GetCurrentClassLogger();
logger.Debug("Initializing main...");

try
{
    WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

    MasterDependencyLoader.Load(builder.Services);
    builder.Services.AddHttpContextAccessor();
    builder.Services.AddMemoryCache();
    builder.Services.AddResponseCompression();
    builder.Services.AddBlazoredToast();
    builder.Services.AddCropper();
    builder.Services.AddSystemd();

    builder.Services.AddAuthentication(options =>
    {
        options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    })
    .AddCookie(options =>
    {
        options.LoginPath = "/auth/login";
        options.LogoutPath = "/auth/logout";
    })
    .AddDiscord(options =>
    {
        options.ClientId = ServerConfig.Instance.DiscordClientId;
        options.ClientSecret = ServerConfig.Instance.DiscordSecret;
        options.CallbackPath = "/auth/discord";
        options.ClaimActions.MapJsonKey(DiscordUtil.DISPLAY_NAME_CLAIM, "global_name");
        options.ClaimActions.MapJsonKey(DiscordUtil.BANNER_CLAIM, "banner");
    });

    builder.Services.AddAuthorization();

    builder.Services.AddCors(options =>
    {
        options.AddDefaultPolicy(builder =>
        {
            builder.WithOrigins(ServerConfig.Instance.AllowedOrigins);
            builder.WithHeaders(
                "Content-Type",
                "Authorization",
                "Content-Disposition",
                "User-Agent",
                "X-Requested-With",
                "X-Forwarded-For",
                "X-Real-IP",
                "X-Filename",
                "X-Source-Page"
            );
            builder.WithExposedHeaders(
                "Retry-After",
                "WWW-Authenticate",
                "Warning",
                "X-Role-Changed",
                "X-Invalid-Parameter"
            );
            builder.AllowCredentials();
            builder.AllowAnyMethod();
        });
    });

    builder.Services.AddControllersWithViews(options =>
    {
        options.Filters.AddService<UserContextInjectionSetupHandler>();
    }).AddNewtonsoftJson(options =>
    {
        ConfigureJsonSerializerSettings(options.SerializerSettings);
        options.SerializerSettings.Error += delegate (object sender, ErrorEventArgs args)
        {
            if (!args.ErrorContext.Handled || args.ErrorContext.Error != null)
            {
                throw args.ErrorContext.Error;
            }
        };
    });

    builder.Services.AddRazorComponents()
        .AddInteractiveServerComponents()
        .AddHubOptions(options =>
        {
            options.MaximumReceiveMessageSize = null;
        })
        .AddCircuitOptions(options =>
        {
            options.DetailedErrors = true;
        });

    if (!builder.Environment.IsDevelopment())
    {
        builder.WebHost.UseStaticWebAssets();
    }

    // NLog: Setup NLog for Dependency injection
    builder.Logging.ClearProviders();
    builder.Host.UseNLog();

    JsonConvert.DefaultSettings = () =>
    {
        JsonSerializerSettings settings = new();
        ConfigureJsonSerializerSettings(settings);
        return settings;
    };

    WebApplication app = builder.Build();

    // Configure the HTTP request pipeline.
    if (!app.Environment.IsDevelopment())
    {
        app.UseExceptionHandler("/Error", createScopeForErrors: true);
        // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
        app.UseHsts();
    }

    app.UseHttpsRedirection();
    app.UseStaticFiles();

    app.UseRouting();
    app.UseCors();

    app.UseAuthentication();
    app.UseAuthorization();
    app.UseAntiforgery();
    app.UseResponseCompression();

    app.MapControllers();

    app.MapRazorComponents<App>()
        .AddInteractiveServerRenderMode();

    await app.Services
        .CreateScope()
        .ServiceProvider
        .GetRequiredService<IMigrationRunner>()
        .RunDatabaseMigrationsAsync()
        .SafeAsync();

    app.Run();
}
catch (Exception exception)
{
    logger.Error(exception, "Stopped program because of exception.");
    throw;
}
finally
{
    LogManager.Shutdown();
}

return;

static void ConfigureJsonSerializerSettings(JsonSerializerSettings settings)
{
    settings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
    settings.MissingMemberHandling = MissingMemberHandling.Ignore;
    settings.NullValueHandling = NullValueHandling.Include;
    settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
    settings.PreserveReferencesHandling = PreserveReferencesHandling.None;
    settings.Converters.Add(new StringEnumConverter { AllowIntegerValues = false });
}