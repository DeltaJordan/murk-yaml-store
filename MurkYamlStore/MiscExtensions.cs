using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using MurkYamlStore.Core;
using MurkYamlStore.Util;

namespace MurkYamlStore;

[EditorBrowsable(EditorBrowsableState.Never)]
public static partial class MiscExtensions
{
	[GeneratedRegex("[^\\s\\u0000-\\u001F]", RegexOptions.Multiline | RegexOptions.Compiled | RegexOptions.ECMAScript | RegexOptions.CultureInvariant)]
	private static partial Regex MyRegex();

	public static void SafeDispose(
		this IDisposable @this
	)
	{
		@this?.Dispose();
	}

	public static ValueTask SafeDisposeAsync(
		this IAsyncDisposable @this
	)
	{
		if (@this == null) return ValueTask.CompletedTask;
		return @this.DisposeAsync();
	}

	public static IActionResult WithHeader(
		this IActionResult actionResult,
		string headerName,
		StringValues headerValue
	)
	{
		if (actionResult is HeaderActionResult har)
		{
			har.AddHeader(headerName, headerValue);
			return actionResult;
		}
		else
		{
			har = new HeaderActionResult(actionResult);
			har.AddHeader(headerName, headerValue);
			return har;
		}
	}

	public static T? ToNullable<T>(
		this TryResult<T> tryResult
	) where T : struct
	{
		return tryResult.HasValue ? tryResult.Value : null;
	}

	public static TryResult<T> ToTryResult<T>(
		this T? nullable
	) where T : struct
	{
		return nullable.HasValue ? new TryResult<T>(nullable.Value) : new TryResult<T>();
	}

	public static string ToISOString(this DateTime dateTime)
	{
		return dateTime.ToUniversalTime().ToString(
			format: "yyyy-MM-ddTHH:mm:ss.fffZ",
			provider: CultureInfo.InvariantCulture
		);
	}

	public static long ToUnixTime(this DateTime dateTime)
	{
		return dateTime.ToUniversalTime().Subtract(DateTime.UnixEpoch).Ticks / TimeSpan.TicksPerSecond;
	}

	public static long ToUnixTimeMs(this DateTime dateTime)
	{
		return dateTime.ToUniversalTime().Subtract(DateTime.UnixEpoch).Ticks / TimeSpan.TicksPerMillisecond;
	}

	private static readonly Regex HasPrintableRegex = MyRegex();

	public static bool ContainsPrintableCharacter(this string str)
	{
		return str != null && HasPrintableRegex.IsMatch(str);
	}

	public static int CountBits(this uint number)
	{
		int bitCount = 0;
		while (number != 0)
		{
			number &= number - 1;
			bitCount++;
		}
		return bitCount;
	}

	public static int CountBits(this byte number)
	{
		return ((uint)number).CountBits();
	}

	public static bool TryParseJsBool(
		this string boolStr,
		out bool value
	)
	{
		if (string.IsNullOrEmpty(boolStr))
		{
			value = false;
			return false;
		}

		if (boolStr.StartsWith('"') && boolStr.EndsWith('"'))
		{
			boolStr = boolStr[1..^1];
		}

		boolStr = boolStr.ToLowerInvariant();
		if (boolStr.Equals("true"))
		{
			value = true;
			return true;
		}
		else if (boolStr.Equals("false"))
		{
			value = false;
			return true;
		}

		value = false;
		return false;
	}
}
