using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using MurkYamlStore.Core;

namespace MurkYamlStore.Util;

public static partial class FileHelpers
{
    [GeneratedRegex(@"^(.*)\s\((\d+)\)$", RegexOptions.Compiled | RegexOptions.ECMAScript | RegexOptions.CultureInvariant)]
    private static partial Regex NumberedFileGenRegex();
    private static readonly Regex NumberedFileRegex = NumberedFileGenRegex();
    
    public static bool IsFileSizeValid(long fileSize)
    {
        return fileSize <= ServerConfig.Instance.UploadSizeLimit;
    }

    public static IActionResult ReturnDefaultImage()
    {
        return new VirtualFileResult("/assets/img/murk_default.png", "image/png");
    }

    public static string GetYamlDirectory(Guid archiId)
    {
        string yamlDir = Path.Combine(GetArchiDirectory(archiId), "yaml");
        return GetOrCreateDirectory(yamlDir);
    }
    
    public static string GetArchiDirectory(Guid archiId)
    {
        return GetOrCreateDirectory($"user-data/archis/{archiId}");
    }

    public static string GetGameDirectory(Guid gameId, Guid archiId)
    {
        string archiDir = GetArchiDirectory(archiId);
        return GetOrCreateDirectory(Path.Combine(archiDir, $"game/{gameId}"));
    }
    
    public static string MakeUniqueFilename(string filePath) {
        if( !File.Exists( filePath ) && !Directory.Exists( filePath ) ) return filePath;

        string directory = Path.GetDirectoryName( filePath );
        string stem = Path.GetFileNameWithoutExtension( filePath );
        string extension = Path.GetExtension( filePath );

        ulong number = 1;
        Match match = NumberedFileRegex.Match( stem );
        if( match.Success && match.Groups.Count == 3 && ulong.TryParse( match.Groups[2].Value, out number ) ) {
            stem = match.Groups[1].Value;
        }

        do {
            filePath = Path.Combine( directory, $"{stem} ({++number}){extension}" );
        } while( File.Exists( filePath ) || Directory.Exists( filePath ) );

        return filePath;
    }
    
    public static string GuessMimeType(string filename) {
        return Path.GetExtension(filename).ToLowerInvariant() switch {
            ".zip" => "application/zip",
            ".apworld" => "application/x-apworld",
            ".yaml" => "application/yaml",
            ".son" or ".j.son" or ".json" => "application/json",
            ".png" => "image/png",
            ".bmp" => "image/bmp",
            ".gif" => "image/gif",
            ".peg" or ".j.peg" or ".jpg" or ".jpeg" => "image/jpeg",
            _ => "application/octet-stream",
        };
    }
    
    private static string GetOrCreateDirectory(string path)
    {
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        return path;
    }
}