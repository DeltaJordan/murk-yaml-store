using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace MurkYamlStore.Util
{

    /// <summary>
    /// An alternative to Lazy<> that doesn't cache exceptions
    /// </summary>
    public sealed class SafeLazy<T>
    {

        private const int STATE_UNINITIALIZED = 0;
        private const int STATE_UPDATING = 1;
        private const int STATE_INITIALIZED = 2;

        private readonly Func<T> m_factory;
        private readonly LazyThreadSafetyMode m_mode;
        private volatile int m_state;
        private T m_value;

        public SafeLazy(
            Func<T> initializer,
            LazyThreadSafetyMode threadSafetyMode
        )
        {
            this.m_factory = initializer;
            this.m_mode = threadSafetyMode;
            this.m_state = STATE_UNINITIALIZED;
            this.m_value = default;
        }

        public SafeLazy(T value)
        {
            this.m_factory = null;
            this.m_mode = LazyThreadSafetyMode.None;
            this.m_state = STATE_INITIALIZED;
            this.m_value = value;
        }

        public T Value
        {
            get
            {
                if (this.m_state == STATE_INITIALIZED)
                {
                    Interlocked.MemoryBarrier();
                    return this.m_value;
                }

                switch (this.m_mode)
                {
                    case LazyThreadSafetyMode.None: this.InitUnsafe(); break;
                    case LazyThreadSafetyMode.PublicationOnly: this.InitRace(); break;
                    case LazyThreadSafetyMode.ExecutionAndPublication: this.InitAtomic(); break;
                    default: throw new InvalidEnumArgumentException();
                }

                return this.m_value;
            }
        }

        private void InitUnsafe()
        {
            this.m_value = this.m_factory();
            this.m_state = STATE_INITIALIZED;
        }

        private void InitRace()
        {
            T myValue = this.m_factory();
            int previousState = Interlocked.CompareExchange(
                ref this.m_state,
                value: STATE_UPDATING,
                comparand: STATE_UNINITIALIZED
            );

            if (previousState == STATE_UPDATING)
            {
                while (this.m_state == STATE_UPDATING)
                {
                    Thread.Yield();
                }
            }

            if (previousState == STATE_UNINITIALIZED)
            {
                this.m_value = myValue;
                this.m_state = STATE_INITIALIZED;
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private void InitAtomic()
        {
            if (this.m_state == STATE_UNINITIALIZED)
            {
                this.m_value = this.m_factory();
                this.m_state = STATE_INITIALIZED;
            }
        }


    }

}
