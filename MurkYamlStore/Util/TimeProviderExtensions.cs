using MurkYamlStore.Tools;

namespace MurkYamlStore.Util;

public static class BrowserTimeProviderExtensions
{
    public static DateTime ToLocalDateTime(this IBrowserTimeProvider timeProvider, DateTime dateTime, bool ignoreKind = false)
    {
        if (timeProvider.LocalTimeZone == null)
        {
            return dateTime;
        }

        return dateTime.Kind switch
        {
            DateTimeKind.Unspecified => ignoreKind
                ? DateTime.SpecifyKind(
                    TimeZoneInfo.ConvertTimeFromUtc(dateTime, timeProvider.LocalTimeZone),
                    DateTimeKind.Local
                )
                : dateTime,
            DateTimeKind.Local => dateTime,
            _ => DateTime.SpecifyKind(
                TimeZoneInfo.ConvertTimeFromUtc(dateTime, timeProvider.LocalTimeZone),
                DateTimeKind.Local
            )
        };
    }
    
    public static DateTime ToLocalDateTime(this IBrowserTimeProvider timeProvider, DateTimeOffset dateTime)
    {
        DateTime local = TimeZoneInfo.ConvertTimeFromUtc(dateTime.UtcDateTime, timeProvider.LocalTimeZone);
        local = DateTime.SpecifyKind(local, DateTimeKind.Local);
        return local;
    }
}