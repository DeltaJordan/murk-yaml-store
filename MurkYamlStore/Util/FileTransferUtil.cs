using Microsoft.AspNetCore.Components.Forms;

namespace MurkYamlStore.Util;

public static class FileTransferUtil
{
    /// <summary>
    /// Reads and stores <see cref="IBrowserFile"/> to disk.
    /// </summary>
    /// <param name="filePath">Path to save the file to.</param>
    /// <param name="file">File uploaded by user.</param>
    /// <param name="maxSize">Max size in bytes. Uses <see cref="ServerConfig"/> size limit if null.</param>
    public static async Task UploadAndSaveAsync(string filePath, IBrowserFile file, long? maxSize = null)
    {
        await using FileStream fs = new(filePath, FileMode.Create);
        await file.OpenReadStream(maxSize ?? ServerConfig.Instance.UploadSizeLimit).CopyToAsync(fs).SafeAsync();
    }
}