using MurkYamlStore.HttpException;

namespace MurkYamlStore.Util;

public static class StringLock
{
    private static readonly Dictionary<string, IDisposable> tickets = [];
    private static readonly object globalLock = new();

    private sealed class Ticket(string id) : IDisposable
    {

        private readonly SemaphoreSlim @lock = new(1);
        private readonly string id = id;
        private int refCount = 1;

        public SemaphoreSlim Lock { get => this.@lock; }

        public void Ref()
        {
            this.refCount++;
        }

        void IDisposable.Dispose()
        {
            lock (globalLock)
            {
                this.refCount--;
                if (this.refCount <= 0)
                {
                    tickets.Remove(this.id);
                }
            }

            this.@lock.Release();
        }

    }

    public static async Task<IDisposable> AcquireAsync(string id)
    {
        Ticket ticket = null;
        lock (globalLock)
        {
            if (tickets.TryGetValue(id, out IDisposable _ticket))
            {
                ticket = _ticket as Ticket;
                ticket.Ref();
            }
            else
            {
                ticket = new Ticket(id);
                tickets.Add(id, ticket);
            }
        }

        if (!await ticket.Lock.WaitAsync(millisecondsTimeout: 15000).SafeAsync())
        {
            throw new DeadlockException(id);
        }

        return ticket;
    }
}
