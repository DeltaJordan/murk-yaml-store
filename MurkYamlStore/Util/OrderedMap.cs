using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace MurkYamlStore.Util;

public sealed class OrderedMap<TKey, TValue> : IReadOnlyOrderedMap<TKey, TValue>
{

	private readonly OrderedDictionary inner;

	public OrderedMap()
	{
		this.inner = [];
	}

	public OrderedMap(IEnumerable<KeyValuePair<TKey, TValue>> values)
	{
		this.inner = [];
		foreach (KeyValuePair<TKey, TValue> kvp in values)
		{
			this.inner.Add(kvp.Key, kvp.Value);
		}
	}

	TValue IReadOnlyDictionary<TKey, TValue>.this[TKey key] => (TValue)this.inner[key];
	IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys => this.inner.Keys.Cast<TKey>();
	IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values => this.inner.Values.Cast<TValue>();
	int IReadOnlyCollection<KeyValuePair<TKey, TValue>>.Count => this.inner.Count;

	bool IReadOnlyDictionary<TKey, TValue>.ContainsKey(TKey key)
	{
		return this.inner.Contains(key);
	}

	bool IReadOnlyDictionary<TKey, TValue>.TryGetValue(TKey key, out TValue value)
	{
		if (this.inner.Contains(key))
		{
			value = (TValue)this.inner[key];
			return true;
		}

		value = default;
		return false;
	}

	IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
	{
		return this.inner.Cast<DictionaryEntry>().Select(
			kvp => new KeyValuePair<TKey, TValue>((TKey)kvp.Key, (TValue)kvp.Value)
		).GetEnumerator();
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		IEnumerable<KeyValuePair<TKey, TValue>> @this = this;
		return @this.GetEnumerator();
	}

	TValue IReadOnlyOrderedMap<TKey, TValue>.ValueAt(int index)
	{
		return (TValue)this.inner[index];
	}

	public void Add(TKey key, TValue value)
	{
		this.inner.Add(key, value);
	}

	public void Remove(TKey key)
	{
		this.inner.Remove(key);
	}

}
