using MurkYamlStore.Core;
using MurkYamlStore.Enums;

namespace MurkYamlStore.Util;

public static class UserContextHelper
{
    public static bool IsAtLeast(this TryResult<IUserContext> userContext, Role role)
    {
        return userContext.HasValue && userContext.Value.Role == role;
    }
}