using System.Security.Claims;
using MurkYamlStore.Core;

namespace MurkYamlStore.Util;

public static class DiscordUtil
{
    public const string DISCORD_CDN = "https://cdn.discordapp.com";

    public const string AVATAR_CLAIM = "urn:discord:avatar:hash";
    public const string BANNER_CLAIM = "urn:discord:banner:hash";
    public const string ID_CLAIM = ClaimTypes.NameIdentifier;
    public const string DISPLAY_NAME_CLAIM = ClaimTypes.GivenName;

    public static string GetAvatarHash(this ClaimsPrincipal claimsPrincipal)
    {
        return claimsPrincipal?.FindFirstValue(AVATAR_CLAIM);
    }

    public static string GetBannerHash(this ClaimsPrincipal claimsPrincipal)
    {
        return claimsPrincipal?.FindFirstValue(BANNER_CLAIM);
    }

    public static ulong? GetDiscordId(this ClaimsPrincipal claimsPrincipal)
    {
        string discordIdStr = claimsPrincipal?.FindFirstValue(ID_CLAIM);
        if (ulong.TryParse(discordIdStr, out ulong parsedId))
        {
            return parsedId;
        }

        return null;
    }

    /// <summary>
    /// Gets discord display name, unless null then returns username.
    /// </summary>
    /// <param name="claimsPrincipal"></param>
    /// <returns></returns>
    public static string GetDisplayName(this ClaimsPrincipal claimsPrincipal)
    {
        string displayName = claimsPrincipal?.FindFirstValue(DISPLAY_NAME_CLAIM);

        return displayName ?? claimsPrincipal?.Identity?.Name;
    }

    public static string GetUsername(this ClaimsPrincipal claimsPrincipal)
    {
        return claimsPrincipal?.Identity?.Name;
    }

    public static string GetDisplayName(this DiscordRef discord)
    {
        string displayName = discord.DisplayName;
        return displayName ?? discord.Username;
    }

    public static string GetMention(this DiscordRef discord)
    {
        return $"<@{discord.UserId}>";
    }
}