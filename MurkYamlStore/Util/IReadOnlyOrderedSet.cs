using System.Collections.Generic;

namespace MurkYamlStore.Util;

public interface IReadOnlyOrderedSet<T> : IReadOnlySet<T>
{
	T ItemAt(int index);
}
