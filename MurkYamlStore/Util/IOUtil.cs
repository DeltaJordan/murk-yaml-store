namespace MurkYamlStore.Util;

public static class IOUtil
{
    public static string AppDirectory { get; } = AppDomain.CurrentDomain.BaseDirectory;
}