using System.Collections.Generic;

namespace MurkYamlStore.Util;

public interface IReadOnlyOrderedMap<TKey, TValue> : IReadOnlyDictionary<TKey, TValue>
{
	TValue ValueAt(int index);
}
