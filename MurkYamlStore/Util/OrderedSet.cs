using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Newtonsoft.Json;

namespace MurkYamlStore.Util;

[JsonConverter(typeof(OrderedSetJsonConverter))]
public sealed class OrderedSet<T> : ICollection<T>, IReadOnlyOrderedSet<T> where T : IEquatable<T>
{

	private sealed class Node(T value, Node prev = null, Node next = null)
	{
		public T Value { get; } = value;
		public Node Prev { get; set; } = prev;
		public Node Next { get; set; } = next;
	}

	private sealed class Iterator : IEnumerator<T>
	{

		private readonly Node start;
		private Node current;

		public Iterator(Node node)
		{
			this.start = new Node(default, null, node);
			this.current = this.start;
		}

		T IEnumerator<T>.Current => this.current.Value;
		object IEnumerator.Current => this.current.Value;
		void IDisposable.Dispose() { }

		bool IEnumerator.MoveNext()
		{
			this.current = this.current?.Next;
			return this.current != null;
		}

		void IEnumerator.Reset()
		{
			this.current = this.start;
		}

	}

	private readonly Dictionary<T, Node> m_nodes;
	private Node m_first;
	private Node m_last;

	public OrderedSet()
	{
		this.m_nodes = [];
		this.m_first = null;
		this.m_last = null;
	}

	public OrderedSet(IEnumerable values) : this()
	{
		foreach (object value in values) this.Add((T)value);
	}

	public int Count => this.m_nodes.Count;
	public bool IsReadOnly => false;

	int IReadOnlyCollection<T>.Count => this.m_nodes.Count;

	public void Add(T item)
	{
		if (this.m_nodes.ContainsKey(item)) return;
		Node node = new(item, this.m_last, null);
		this.m_nodes.Add(item, node);
		if (this.m_last != null) this.m_last.Next = node;
		this.m_last = node;
		this.m_first ??= node;
	}

	public void Clear()
	{
		this.m_nodes.Clear();
		this.m_first = null;
		this.m_last = null;
	}

	public bool Contains(T item)
	{
		return this.m_nodes.ContainsKey(item);
	}

	public void CopyTo(T[] array, int arrayIndex)
	{
		for (Node node = this.m_first; node != null; node = node.Next)
		{
			array[arrayIndex++] = node.Value;
		}
	}

	public IEnumerator<T> GetEnumerator()
	{
		return new Iterator(this.m_first);
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return new Iterator(this.m_first);
	}

	public bool Remove(T item)
	{
		if (!this.m_nodes.ContainsKey(item)) return false;
		Node node = this.m_nodes[item];
		this.m_nodes.Remove(item);

		if (node.Prev == null)
		{
			this.m_first = node.Next;
		}
		else
		{
			node.Prev.Next = node.Next;
		}

		if (node.Next == null)
		{
			this.m_last = node.Prev;
		}
		else
		{
			node.Next.Prev = node.Prev;
		}

		return true;
	}

	public T Shift()
	{
		if (this.m_first == null) return default;
		T value = this.m_first.Value;
		this.m_nodes.Remove(value);
		if (this.m_first.Next != null)
		{
			this.m_first.Next.Prev = null;
			this.m_first = this.m_first.Next;
		}
		return value;
	}

	public T Pop()
	{
		if (this.m_last == null) return default;
		T value = this.m_last.Value;
		this.m_nodes.Remove(value);
		if (this.m_last.Prev != null)
		{
			this.m_last.Prev.Next = null;
			this.m_last = this.m_last.Prev;
		}
		return value;
	}

	public T ItemAt(int index)
	{
		if (index >= 0)
		{
			Node n = this.m_first;
			for (int i = 0; i < index; i++) n = n.Next;
			return n.Value;
		}
		else
		{
			Node n = this.m_last;
			for (int i = -1; i > index; i--) n = n.Prev;
			return n.Value;
		}
	}

	bool IReadOnlySet<T>.Contains(T item)
	{
		return this.m_nodes.ContainsKey(item);
	}

	bool IReadOnlySet<T>.IsProperSubsetOf(IEnumerable<T> other)
	{
		if (other is ISet<T> otherSet)
		{
			return otherSet.IsProperSupersetOf(this);
		}
		else if (other is IReadOnlySet<T> roSet)
		{
			return roSet.IsProperSupersetOf(this);
		}
		return other.ToHashSet().IsProperSupersetOf(this);
	}

	bool IReadOnlySet<T>.IsProperSupersetOf(IEnumerable<T> other)
	{
		int otherCount = 0;
		foreach (T element in other.Distinct())
		{
			otherCount++;
			if (!this.m_nodes.ContainsKey(element)) return false;
		}
		return this.m_nodes.Count > otherCount;
	}

	bool IReadOnlySet<T>.IsSubsetOf(IEnumerable<T> other)
	{
		if (other is ISet<T> otherSet)
		{
			return otherSet.IsSupersetOf(this);
		}
		else if (other is IReadOnlySet<T> roSet)
		{
			return roSet.IsSupersetOf(this);
		}
		return other.ToHashSet().IsSupersetOf(this);
	}

	bool IReadOnlySet<T>.IsSupersetOf(IEnumerable<T> other)
	{
		foreach (T element in other)
		{
			if (!this.m_nodes.ContainsKey(element)) return false;
		}
		return true;
	}

	bool IReadOnlySet<T>.Overlaps(IEnumerable<T> other)
	{
		foreach (T element in other)
		{
			if (this.m_nodes.ContainsKey(element)) return true;
		}
		return false;
	}

	bool IReadOnlySet<T>.SetEquals(IEnumerable<T> other)
	{
		HashSet<T> seenValues = [];
		int otherCount = 0;
		foreach (T element in other)
		{
			if (
				seenValues.Contains(element) ||
				!this.m_nodes.ContainsKey(element) ||
				otherCount++ > this.m_nodes.Count
			) return false;
			seenValues.Add(element);
		}
		return true;
	}

	IEnumerator<T> IEnumerable<T>.GetEnumerator()
	{
		return new Iterator(this.m_first);
	}

}

[EditorBrowsable(EditorBrowsableState.Never)]
internal sealed class OrderedSetJsonConverter : JsonConverter
{

	public override bool CanConvert(Type objectType)
	{
		return objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(OrderedSet<>);
	}

	public override object ReadJson(
		JsonReader reader,
		Type objectType,
		object existingValue,
		JsonSerializer serializer
	)
	{
		if (reader.TokenType == JsonToken.Null) return null;

		Type elementType = objectType.GenericTypeArguments[0];
		return Activator.CreateInstance(
			type: objectType,
			args: ReadEnumerable(serializer, reader, elementType)
		);
	}

	public override void WriteJson(
		JsonWriter writer,
		object value,
		JsonSerializer serializer
	)
	{
		if (value == null)
		{
			writer.WriteNull();
			return;
		}

		writer.WriteStartArray();
		foreach (object element in value as IEnumerable)
		{
			serializer.Serialize(writer, element);
		}
		writer.WriteEndArray();
	}

	private static IEnumerable ReadEnumerable(
		JsonSerializer serializer,
		JsonReader reader,
		Type elementType
	)
	{
		if (reader.TokenType != JsonToken.StartArray) throw new JsonSerializationException();
		while (reader.Read() && reader.TokenType != JsonToken.EndArray)
		{
			yield return serializer.Deserialize(reader, elementType);
		}
		if (reader.TokenType != JsonToken.EndArray) throw new JsonSerializationException();
	}

}
