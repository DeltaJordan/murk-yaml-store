using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace MurkYamlStore.Util;

public sealed class HeaderActionResult(IActionResult innerResult, IDictionary<string, StringValues> headers = null) : IActionResult
{

	private readonly IActionResult innerResult = innerResult;
	private readonly IDictionary<string, StringValues> headers = headers ?? new Dictionary<string, StringValues>();

	Task IActionResult.ExecuteResultAsync(ActionContext context)
	{
		foreach (KeyValuePair<string, StringValues> header in this.headers)
		{
			context.HttpContext.Response.Headers.Append(header.Key, header.Value);
		}
		return this.innerResult.ExecuteResultAsync(context);
	}

	public void AddHeader(string name, StringValues value)
	{
		this.headers.Add(name, value);
	}

}
