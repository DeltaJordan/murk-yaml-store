using System.Text;
using MurkYamlStore.Core;
using MurkYamlStore.HttpException;

namespace MurkYamlStore.Pagination;

public sealed class BookmarkToken
{
	private const byte TYPE_NULL = 0;
	private const byte TYPE_INT = 1;
	private const byte TYPE_LONG = 2;
	private const byte TYPE_FLOAT = 3;
	private const byte TYPE_DATE = 4;
	private const byte TYPE_GUID = 5;
	private const byte TYPE_STRING = 6;
	private const byte TYPE_DID = 7;

	private readonly List<object> parts;
	private int readerPosition;

	public BookmarkToken(string token = null)
	{
		this.parts = [];
		this.readerPosition = 0;

		if (!string.IsNullOrEmpty(token))
		{
			byte[] data = SafeBase64Decode(token);
			for (int i = 0; i < data.Length;)
			{
				byte type = data[i++];
				switch (type)
				{
					case TYPE_NULL:
						this.parts.Add(null);
						break;
					case TYPE_INT:
						if (i + 4 > data.Length) throw new BadRequestException("Invalid bookmark token");
						this.parts.Add(BitConverter.ToInt32(data.AsSpan(i, 4)));
						i += 4;
						break;
					case TYPE_LONG:
						if (i + 8 > data.Length) throw new BadRequestException("Invalid bookmark token");
						this.parts.Add(BitConverter.ToInt64(data.AsSpan(i, 8)));
						i += 8;
						break;
					case TYPE_FLOAT:
						if (i + 4 > data.Length) throw new BadRequestException("Invalid bookmark token");
						this.parts.Add(BitConverter.ToSingle(data.AsSpan(i, 4)));
						i += 4;
						break;
					case TYPE_DATE:
						if (i + 8 > data.Length) throw new BadRequestException("Invalid bookmark token");
						long ticks = BitConverter.ToInt64(data.AsSpan(i, 8));
						this.parts.Add(new DateTime(ticks, DateTimeKind.Utc));
						i += 8;
						break;
					case TYPE_GUID:
						if (i + 16 > data.Length) throw new BadRequestException("Invalid bookmark token");
						this.parts.Add(new Guid(data.AsSpan(i, 16)));
						i += 16;
						break;
					case TYPE_STRING:
						{
							int s = i;
							while (!data[i++].Equals(0))
							{
								if (i >= data.Length) throw new BadRequestException("Invalid bookmark token");
							}
							this.parts.Add(Encoding.UTF8.GetString(data.AsSpan(s, i - s - 1)));
							break;
						}
					case TYPE_DID:
						if (i + 8 > data.Length) throw new BadRequestException("Invalid bookmark token");
						this.parts.Add(new DiscordId(BitConverter.ToUInt64(data.AsSpan(i, 8))));
						i += 8;
						break;
					default:
						throw new BadRequestException("Invalid bookmark token");
				}
			}
		}
	}

	public bool HasValue => this.parts.Count > 0;

	public void ResetReader()
	{
		this.readerPosition = 0;
	}

	public void Clear()
	{
		this.readerPosition = 0;
		this.parts.Clear();
	}

	public void PushValue(int? value)
	{
		if (!value.HasValue) this.parts.Add(null);
		else this.parts.Add(value.Value);
	}

	public void PushValue(long? value)
	{
		if (!value.HasValue) this.parts.Add(null);
		else this.parts.Add(value.Value);
	}

	public void PushValue(float? value)
	{
		if (!value.HasValue) this.parts.Add(null);
		else this.parts.Add(value.Value);
	}

	public void PushValue(DateTime? value)
	{
		if (!value.HasValue) this.parts.Add(null);
		else this.parts.Add(value.Value);
	}

	public void PushValue(Guid? value)
	{
		if (!value.HasValue) this.parts.Add(null);
		else this.parts.Add(value.Value);
	}

	public void PushValue(DiscordId? value)
	{
		if (!value.HasValue) this.parts.Add(null);
		else this.parts.Add(value.Value);
	}

	public void PushValue(string value)
	{
		this.parts.Add(value);
	}

	public T ReadNext<T>()
	{
		try
		{
			if (typeof(T) == typeof(ulong) || typeof(T) == typeof(DiscordId))
			{
				return (T)((ulong)this.parts[this.readerPosition++] as object);
			}
			else if (typeof(T) == typeof(ulong?) || typeof(T) == typeof(DiscordId?))
			{
				return (T)((ulong?)this.parts[this.readerPosition++] as object);
			}
			else if (typeof(T) == typeof(string))
			{
				return (T)(object)(string)this.parts[this.readerPosition++];
			}
			else
			{
				return (T)this.parts[this.readerPosition++];
			}
		}
		catch (Exception ex)
		{
			throw new BadRequestException("Invalid bookmark token", innerException: ex);
		}
	}

	public string Encode()
	{
		if (this.parts.Count == 0) return null;

		IEnumerable<byte> token = Enumerable.Empty<byte>();
		foreach (object part in this.parts)
		{
			if (part == null)
			{
				token = token.Append(TYPE_NULL);
				continue;
			}

			Type type = part.GetType();
			if (type.Equals(typeof(int)))
			{
				token = token.Append(TYPE_INT).Concat(BitConverter.GetBytes((int)part));
			}
			else if (type.Equals(typeof(long)))
			{
				token = token.Append(TYPE_LONG).Concat(BitConverter.GetBytes((long)part));
			}
			else if (type.Equals(typeof(float)))
			{
				token = token.Append(TYPE_FLOAT).Concat(BitConverter.GetBytes((float)part));
			}
			else if (type.Equals(typeof(DateTime)))
			{
				token = token.Append(TYPE_DATE).Concat(BitConverter.GetBytes(((DateTime)part).Ticks));
			}
			else if (type.Equals(typeof(Guid)))
			{
				token = token.Append(TYPE_GUID).Concat(((Guid)part).ToByteArray());
			}
			else if (type.Equals(typeof(string)))
			{
				token = token.Append(TYPE_STRING).Concat(Encoding.UTF8.GetBytes((string)part)).Append((byte)0);
			}
			else if (type.Equals(typeof(ulong)) || type.Equals(typeof(DiscordId)))
			{
				token = token.Append(TYPE_DID).Concat(BitConverter.GetBytes((ulong)part));
			}
			else
			{
				throw new InvalidCastException("Unexpected type in bookmark token");
			}
		}

		return SafeBase64Encode(token.ToArray());
	}

	private static string SafeBase64Encode(byte[] data)
	{
		return Convert.ToBase64String(data)
			.Replace('+', '-')
			.Replace('/', '.')
			.TrimEnd('=');
	}

	private static byte[] SafeBase64Decode(string data)
	{
		while (data.Length % 4 != 0) data += '=';
		try
		{
			return Convert.FromBase64String(data.Replace('.', '/').Replace('-', '+'));
		}
		catch (Exception ex)
		{
			throw new BadRequestException("Invalid bookmark token", innerException: ex);
		}
	}

	public override string ToString()
	{
		return this.Encode();
	}

}
