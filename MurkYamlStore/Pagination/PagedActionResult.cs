using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace MurkYamlStore.Pagination;

internal sealed class PagedActionResult<T> : IActionResult
{

	private readonly ICollection<T> results;
	private readonly BookmarkToken bookmark;

	public PagedActionResult(
		IEnumerable<T> results,
		BookmarkToken bookmark
	)
	{
		this.results = results.CoerceToICollection();
		this.bookmark = bookmark;
	}

	Task IActionResult.ExecuteResultAsync(ActionContext context)
	{
		string bookmarkString = this.bookmark?.Encode();
		string nextPage = null;

		if (bookmarkString != null)
		{
			PathString requestPath = context.HttpContext.Request.Path;
			IQueryCollection originalQueryParams = context.HttpContext.Request.Query;

			QueryString queryString = QueryString.Empty;
			foreach (KeyValuePair<string, StringValues> q in originalQueryParams)
			{
				if (q.Key == "bookmark") continue;
				if (q.Key == "getCount") continue;
				queryString = queryString.Add(q.Key, q.Value.ToString());
			}
			queryString = queryString.Add("bookmark", bookmarkString);

			nextPage = requestPath + queryString.Value;
			context.HttpContext.Response.Headers.Append("Link", $"{nextPage}; rel=\"next\"");
		}

		return new ObjectResult(new
		{
			bookmark = bookmarkString,
			next = nextPage,
			this.results
		}).ExecuteResultAsync(context);

	}

}
