namespace MurkYamlStore.Pagination;

internal static class SecuredPageHelper
{

	private const int MIN_PAGE_SIZE = 10;
	private const int MAX_PAGE_SIZE = 1024;

	public static async Task<IReadOnlyList<T>> FetchPageAsync<T>(
		Func<int, BookmarkToken, Task<IReadOnlyList<T>>> pageFetcher,
		Func<IReadOnlyList<T>, Task<IReadOnlyList<T>>> filter,
		Action<BookmarkToken, T> bookmarkUpdater,
		int pageSize,
		BookmarkToken bookmark
	)
	{
		IEnumerable<T> finalResults = [];
		int finalResultCount = 0;
		int requestedPageSize = pageSize;

		int totalUnfilteredHitCount = 0;
		int totalFilteredHitCount = 0;

		if (pageSize < MIN_PAGE_SIZE)
		{
			pageSize = MIN_PAGE_SIZE;
		}

		while (true)
		{
			IReadOnlyList<T> pageResults = await pageFetcher(pageSize, bookmark).SafeAsync();
			IReadOnlyList<T> filteredResults = await filter(pageResults).SafeAsync();

			totalUnfilteredHitCount += pageResults.Count;
			totalFilteredHitCount += filteredResults.Count;

			finalResults = finalResults.Concat(filteredResults);
			finalResultCount += filteredResults.Count;

			if (pageResults.Count < pageSize)
			{
				if (finalResultCount > requestedPageSize)
				{
					IReadOnlyList<T> returnValue = finalResults.Take(requestedPageSize).ToList();
					bookmarkUpdater(bookmark, returnValue[requestedPageSize - 1]);
					return returnValue;
				}
				else
				{
					bookmark.Clear();
					return finalResults.ToList();
				}
			}

			if (finalResultCount == requestedPageSize)
			{
				bookmarkUpdater(bookmark, filteredResults[filteredResults.Count - 1]);
				return finalResults.ToList();
			}
			else if (finalResultCount > requestedPageSize)
			{
				IReadOnlyList<T> returnValue = finalResults.Take(requestedPageSize).ToList();
				bookmarkUpdater(bookmark, returnValue[requestedPageSize - 1]);
				return returnValue;
			}

			double density = totalFilteredHitCount / (double)totalUnfilteredHitCount;
			int maxPageSize = Math.Min(Math.Max(requestedPageSize, pageSize) * 2, MAX_PAGE_SIZE);
			if (density == 0.0)
			{
				pageSize = maxPageSize;
			}
			else
			{
				pageSize = Math.Max(MIN_PAGE_SIZE, (int)Math.Min(maxPageSize, (requestedPageSize - finalResultCount) / density));
			}

		}
	}

	public static Task<IReadOnlyList<T>> FetchPageAsync<T>(
		Func<int, BookmarkToken, Task<IReadOnlyList<T>>> pageFetcher,
		Func<IReadOnlyList<T>, IReadOnlyList<T>> filter,
		Action<BookmarkToken, T> bookmarkUpdater,
		int pageSize,
		BookmarkToken bookmark
	)
	{
		return FetchPageAsync(
			pageFetcher,
			(results) => Task.FromResult(filter(results)),
			bookmarkUpdater,
			pageSize,
			bookmark
		);
	}

	public static Task<IReadOnlyList<T>> FetchPageAsync<T>(
		Func<int, BookmarkToken, Task<IReadOnlyList<T>>> pageFetcher,
		Func<T, bool> filter,
		Action<BookmarkToken, T> bookmarkUpdater,
		int pageSize,
		BookmarkToken bookmark
	)
	{
		return FetchPageAsync(
			pageFetcher,
			(results) => results.Where(filter).CoerceToIReadOnlyList(),
			bookmarkUpdater,
			pageSize,
			bookmark
		);
	}

}
