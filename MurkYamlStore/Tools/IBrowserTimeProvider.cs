using Microsoft.VisualStudio.Threading;

namespace MurkYamlStore.Tools;

public interface IBrowserTimeProvider
{
    event AsyncEventHandler LocalTimeZoneChanged;
    TimeZoneInfo LocalTimeZone { get; }
    Task SetBrowserTimeZoneAsync(string timeZone);
}