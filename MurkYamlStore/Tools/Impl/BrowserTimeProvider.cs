using Microsoft.VisualStudio.Threading;

namespace MurkYamlStore.Tools.Impl;

public sealed class BrowserTimeProvider : IBrowserTimeProvider
{
    public event AsyncEventHandler LocalTimeZoneChanged;

    public TimeZoneInfo LocalTimeZone { get; private set; }

    public bool IsLocalTimeZoneSet => this.LocalTimeZone != null;

    public async Task SetBrowserTimeZoneAsync(string timeZone)
    {
        if (!TimeZoneInfo.TryFindSystemTimeZoneById(timeZone, out TimeZoneInfo timeZoneInfo))
        {
            timeZoneInfo = null;
        }

        // NOTE: This sets browserLocalTimeZone to null if timeZoneInfo is null.
        if (timeZoneInfo?.Equals(this.LocalTimeZone) != true)
        {
            this.LocalTimeZone = timeZoneInfo;

            if (this.LocalTimeZoneChanged != null)
            {
                await this.LocalTimeZoneChanged.InvokeAsync(this, EventArgs.Empty).SafeAsync();
            }
        }
    }
}
