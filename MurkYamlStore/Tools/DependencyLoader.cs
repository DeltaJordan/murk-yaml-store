using MurkYamlStore.Tools.Impl;

namespace MurkYamlStore.Tools;

internal static class DependencyLoader
{
    public static void Load(IServiceCollection registry)
    {
        registry.AddScoped<IBrowserTimeProvider, BrowserTimeProvider>();
    }
}