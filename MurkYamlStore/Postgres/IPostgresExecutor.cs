using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Npgsql;
using MurkYamlStore.Core;

namespace MurkYamlStore.Postgres;


/// <summary>
/// A base interface for methods common to <see cref="IPostgresDatabase"/>
/// and <see cref="IPostgresTransaction"/>. See those interfaces for more
/// information.
/// </summary>
public interface IPostgresExecutor
{

	/// <summary>
	/// Execute a SQL command, but do not read the results.
	/// </summary>
	/// <param name="command">The SQL command to execute.</param>
	/// <returns>The number of rows affected by the query.</returns>
	/// <exception cref="PostgresException">
	/// The SQL command raises an error. This exception is thrown when an
	/// error is reported by the PostgreSQL backend. Other errors such as
	/// network issues result in an <see cref="NpgsqlException"/> instead,
	/// which is a base class of this exception.
	/// </exception>
	/// <exception cref="NpgsqlException">
	/// This exception is thrown when server-related issues occur.
	/// PostgreSQL specific errors raise a <see cref="PostgresException"/>,
	/// which is a subclass of this exception.
	/// </exception>
	Task<int> ExecNonQueryAsync(PostgresCommand command);

	/// <summary>
	/// Execute a SQL command and return the first column of the first row
	/// of the result set, or an empty TryResult if no data is returned.
	/// </summary>
	/// <param name="command">The SQL command to execute.</param>
	/// <returns>
	/// The first column of the first row of the result set.
	/// </returns>
	/// <exception cref="PostgresException">
	/// The SQL command raises an error. This exception is thrown when an
	/// error is reported by the PostgreSQL backend. Other errors such as
	/// network issues result in an <see cref="NpgsqlException"/> instead,
	/// which is a base class of this exception.
	/// </exception>
	/// <exception cref="NpgsqlException">
	/// This exception is thrown when server-related issues occur.
	/// PostgreSQL specific errors raise a <see cref="PostgresException"/>,
	/// which is a subclass of this exception.
	/// </exception>
	Task<TryResult<T>> ExecTryReadScalarAsync<T>(PostgresCommand command);

	/// <summary>
	/// Execute a SQL command and return the first record in the result set.
	/// </summary>
	/// <param name="command">The SQL command to execute.</param>
	/// <param name="dbConverter">A converter for the data record.</param>
	/// <returns>The first record in the result set.</returns>
	/// <exception cref="PostgresException">
	/// The SQL command raises an error. This exception is thrown when an
	/// error is reported by the PostgreSQL backend. Other errors such as
	/// network issues result in an <see cref="NpgsqlException"/> instead,
	/// which is a base class of this exception.
	/// </exception>
	/// <exception cref="NpgsqlException">
	/// This exception is thrown when server-related issues occur.
	/// PostgreSQL specific errors raise a <see cref="PostgresException"/>,
	/// which is a subclass of this exception.
	/// </exception>
	Task<TryResult<Dto>> ExecTryReadFirstAsync<Dto>(
		PostgresCommand command,
		Func<IDataRecord, Dto> dbConverter
	);

	/// <summary>
	/// Execute a SQL command and return the result set. The entire result
	/// set is loaded into an <see cref="IReadOnlyList{Dto}"/> before the
	/// task becomes completed.
	/// </summary>
	/// <param name="command">The SQL command to execute.</param>
	/// <param name="dbConverter">A converter for the data record.</param>
	/// <returns>The result set.</returns>
	/// <exception cref="PostgresException">
	/// The SQL command raises an error. This exception is thrown when an
	/// error is reported by the PostgreSQL backend. Other errors such as
	/// network issues result in an <see cref="NpgsqlException"/> instead,
	/// which is a base class of this exception.
	/// </exception>
	/// <exception cref="NpgsqlException">
	/// This exception is thrown when server-related issues occur.
	/// PostgreSQL specific errors raise a <see cref="PostgresException"/>,
	/// which is a subclass of this exception.
	/// </exception>
	Task<IReadOnlyList<Dto>> ExecReadManyAsync<Dto>(
		PostgresCommand command,
		Func<IDataRecord, Dto> dbConverter
	);

}
