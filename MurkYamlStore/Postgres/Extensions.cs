using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Threading.Tasks;
using MurkYamlStore.Core;
using MurkYamlStore.HttpException;
using MurkYamlStore.Postgres.TypeConverters;
using Newtonsoft.Json;
using Npgsql;

namespace MurkYamlStore.Postgres;

[EditorBrowsable(EditorBrowsableState.Never)]
public static class PostgresExtensionMethods
{

    /// <summary>
    /// Read a field from the database record in the column with the given
    /// name. Null values are automatically converted from
    /// <c>DBNull.Value</c> to <c>null</c>.
    /// </summary>
    /// <param name="columnName">The name of the column to read</param>
    /// <typeparam name="T">The C# data type of the field</typeparam>
    /// <returns>The value of the field</returns>
    public static T Get<T>(
        this IDataRecord record,
        string columnName
    )
    {
        int index = record.GetOrdinal(columnName);
        return DbTypeConverter<T>.FromDbValue(record.GetValue(index));
    }

    /// <summary>
    /// Read a json or jsonb field from the database record in the column
    /// with the given name.
    /// </summary>
    /// <param name="columnName">The name of the column to read</param>
    /// <typeparam name="T">The C# data type of the field</typeparam>
    /// <returns>The value of the field</returns>
    public static T GetJson<T>(
        this IDataRecord record,
        string columnName
    )
    {
        int index = record.GetOrdinal(columnName);
        object dbValue = record.GetValue(index);
        if (dbValue is DBNull) return (T)(object)null;
        return JsonConvert.DeserializeObject<T>(dbValue as string);
    }

    /// <summary>
    /// Execute a SQL command and return the first column of the result set.
    /// The entire column is loaded into an <see cref="IReadOnlyList{T}"/>
    /// before the task becomes completed. Intended to be used for queries
    /// whose result set contains exactly one column.
    /// </summary>
    /// <param name="command">The SQL command to execute.</param>
    /// <returns>The first column of the result set.</returns>
    /// <exception cref="PostgresException">
    /// The SQL command raises an error. This exception is thrown when an
    /// error is reported by the PostgreSQL backend. Other errors such as
    /// network issues result in an <see cref="NpgsqlException"/> instead,
    /// which is a base class of this exception.
    /// </exception>
    /// <exception cref="NpgsqlException">
    /// This exception is thrown when server-related issues occur.
    /// PostgreSQL specific errors raise a <see cref="PostgresException"/>,
    /// which is a subclass of this exception.
    /// </exception>
    public static Task<IReadOnlyList<T>> ExecReadColumnAsync<T>(
        this IPostgresExecutor database,
        PostgresCommand command
    )
    {
        return database.ExecReadManyAsync<T>(
            command,
            record => DbTypeConverter<T>.FromDbValue(record.GetValue(0))
        );
    }

    public static async Task<T> ExecReadScalarAsync<T>(
        this IPostgresExecutor database,
        PostgresCommand command
    )
    {
        TryResult<T> maybeResult = await database.ExecTryReadScalarAsync<T>(command).SafeAsync();
        if (!maybeResult.HasValue) throw new NotFoundException();
        return maybeResult.Value;
    }

    public static string EscapeLike(
        this IPostgresExecutor database,
        string pattern
    )
    {
        string escapedPattern = string.Empty;
        foreach (char c in pattern)
        {
            if (c == '\\' || c == '_' || c == '%') escapedPattern += '\\';
            escapedPattern += c;
        }
        return escapedPattern;
    }

    /// <summary>
    /// Gets the <see cref="PostgresErrorClass"/> of the current
    /// <see cref="PostgresException"/>.
    /// </summary>
    /// <returns>
    /// The <see cref="PostgresErrorClass"/> of the current
    /// <see cref="PostgresException"/>
    /// </returns>
    public static PostgresErrorClass GetErrorClass(
        this PostgresException exception
    )
    {
        return exception.SqlState.Substring(0, 2) switch
        {
            "00" => PostgresErrorClass.SuccessfulCompletion,
            "01" => PostgresErrorClass.Warning,
            "02" => PostgresErrorClass.NoData,
            "03" => PostgresErrorClass.SqlStatementNotYetComplete,
            "08" => PostgresErrorClass.ConnectionException,
            "09" => PostgresErrorClass.TriggeredActionException,
            "0A" => PostgresErrorClass.FeatureNotSupported,
            "0B" => PostgresErrorClass.InvalidTransactionInitiation,
            "0F" => PostgresErrorClass.LocatorException,
            "0L" => PostgresErrorClass.InvalidGrantor,
            "0P" => PostgresErrorClass.InvalidRoleSpecification,
            "0Z" => PostgresErrorClass.DiagnosticsException,
            "20" => PostgresErrorClass.CaseNotFound,
            "21" => PostgresErrorClass.CardinalityViolation,
            "22" => PostgresErrorClass.DataException,
            "23" => PostgresErrorClass.IntegrityConstraintViolation,
            "24" => PostgresErrorClass.InvalidCursorState,
            "25" => PostgresErrorClass.InvalidTransactionState,
            "26" => PostgresErrorClass.InvalidSqlStatementName,
            "27" => PostgresErrorClass.TriggeredDataChangeViolation,
            "28" => PostgresErrorClass.InvalidAuthorizationSpecification,
            "2B" => PostgresErrorClass.DependentPrivilegeDescriptorsStillExist,
            "2D" => PostgresErrorClass.InvalidTransactionTermination,
            "2F" => PostgresErrorClass.SqlRoutineException,
            "34" => PostgresErrorClass.InvalidCursorName,
            "38" => PostgresErrorClass.ExternalRoutineException,
            "39" => PostgresErrorClass.ExternalRoutineInvocationException,
            "3B" => PostgresErrorClass.SavepointException,
            "3D" => PostgresErrorClass.InvalidCatalogName,
            "3F" => PostgresErrorClass.InvalidSchemaName,
            "40" => PostgresErrorClass.TransactionRollback,
            "42" => PostgresErrorClass.SyntaxErrorOrAccessRuleViolation,
            "44" => PostgresErrorClass.WithCheckOptionViolation,
            "53" => PostgresErrorClass.InsufficientResources,
            "54" => PostgresErrorClass.ProgramLimitExceeded,
            "55" => PostgresErrorClass.ObjectNotInPrerequisiteState,
            "57" => PostgresErrorClass.OperatorIntervention,
            "58" => PostgresErrorClass.SystemError,
            "F0" => PostgresErrorClass.ConfigurationFileError,
            "HV" => PostgresErrorClass.ForeignDataWrapperError,
            "P0" => PostgresErrorClass.PlPgsqlError,
            "XX" => PostgresErrorClass.InternalError,
            _ => PostgresErrorClass.UnknownErrorClass,
        };
    }

}
