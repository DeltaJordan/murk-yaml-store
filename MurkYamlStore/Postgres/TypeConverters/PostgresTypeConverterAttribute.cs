using System;

namespace MurkYamlStore.Postgres.TypeConverters;

/// <summary>
/// Specifies the <see cref="IPostgresTypeConverter{T}" /> used to convert
/// the type to and from a database type.
/// </summary>
/// <seealso cref="IPostgresTypeConverter{T}"/>
/// <remarks>
/// Specifies the <see cref="IPostgresTypeConverter{T}" /> used to
/// convert the type to and from a database type.
/// </remarks>
/// <param name="converterType">
/// The type of the converter. The type must inherit from the
/// <see cref="IPostgresTypeConverter{T}"/> interface where <c>T</c> is
/// the type being converted.
/// </param>
[AttributeUsage(
	validOn: AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Class,
	AllowMultiple = false,
	Inherited = false
)]
public sealed class PostgresTypeConverterAttribute(Type converterType) : Attribute
{
	private readonly Type converterType = converterType;
	internal Type ConverterType => this.converterType;
}
