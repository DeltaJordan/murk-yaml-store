using System;
using System.Collections.Generic;
using NpgsqlTypes;

namespace MurkYamlStore.Postgres.TypeConverters.Impl;

internal sealed class ListTypeConverter<T> : IPostgresTypeConverter<List<T>>, IPostgresTypeConverter<IList<T>>
{

	internal readonly IPostgresTypeConverter<T> m_innerConverter;

	public ListTypeConverter(
		IPostgresTypeConverter<T> innerConverter
	)
	{
		this.m_innerConverter = innerConverter;
	}

	object IPostgresTypeConverter<IList<T>>.ToDbValue(IList<T> value)
	{
		return this.ToDbValueInternal(value);
	}

	object IPostgresTypeConverter<List<T>>.ToDbValue(List<T> value)
	{
		return this.ToDbValueInternal(value);
	}

	List<T> IPostgresTypeConverter<List<T>>.FromDbValue(object dbValue)
	{
		return this.FromDbValueInternal(dbValue);
	}

	IList<T> IPostgresTypeConverter<IList<T>>.FromDbValue(object dbValue)
	{
		return this.FromDbValueInternal(dbValue);
	}

	NpgsqlDbType IPostgresTypeConverter<List<T>>.DatabaseType
	{
		get { return NpgsqlDbType.Array | this.m_innerConverter.DatabaseType; }
	}

	NpgsqlDbType IPostgresTypeConverter<IList<T>>.DatabaseType
	{
		get { return NpgsqlDbType.Array | this.m_innerConverter.DatabaseType; }
	}

	private object ToDbValueInternal(IList<T> value)
	{
		if (value == null)
		{
			return DBNull.Value;
		}

		object[] dbValues = new object[value.Count];
		for (int i = 0; i < value.Count; i++)
		{
			dbValues[i] = this.m_innerConverter.ToDbValue(value[i]);
		}

		return dbValues;
	}

	private List<T> FromDbValueInternal(object dbValue)
	{
		if (dbValue is DBNull || dbValue == null)
		{
			return null;
		}

		Array dbValues = (Array)dbValue;
		List<T> values = new(dbValues.Length);
		for (int i = 0; i < dbValues.LongLength; i++)
		{
			values.Add(this.m_innerConverter.FromDbValue(dbValues.GetValue(i)));
		}
		return values;
	}

}
