using System.Drawing;
using System.Numerics;
using NpgsqlTypes;

namespace MurkYamlStore.Postgres.TypeConverters.Impl;

internal sealed class ColorTypeConverter : IPostgresTypeConverter<Color>
{
    NpgsqlDbType IPostgresTypeConverter<Color>.DatabaseType => NpgsqlDbType.Bytea;

    Color IPostgresTypeConverter<Color>.FromDbValue(object dbValue)
    {
        byte[] colorBytes = (byte[])dbValue;

        // Truncate alpha
        if (colorBytes.Length > 3)
        {
            colorBytes = colorBytes[^3..];
        }

        return Color.FromArgb(255, colorBytes[0], colorBytes[1], colorBytes[2]);
    }

    object IPostgresTypeConverter<Color>.ToDbValue(Color value)
    {
        return new byte[] { value.R, value.G, value.B };
    }
}
