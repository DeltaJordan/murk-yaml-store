using System;
using NpgsqlTypes;

namespace MurkYamlStore.Postgres.TypeConverters.Impl;

internal sealed class EnumTypeConverter<T> : IPostgresTypeConverter<T>
{

    NpgsqlDbType IPostgresTypeConverter<T>.DatabaseType => NpgsqlDbType.Text;

    T IPostgresTypeConverter<T>.FromDbValue(object dbValue)
    {
        return (T)Enum.Parse(typeof(T), dbValue as string);
    }

    object IPostgresTypeConverter<T>.ToDbValue(T value)
    {
        return value.ToString();
    }

}
