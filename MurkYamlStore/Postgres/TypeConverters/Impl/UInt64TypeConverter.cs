using System.Numerics;
using NpgsqlTypes;

namespace MurkYamlStore.Postgres.TypeConverters.Impl;

internal sealed class UInt64TypeConverter : IPostgresTypeConverter<ulong>
{
    NpgsqlDbType IPostgresTypeConverter<ulong>.DatabaseType => NpgsqlDbType.Numeric;

    ulong IPostgresTypeConverter<ulong>.FromDbValue(object dbValue)
    {
        return decimal.ToUInt64((decimal)dbValue);
    }

    object IPostgresTypeConverter<ulong>.ToDbValue(ulong value)
    {
        return (decimal)value;
    }
}
