using System;
using NpgsqlTypes;

namespace MurkYamlStore.Postgres.TypeConverters.Impl;

internal sealed class NullableTypeConverter<T> : IPostgresTypeConverter<T?> where T : struct
{

	internal readonly IPostgresTypeConverter<T> m_innerConverter;

	public NullableTypeConverter(
		IPostgresTypeConverter<T> innerConverter
	)
	{
		this.m_innerConverter = innerConverter;
	}

	object IPostgresTypeConverter<T?>.ToDbValue(T? value)
	{
		if (value.HasValue)
		{
			return this.m_innerConverter.ToDbValue(value.Value);
		}
		return DBNull.Value;
	}

	T? IPostgresTypeConverter<T?>.FromDbValue(object dbValue)
	{
		if (dbValue is DBNull || dbValue == null)
		{
			return null;
		}
		return this.m_innerConverter.FromDbValue(dbValue);
	}

	NpgsqlDbType IPostgresTypeConverter<T?>.DatabaseType
	{
		get { return this.m_innerConverter.DatabaseType; }
	}

}
