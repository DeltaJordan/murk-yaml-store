using NpgsqlTypes;

namespace MurkYamlStore.Postgres.TypeConverters.Impl;

internal sealed class UInt16TypeConverter : IPostgresTypeConverter<ushort>
{
    NpgsqlDbType IPostgresTypeConverter<ushort>.DatabaseType => NpgsqlDbType.Numeric;

    ushort IPostgresTypeConverter<ushort>.FromDbValue(object dbValue)
    {
        return (ushort)dbValue;
    }

    object IPostgresTypeConverter<ushort>.ToDbValue(ushort value)
    {
        return (int)value;
    }
}
