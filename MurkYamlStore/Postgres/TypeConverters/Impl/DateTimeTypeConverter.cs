using System;
using NpgsqlTypes;

namespace MurkYamlStore.Postgres.TypeConverters.Impl;

internal sealed class DateTimeTypeConverter : IPostgresTypeConverter<DateTime>
{

	object IPostgresTypeConverter<DateTime>.ToDbValue(DateTime value)
	{
		return value;
	}

	DateTime IPostgresTypeConverter<DateTime>.FromDbValue(object dbValue)
	{
		DateTime timestamp = (DateTime)dbValue;
		if (timestamp.Kind == DateTimeKind.Unspecified)
		{
			return DateTime.SpecifyKind(timestamp, DateTimeKind.Utc);
		}
		else
		{
			return timestamp;
		}
	}

	NpgsqlDbType IPostgresTypeConverter<DateTime>.DatabaseType
	{
		/* Always return Timestamp (without time zone), even if the DateTime
			 * is in local time. This works fine because of the way that Npgsql
			 * handles DateTime. So long as the database column type matches the
			 * DateTime kind, the correct time will be used.
			 */
		get { return NpgsqlDbType.Timestamp; }
	}

}
