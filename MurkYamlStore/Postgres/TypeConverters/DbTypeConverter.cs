using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using MurkYamlStore.Postgres.TypeConverters.Impl;
using NpgsqlTypes;

namespace MurkYamlStore.Postgres.TypeConverters;

internal static class DbTypeConverter<T>
{

    private static readonly IPostgresTypeConverter<T> converter;

    static DbTypeConverter()
    {
        if (typeof(T) == typeof(DateTime))
        {
            converter = (IPostgresTypeConverter<T>)(object)new DateTimeTypeConverter();
            return;
        }

        if (typeof(T) == typeof(Color))
        {
            converter = (IPostgresTypeConverter<T>)(object)new ColorTypeConverter();
            return;
        }

        if (typeof(T) == typeof(byte[]))
        {
            converter = new DefaultTypeConverter<T>();
            return;
        }

        if (typeof(T) == typeof(ushort))
        {
            converter = (IPostgresTypeConverter<T>)(object)new UInt16TypeConverter();
            return;
        }

        if (typeof(T).IsArray)
        {
            Type elementType = typeof(T).GetElementType();
            object innerConverter = GetConverterForType(elementType);

            converter = CreateWrappingConverter(
                genericTypeConverter: typeof(ArrayTypeConverter<>),
                innerType: elementType,
                innerConverter: innerConverter
            );
            return;
        }

        if (
            IsGenericTypeOf(typeof(T), typeof(List<>)) ||
            IsGenericTypeOf(typeof(T), typeof(IList<>))
        )
        {
            Type elementType = typeof(T).GetGenericArguments()[0];
            object innerConverter = GetConverterForType(elementType);

            converter = CreateWrappingConverter(
                genericTypeConverter: typeof(ListTypeConverter<>),
                innerType: elementType,
                innerConverter: innerConverter
            );
            return;
        }

        if (IsGenericTypeOf(typeof(T), typeof(IEnumerable<>)))
        {
            Type elementType = typeof(T).GetGenericArguments()[0];
            object innerConverter = GetConverterForType(elementType);

            converter = CreateWrappingConverter(
                genericTypeConverter: typeof(EnumerableTypeConverter<>),
                innerType: elementType,
                innerConverter: innerConverter
            );
            return;
        }

        if (IsGenericTypeOf(typeof(T), typeof(Nullable<>)))
        {
            Type innerType = Nullable.GetUnderlyingType(typeof(T));
            object innerConverter = GetConverterForType(innerType);

            converter = CreateWrappingConverter(
                genericTypeConverter: typeof(NullableTypeConverter<>),
                innerType: innerType,
                innerConverter: innerConverter
            );
            return;
        }

        if (typeof(T).IsEnum)
        {
            converter = new EnumTypeConverter<T>();
            return;
        }

        object[] ptcAttributes = typeof(T).GetCustomAttributes(
            attributeType: typeof(PostgresTypeConverterAttribute),
            inherit: false
        );

        if (ptcAttributes.Length > 0)
        {
            PostgresTypeConverterAttribute pgTypeConverterAttr = (PostgresTypeConverterAttribute)ptcAttributes[0];
            Type converterType = pgTypeConverterAttr.ConverterType;

            if (!typeof(IPostgresTypeConverter<T>).IsAssignableFrom(converterType))
            {
                throw new InvalidCastException(
                    "The type provided to a [PostgresTypeConverter] attribute " +
                    "is not a type converter for the expected type."
                );
            }

            converter = (IPostgresTypeConverter<T>)Activator.CreateInstance(converterType);
            return;
        }

        converter = new DefaultTypeConverter<T>();
    }

    internal static object ToDbValue(T value)
    {
        return converter.ToDbValue(value);
    }

    internal static T FromDbValue(object dbValue)
    {
        return converter.FromDbValue(dbValue);
    }

    internal static NpgsqlDbType DatabaseType
    {
        get { return converter.DatabaseType; }
    }


    internal static IPostgresTypeConverter<T> Converter
    {
        get { return converter; }
    }

    private static object GetConverterForType(Type type)
    {
        return typeof(DbTypeConverter<>).MakeGenericType(type).GetProperty(
            name: "Converter",
            bindingAttr: BindingFlags.Static | BindingFlags.NonPublic
        ).GetMethod.Invoke(null, null);
    }

    private static IPostgresTypeConverter<T> CreateWrappingConverter(
        Type genericTypeConverter,
        Type innerType,
        object innerConverter
    )
    {
        return (IPostgresTypeConverter<T>)Activator.CreateInstance(
            type: genericTypeConverter.MakeGenericType(innerType),
            args: new[] { innerConverter }
        );
    }

    private static bool IsGenericTypeOf(
        Type type,
        Type genericTypeDefinition
    )
    {
        return
            type.IsGenericType &&
            type.GetGenericTypeDefinition() == genericTypeDefinition
        ;
    }

}
