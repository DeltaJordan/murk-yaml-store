using System.Threading.Tasks;

namespace MurkYamlStore.Postgres.Migration;

public interface IMigrationRunner
{
	Task RunDatabaseMigrationsAsync();
}
