using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using MurkYamlStore.Core;
using MurkYamlStore.Postgres;
using NLog;
using Npgsql;

namespace MurkYamlStore.Postgres.Migration;

internal sealed class MigrationRunner : IMigrationRunner
{

    private static readonly Logger logger = LogManager.GetCurrentClassLogger();
    private readonly IPostgresDatabase database;

    public MigrationRunner(
        IPostgresDatabase database
    )
    {
        this.database = database;
    }

    async Task IMigrationRunner.RunDatabaseMigrationsAsync()
    {
        string migrationDir = $"{Directory.GetCurrentDirectory()}/migration";
        if (!Directory.Exists(migrationDir))
        {
            migrationDir = Directory.GetParent(Directory.GetCurrentDirectory()).FullName + "/migration";
            if (!Directory.Exists(migrationDir))
            {
                throw new Exception("Database migration directory not found.");
            }
        }

        HashSet<string> completedMigrations = [];
        TryResult<long> initialSetupRun = await this.database.ExecTryReadScalarAsync<long>(
            new PostgresCommand(
                sql: @"
SELECT COUNT(*) FROM information_schema.tables
	WHERE table_schema = 'public'
	AND table_name = 'migrations'
",
                prepare: false
            )
        ).SafeAsync();

        if (initialSetupRun.Value > 0)
        {
            completedMigrations.UnionWith(
                await this.database.ExecReadColumnAsync<string>(
                    new PostgresCommand(
                        sql: "SELECT filename FROM migrations",
                        prepare: false
                    )
                ).SafeAsync()
            );
        }

        IEnumerable<string> migrationScripts = Directory.EnumerateFiles(migrationDir).OrderBy(x => x);
        foreach (string scriptPath in migrationScripts)
        {
            string scriptName = Path.GetFileNameWithoutExtension(scriptPath);
            if (completedMigrations.Contains(scriptName)) continue;
            logger.Info($"Running migration script {scriptName}");
            try
            {
                await this.RunMigrationScriptAsync(scriptPath, scriptName).SafeAsync();
            }
            catch (Exception)
            {
                logger.Fatal($"Migration script '{scriptName}' failed.");
                throw;
            }
            logger.Info($"Migration '{scriptName}' completed successfully.");
        }

        NpgsqlConnection.ClearAllPools();
    }

    private async Task RunMigrationScriptAsync(string scriptPath, string migrationId)
    {
        string fullScript = await File.ReadAllTextAsync(scriptPath, Encoding.UTF8).SafeAsync();
        string[] batch = fullScript.Split("\nGO\n", StringSplitOptions.RemoveEmptyEntries);

        IPostgresTransaction transaction = await this.database.NewTransactionAsync(PostgresIsolationLevel.ReadCommitted).SafeAsync();
        await using ConfiguredAsyncDisposable handle = transaction.GetHandle();

        foreach (string sql in batch)
        {
            await transaction.ExecNonQueryAsync(
                new PostgresCommand(sql, prepare: false)
            ).SafeAsync();
        }

        PostgresCommand cmd = new(@"
INSERT INTO migrations( filename, date_completed )
VALUES( :filename, :date_completed )",
            prepare: false
        );
        cmd.AddParameter("filename", migrationId);
        cmd.AddParameter("date_completed", DateTime.UtcNow);

        await transaction.ExecNonQueryAsync(cmd).SafeAsync();
        await transaction.CommitAsync().SafeAsync();
    }

}
