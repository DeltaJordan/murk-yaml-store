using Microsoft.Extensions.DependencyInjection;
using MurkYamlStore.Activation;
using MurkYamlStore.Postgres.Impl;
using MurkYamlStore.Postgres.Migration;

namespace MurkYamlStore.Postgres;

internal static class DependencyLoader
{

	public static void Load(IServiceCollection registry)
	{
		registry.AddFactory<IPostgresDatabase, PostgresDatabaseFactory>();
		registry.AddSingleton<IMigrationRunner, MigrationRunner>();
	}

}
