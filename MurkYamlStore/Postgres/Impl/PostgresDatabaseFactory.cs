using MurkYamlStore.Activation;
using MurkYamlStore.Postgres;

namespace MurkYamlStore.Postgres.Impl;

internal sealed class PostgresDatabaseFactory : IFactory<IPostgresDatabase>
{

	IPostgresDatabase IFactory<IPostgresDatabase>.Create()
	{
		return new PostgresDatabase(ServerConfig.Instance.DbConnectionString);
	}

}
