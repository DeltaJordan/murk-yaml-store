using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using MurkYamlStore.Postgres;
using Npgsql;
using MurkYamlStore.Core;
using MurkYamlStore.Postgres.TypeConverters;

namespace MurkYamlStore.Postgres.Impl;

internal abstract class PostgresExecutorBase : IPostgresExecutor
{

	async Task<int> IPostgresExecutor.ExecNonQueryAsync(
		PostgresCommand command
	)
	{
		int rowsAffected = -1;
		await this.ExecuteAsync(command, async cmd =>
		{
			rowsAffected = await cmd.ExecuteNonQueryAsync().SafeAsync();
		}).SafeAsync();

		return rowsAffected;
	}

	async Task<TryResult<T>> IPostgresExecutor.ExecTryReadScalarAsync<T>(
		PostgresCommand command
	)
	{
		TryResult<T> result = new();
		await this.ExecuteAsync(command, async cmd =>
		{
			await using DbDataReader reader = await cmd.ExecuteReaderAsync().SafeAsync();
			if (await reader.ReadAsync().SafeAsync() && reader.FieldCount > 0)
			{
				result = new TryResult<T>(DbTypeConverter<T>.FromDbValue(reader[0]));
			}
		}).SafeAsync();
		return result;
	}

	async Task<TryResult<Dto>> IPostgresExecutor.ExecTryReadFirstAsync<Dto>(
		PostgresCommand command,
		Func<IDataRecord, Dto> dbConverter
	)
	{
		TryResult<Dto> result = new();
		await this.ExecuteAsync(command, async cmd =>
		{
			await using DbDataReader reader = await cmd.ExecuteReaderAsync().SafeAsync();
			if (await reader.ReadAsync().SafeAsync())
			{
				result = new TryResult<Dto>(dbConverter(reader));
			}
		}).SafeAsync();
		return result;
	}

	async Task<IReadOnlyList<Dto>> IPostgresExecutor.ExecReadManyAsync<Dto>(
		PostgresCommand command,
		Func<IDataRecord, Dto> dbConverter
	)
	{
		List<Dto> results = [];
		await this.ExecuteAsync(command, async cmd =>
		{
			await using DbDataReader reader = await cmd.ExecuteReaderAsync().SafeAsync();
			while (await reader.ReadAsync().SafeAsync())
			{
				results.Add(dbConverter(reader));
			}
		}).SafeAsync();
		return results;
	}

	protected abstract Task ExecuteAsync(
		PostgresCommand command,
		Func<NpgsqlCommand, Task> action
	);

}
