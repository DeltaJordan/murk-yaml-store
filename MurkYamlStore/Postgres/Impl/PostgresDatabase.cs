using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Npgsql;

namespace MurkYamlStore.Postgres.Impl;

internal sealed class PostgresDatabase : PostgresExecutorBase, IPostgresDatabase
{

	private readonly string connectionString;

	internal PostgresDatabase(string npgsqlConnectionString)
	{
		this.connectionString = npgsqlConnectionString;
	}

	Task<IPostgresTransaction> IPostgresDatabase.NewTransactionAsync(
		PostgresIsolationLevel isolationLevel
	)
	{
		return PostgresTransaction.ConstructAsync(this.connectionString, isolationLevel);
	}

	protected override async Task ExecuteAsync(
		PostgresCommand command,
		Func<NpgsqlCommand, Task> action
	)
	{
		NpgsqlConnection connection = new(this.connectionString);
		await using ConfiguredAsyncDisposable connectionHandle = connection.ConfigureAwait(false);

		await connection.OpenAsync().SafeAsync();

		NpgsqlCommand cmd = await command.BuildAsync(connection).SafeAsync();
		await using ConfiguredAsyncDisposable commandHandle = cmd.ConfigureAwait(false);

		await action(cmd).SafeAsync();
	}

}
