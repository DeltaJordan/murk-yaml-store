using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using MurkYamlStore.Postgres;
using Npgsql;

namespace MurkYamlStore.Postgres.Impl;

internal sealed class PostgresTransaction : PostgresExecutorBase, IPostgresTransaction
{
	private enum TransactionState
	{
		Uncommited,
		Commited,
		RolledBack
	}

	private readonly NpgsqlConnection connection;
	private readonly NpgsqlTransaction transaction;

	private TransactionState state = TransactionState.Uncommited;

	internal async static Task<IPostgresTransaction> ConstructAsync(
		string connectionString,
		PostgresIsolationLevel pgIsolationLevel
	)
	{
		NpgsqlConnection connection = null;
		NpgsqlTransaction transaction = null;
		try
		{
			connection = new NpgsqlConnection(connectionString);
			await connection.OpenAsync().SafeAsync();
			transaction = connection.BeginTransaction(
				pgIsolationLevel.ToAdoIsolationLevel()
			);
			return new PostgresTransaction(connection, transaction);
		}
		catch (Exception)
		{
			await transaction.SafeDisposeAsync().SafeAsync();
			await connection.SafeDisposeAsync().SafeAsync();
			throw;
		}
	}

	private PostgresTransaction(
		NpgsqlConnection openConnection,
		NpgsqlTransaction transaction
	)
	{
		this.connection = openConnection;
		this.transaction = transaction;
	}

	async ValueTask IAsyncDisposable.DisposeAsync()
	{
		if (this.state == TransactionState.Uncommited)
		{
			this.state = TransactionState.RolledBack;
			await this.transaction.SafeDisposeAsync().SafeAsync();
			await this.connection.SafeDisposeAsync().SafeAsync();
		}
	}

	async Task IPostgresTransaction.CommitAsync()
	{
		IPostgresTransaction @this = this;
		this.AssertIsOpen();

		try
		{
			await this.transaction.CommitAsync().SafeAsync();
		}
		catch (Exception commitException)
		{
			try
			{
				await @this.RollbackAsync().SafeAsync();
			}
			catch (Exception rollbackException)
			{
				throw new AggregateException(commitException, rollbackException);
			}
			throw;
		}

		await @this.DisposeAsync().SafeAsync();
		this.state = TransactionState.Commited;
	}

	async Task IPostgresTransaction.RollbackAsync()
	{
		if (this.state == TransactionState.Commited)
		{
			throw new InvalidOperationException(
				"The transaction could not be rolled back because it has " +
				"already been successfully committed."
			);
		}
		else if (this.state == TransactionState.Uncommited)
		{
			try
			{
				await this.transaction.RollbackAsync().SafeAsync();
			}
			finally
			{
				await ((IAsyncDisposable)this).DisposeAsync().SafeAsync();
			}
		}
	}

	ConfiguredAsyncDisposable IPostgresTransaction.GetHandle()
	{
		return this.ConfigureAwait(false);
	}

	protected async override Task ExecuteAsync(
		PostgresCommand command,
		Func<NpgsqlCommand, Task> action
	)
	{
		this.AssertIsOpen();
		NpgsqlCommand cmd = await command.BuildAsync(this.connection, this.transaction).SafeAsync();
		await using ConfiguredAsyncDisposable cmdHandle = cmd.ConfigureAwait(false);

		try
		{
			await action(cmd).SafeAsync();
		}
		catch (Exception commandException)
		{
			try
			{
				await ((IPostgresTransaction)this).RollbackAsync().SafeAsync();
			}
			catch (Exception rollbackException)
			{
				throw new AggregateException(commandException, rollbackException);
			}
			throw;
		}
	}


	private void AssertIsOpen()
	{
		if (this.state != TransactionState.Uncommited)
		{
			throw new ObjectDisposedException("PostgresTransaction");
		}
	}

}
