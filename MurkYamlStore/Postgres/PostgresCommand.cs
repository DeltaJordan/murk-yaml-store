using System.Collections.Generic;
using System.Threading.Tasks;
using MurkYamlStore.Postgres.TypeConverters;
using Npgsql;
using NpgsqlTypes;

namespace MurkYamlStore.Postgres;


/// <summary>
/// Represents a SQL statement to execute against a PostgreSQL database.
/// <para>
/// When executing a command that contains multiple semicolon-separated
/// statements, an error in any statement will cause the entire command to
/// be rolled back, even if it is not in an explicit transaction.
/// </para>
/// </summary>
/// <threadsafety instance="false" />
/// <remarks>
/// Initialize a new Postgres command with the given SQL query.
/// </remarks>
/// <param name="sql">The text of the query.</param>
/// <param name="prepare">Whether or not to prepare the statement for fast re-use</param>
/// <param name="timeout">
/// The time to wait (in seconds) while trying to execute a command
/// before terminating the attempt and generating an error. Set to 0 for
/// no time limit. If <c>null</c>, the value of <c>CommandTimeout</c>
/// from the connection string is used (Defaults to 30 if not specified
/// in the connection string).
/// </param>
public sealed class PostgresCommand(string sql, bool prepare = true, int? timeout = null)
{
	private readonly List<NpgsqlParameter> parameters = [];
	private readonly bool prepare = prepare;
	private readonly int? timeout = timeout;
	private string sql = sql;

	/// <summary>Append the given text to the SQL query.</summary>
	/// <param name="sql">The text to append to the query.</param>
	public void AppendSql(string sql)
	{
		this.sql += sql;
	}

	/// <summary>
	/// Add a value to a parameter. To use parameters in your SQL query
	/// string, prefix the parameter name in the SQL text with a colon, then
	/// call this method with the parameter name (without the : prefix) and
	/// value.
	///
	/// This method guesses the parameter's database type from its C# type.
	/// For some types (eg. json), it is necessary to explicitly specify the
	/// database type by using the form of this method that takes a database
	/// type as a 3rd argument.
	/// </summary>
	/// <param name="name">The name of the parameter.</param>
	/// <param name="value">The value to use for the parameter.</param>
	public void AddParameter<T>(string name, T value)
	{
		this.AddParameter(name, value, DbTypeConverter<T>.DatabaseType);
	}

	/// <summary>
	/// Add a value to a parameter. To use parameters in your SQL query
	/// string, prefix the parameter name in the SQL text with a colon, then
	/// call this method with the parameter name (without the : prefix),
	/// value, and database type.
	/// </summary>
	/// <param name="name">The name of the parameter.</param>
	/// <param name="value">The value to use for the parameter.</param>
	/// <param name="dbType">The database type of the parameter.</param>
	public void AddParameter<T>(string name, T value, NpgsqlDbType dbType)
	{
		NpgsqlParameter parameter = new(
			parameterName: name,
			value: DbTypeConverter<T>.ToDbValue(value)
		)
		{
			NpgsqlDbType = dbType
		};

		this.parameters.Add(parameter);
	}

	internal async Task<NpgsqlCommand> BuildAsync(
		NpgsqlConnection connection,
		NpgsqlTransaction transaction = null
	)
	{
		NpgsqlCommand cmd = new(this.sql, connection, transaction);

		foreach (NpgsqlParameter parameter in this.parameters)
		{
			cmd.Parameters.Add(parameter.Clone());
		}

		if (this.timeout.HasValue)
		{
			cmd.CommandTimeout = this.timeout.Value;
		}

		if (this.prepare)
		{
			await cmd.PrepareAsync().SafeAsync();
		}

		return cmd;
	}

}
