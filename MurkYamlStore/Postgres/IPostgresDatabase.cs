using System;
using System.Threading.Tasks;

namespace MurkYamlStore.Postgres;


/// <summary>
/// The interface of a Postgres database. Each command is executed in a new
/// virtual connection (but may be on the same physical connection if
/// connection pooling is used).
/// </summary>
/// <threadsafety instance="true" />
public interface IPostgresDatabase : IPostgresExecutor
{

	/// <summary>
	/// Begins a new transaction with the given isolation level. The
	/// transaction is automatically rolled back if it is disposed before
	/// <c>Commit()</c> is called.
	/// <see cref="IPostgresTransaction.CommitAsync"/> or
	/// <see cref="IPostgresTransaction.RollbackAsync"/>.
	/// </para>
	/// </summary>
	/// <param name="isolationLevel">The isolation level.</param>
	/// <returns>The new transaction.</returns>
	Task<IPostgresTransaction> NewTransactionAsync(
		PostgresIsolationLevel isolationLevel = PostgresIsolationLevel.ReadCommitted
	);

}
