namespace MurkYamlStore;

internal static class MasterDependencyLoader
{
    public static void Load(IServiceCollection registry)
    {
        Core.DependencyLoader.Load(registry);
        Postgres.DependencyLoader.Load(registry);
        Caching.DependencyLoader.Load(registry);
        Data.DependencyLoader.Load(registry);
        Domain.DependencyLoader.Load(registry);
        Tools.DependencyLoader.Load(registry);
    }
}
